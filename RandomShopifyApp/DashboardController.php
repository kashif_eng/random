<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Rma;


class DashboardController extends Controller
{
    
    
    public function __construct()
    {
        //$this->middleware('auth');
       
       
    }

 
    
    public function home(Request $request)
    {
        //dd($request->all());
        $shop = Auth::user();   

        if($request->status_filter || $request->date_filter){
            if($request->status_filter && $request->date_filter){
                $rma = RMA::where('status',$request->status_filter)->whereDate('created_at',$request->date_filter)->get();
            }
            else if($request->status_filter){
                $rma = RMA::where('status',$request->status_filter)->get();
            }
            else{
                $rma = RMA::whereDate('created_at',$request->date_filter)->get();
            }            
        }
        else{
            $rma = RMA::where('status',Null)->orWhere('status','pending')->get();
        }
        
        return view('welcome',compact('shop','rma'));
               
    }

    public function updateReturns(Request $request)
    {     
       //dd($request->all());
       Rma::updateOrCreate(['order_name' => $request->order_name,],$request->all());

       echo "<span style='background-color:gray;padding:10px;margin:10px 0px;color:white;'>Your request has been submitted successfully</span>";
    }


    public function changeStatus(Request $request)
    {     
        //dd($request->all());
        Rma::where('order_name',$request->order_number)->update([
                'status' => $request->status
        ]);

        return redirect()->back()->with("success","Status has been saved successfully!!");;
       
    }






}