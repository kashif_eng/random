<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Settings;

use Log;

class ProductsUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $shopDomain;
    public $data;
  
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

   
    public function handle()
    {
        $shop = User::where('name', $this->shopDomain)->first();
                       
        // Log::info(print_r($this->data, true));
        // Log::info("Product Updated");
        // exit;

        $tags = $this->data->tags;
        $tagsArray = explode(', ',$tags);
        $newtags = "";
        $tagFilter = "";
        

        foreach($this->data->variants as $variant){

            $product_id = $variant->product_id;
            $variant_id = $variant->id;            
            $variant_title = $variant->title;
            $inventory_item_id = $variant->inventory_item_id;
            $inventory_quantity = $variant->inventory_quantity;
            $old_inventory_quantity = $variant->old_inventory_quantity;


            if (($key = array_search($variant_title, $tagsArray)) !== false) {
                unset($tagsArray[$key]);
            }
            
            
            if($inventory_quantity > 0){
                $newtags = $newtags.", ".$variant_title;
            }
            

        }


        foreach($tagsArray as $item){
            $tagFilter = $tagFilter.", ".$item;
        }

        $finalTags = $newtags.", ".$tagFilter;
        
        $parameter_array = [
            'product' => 
            [
                'id' => $product_id,                
                'tags' => $finalTags
            ]
        ];

        $shopApi = $shop->api()->rest('PUT', '/admin/products/'.$product_id.'.json',$parameter_array);

        //Log::info(print_r($shopApi, true));

    }
}
