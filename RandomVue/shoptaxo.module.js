import axios from 'axios';
import router from '../router';

export default {
    namespaced: true,
    state:{
        headingTitle: '',
        loader: true,        
        links: {
            mainLink: { title: 'Home', link: '/' },
            secondLink: { title: 'Shop', link: '/shop' },
            lastLink: ''
        },   
        meta: {
            metalength: 0,
            metacurrent: 1
        },
        brands: [],   
        categoryList:[],
        products:[]
        
    },
    mutations:{
        putLoader (state,data){
            state.loader = data
        },
        putHeadingTitle (state, data){
            state.headingTitle = data
        },
        putBrands (state,data){
            state.brands = data
        },        
        putCategoryList (state,data){
            state.categoryList = data
        },
        putProducts (state,data){
            state.products = data
        },        
        putMetalength (state, data) {
            state.meta.metalength = data
        },
        putMetacurrent (state, data) {
            state.meta.metacurrent = data
        }

    },
    actions:{
        fetchProducts(state, page){               
            state.commit('putLoader',true);
            axios.get('/api/shoptaxo?title='+router.currentRoute._rawValue.params.title,{
                params:{
                    page                    
                }
            })
            .then((response) => {
                state.commit('putLoader',false);
                state.commit('putHeadingTitle',response.data.heading);                
                state.commit('putProducts',response.data.data.data);   
                state.commit('putMetalength',response.data.data.last_page);
                state.commit('putMetacurrent',response.data.data.current_page);
            })
        },
        fetchBrands(state, page){
                state.commit('putLoader',true);
                axios.get('/api/brands')
                .then((response) => {
                    state.commit('putLoader',false);
                    state.commit('putBrands',response.data.data);                  
                })
        },        
        fetchCategoryList(state, page){
            state.commit('putLoader',true);
            axios.get('/api/category-list')
            .then((response) => {
                state.commit('putLoader',false);
                state.commit('putCategoryList',response.data.data);                  
            })
        }

    },
    getters:{
        getBrands (state){
            return state.brands
        },           
        getCategoryList (state){
            return state.categoryList
        },   
        getProducts (state){
            return state.products
        },                
        getMeta (state) {
            return state.meta
        }
    }
}