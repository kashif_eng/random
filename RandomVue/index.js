import { createRouter, createWebHistory } from "vue-router";
import store from '../store/index'

import Notfound from '../components/pages/Notfound.vue'

//Pages
import Home from '../components/pages/Home.vue'
import Page from '../components/pages/Page.vue'

//Blog
import BlogGrid from '../components/blog/BlogGrid.vue'
import BlogTaxo from '../components/blog/BlogTaxo.vue'
import BlogTags from '../components/blog/BlogTags.vue'
import BlogPost from '../components/blog/BlogPost.vue'

//Shop
import ShopGrid from '../components/shop/ShopGrid.vue'
import ShopProduct from '../components/shop/ShopProduct.vue'
import ShopCart from '../components/shop/ShopCart.vue'
import ShopCheckout from '../components/shop/ShopCheckout.vue'
import ShopComplete from '../components/shop/ShopComplete.vue'
import ShopBrands from '../components/shop/ShopBrands.vue'
import ShopTaxo from '../components/shop/ShopTaxo.vue'
import ShopAz from '../components/shop/ShopAz.vue'
import SearchGrid from '../components/shop/SearchGrid.vue'

//Customer
import CustomerLogin from '../components/accounts/CustomerLogin.vue'
import CustomerDashboard from '../components/accounts/CustomerDashboard.vue'


const routes = [
    {
        path: '/:catchAll(.*)',
        name: 'Notfound',
        component: Notfound
    },
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/page/:url',
        name: 'Page',
        component: Page
    },
    {
        path: '/blog',
        name: 'BlogGrid',
        component: BlogGrid
    },
    {
        path: '/blog/category/:title',
        name: 'BlogTaxo',
        component: BlogTaxo
    },
    {
        path: '/blog/tags/:title',
        name: 'BlogTags',
        component: BlogTags
    },
    {
        path: '/blog/:url',
        name: 'BlogPost',
        component: BlogPost
    },
    {
        path: '/shop',
        name: 'ShopGrid',
        component: ShopGrid
    },
    {
        path: '/shop/:url',
        name: 'ShopProduct',
        component: ShopProduct
    },
    {
        path: '/cart',
        name: 'ShopCart',
        component: ShopCart
    },
    {
        path: '/checkout',
        name: 'ShopCheckout',
        component: ShopCheckout
    },
    {
        path: '/complete',
        name: 'ShopComplete',
        component: ShopComplete
    },
    {
        path: '/shop/brands/:title',
        name: 'ShopBrands',
        component: ShopBrands
    },
    {
        path: '/shop/product-category/:title',
        name: 'ShopTaxo',
        component: ShopTaxo
    },
    {
        path: '/shop-az',
        name: 'ShopAz',
        component: ShopAz
    },
    {
        path: '/search/:term',
        name: 'SearchGrid',
        component: SearchGrid
    },
    {
        path: '/customer-login',
        name: 'CustomerLogin',
        component: CustomerLogin,
    },
    {
        path: '/customer-logout',
        name: 'CustomerLogout',
        component: CustomerLogin,
        beforeEnter: (to, from, next) => {                 
            store.dispatch('account/attemptLogout')
            next()
        }
    },
    {
        path: '/customer-dashboard',
        name: 'CustomerDashboard',
        component: CustomerDashboard,
        meta: {
           authRequired: true,
        }
    }
    
    
]

//export default createRouter({    
const router = createRouter({       
    scrollBehavior(to, from, savedPosition) {        
        return { top: 0,behavior: 'smooth',}
    },
    // scrollBehavior(to, from, savedPosition) {
    //     return new Promise((resolve, reject) => {
    //       setTimeout(() => {
    //         resolve({ left: 0, top: 0 , behavior: 'smooth'})
    //       }, 500)
    //     })
    //   },
    history: createWebHistory(),
    routes,
    
})

router.beforeEach(async (routeTo, routeFrom, next) => {
      
    const authRequired = routeTo.matched.some((route) => route.meta.authRequired)

    if (!authRequired) return next()

    if(localStorage.getItem('jwt')){
        next()
    }
    else{
        store.dispatch('account/attemptLogout')
        next({ name: 'CustomerLogin' })
    }
    
    // if (megaMenu.length > 0){
    //     next();
    // }
    // else{        
       
    //     next();
    // }
    // const authRequired = routeTo.matched.some((route) => route.meta.authRequired)
  
    // if (!authRequired) return next()
   
    // axios.defaults.headers.common['authorization'] = 'Bearer ' + localStorage.getItem('jwt') // for all requests
    // await axios.get('http://127.0.0.1:8000/api/user').then((data) => {  
    //   localStorage.setItem('userdata', JSON.stringify(data.data.user))
    //   localStorage.setItem('userid', data.data.user.id)
    //   next()
    // }).catch(() => {
    //   next({ name: 'login', query: { redirectFrom: routeTo.fullPath } })
    // });

})

  

//   meta: {
//     title: "Login",
//     beforeResolve(routeTo, routeFrom, next) {
//       // If the user is already logged in       
//       if (store.getters["auth/loggedIn"]) {
//         // Redirect to the home page instead
//         next({ name: "default" });
//       } else {
//         // Continue to the login page
//         next();
//       }
//     },
//   },

export default router