/*
|--------------------------------------------------------------------------
| Main Windows On Load Starts
|--------------------------------------------------------------------------
*/   
$(window).load(function(){

    $("html,body").animate({scrollTop: 0}, 500);        
    jQuery('#spinner_waiter').css({"display": "none", "position": "fixed", "top": "10px"});
       
    
}); 

jQuery(document).ready(function($) {
        
    jQuery('#gostage3').click(function(evt){
        
        if(!jQuery('#try').valid()){ 
            $('html, body').animate({
                scrollTop: ($('.error:visible').offset().top - 60)
            }, 500);
            return false;
        }    
        
        jQuery('#spinner_waiter_upload').css({"display": "flex", "position": "fixed", "top": "10px"});
        jQuery('#spinner_waiter_upload_msg').css({"display": "flex", "position": "fixed", "bottom": "0px", "z-index": "99999"});	
        // setTimeout(function(){
        //     jQuery('#spinner_waiter').css({"display": "none", "position": "fixed", "top": "10px"});	
        // }, 200);


    }); 

}); 
/*
|--------------------------------------------------------------------------
| Main Windows On Load Ends
|--------------------------------------------------------------------------
*/  
if(performance.navigation.type == 2)
{
                
        //window.location = "http://order.researchprospect.com/";
        //window.location = "http://127.0.0.1:8000/";
        
        /*if(confirm("Pressing 'OK' will lose any information you have entered. If you need to amend any of your order details,don't forget you can contact us after placing the order to make any changes.")){
				    location.reload();
		}
       */
          location.reload();

}
jQuery(document).ready(function($) {

    if (window.history && window.history.pushState) {
  
      window.history.pushState('forward', null);
  
      $(window).on('popstate', function() {
        if(confirm("Pressing 'OK' will lose any information you have entered. If you need to amend any of your order details,don't forget you can contact us after placing the order to make any changes.")){
            window.location = "https://rporderapp.looptestingserver.com/";
        }
      });
  
    }
  });
/*if(window.history && history.pushState){ // check for history api support
	window.addEventListener('load', function(){
		// create history states
		history.pushState(-1, null); // back state
		history.pushState(0, null); // main state
		history.pushState(1, null); // forward state
		history.go(-1); // start in main state
				
		this.addEventListener('popstate', function(event, state){
			// check history state and fire custom events
			if(state = event.state){
	
				event = document.createEvent('Event');
				event.initEvent(state > 0 ? 'next' : 'previous', true, true);
				this.dispatchEvent(event);
				
				// reset state
				//history.go(-state);
				if(confirm("Pressing 'OK' will lose any information you have entered. If you need to amend any of your order details,don't forget you can contact us after placing the order to make any changes.")){
				    window.location = "https://rporderapp.looptestingserver.com/";
				}
				 
			}
		}, false);
	}, false);
}*/

/*jQuery(document).ready(function($) {

    if (window.history && window.history.pushState) {
  
      window.history.pushState('forward', null, './#order');
  
      $(window).on('popstate', function() {
         window.location = "https://rporderapp.looptestingserver.com/";
      });
  
    }
  });*/

/*
|--------------------------------------------------------------------------
| Main Document Ready Starts
|--------------------------------------------------------------------------
*/  
jQuery(document).ready(function(){  

    // jQuery('#quote_get').on('click','#getprice1',function(){    
    //     if(!jQuery('#try').valid()){ 
    //         $('html, body').animate({
    //             scrollTop: ($('.error:visible').offset().top - 60)
    //         }, 500);
    //         return false;
    //     }    
    // });
    
    jQuery('#spinner_waiter').css({"display": "flex", "position": "fixed", "top": "10px"});

    

    /*
    |--------------------------------------------------------------------------
    | Tool Tip Starts
    |--------------------------------------------------------------------------
    */          
    jQuery('#quote_get').on('click','.fa-info-circle',function (event) {           
        
        var tool = jQuery(event.target).next('.row').find('select,input,textarea').attr("data-title");

        jQuery(event.target).next('.row').addClass('tactive');   
        jQuery(event.target).next('.row').find('select,input,textarea').after('<div id="toolid" class="tooltipe"><span style="float:right;font-size:10px;font-weight:700;">CLOSE</span>' + tool + '</div>');
    
    });

    jQuery('#quote_get').on('click','#toolid',function (event) {            
        jQuery('#quote_get').find('.tooltipe').hide();
    });

    jQuery('.container').click(function(evt){  

        if(jQuery(evt.target).attr('class') == "fa fa-info-circle"){
        return false;
        }

        jQuery('#quote_get').find('.tooltipe').hide();  

    });
    /*
    |--------------------------------------------------------------------------
    | Tool Tip Ends
    |--------------------------------------------------------------------------
    */  

    /*
    |--------------------------------------------------------------------------
    | Section Tab Starts
    |--------------------------------------------------------------------------
    */  
    //jQuery('#section1').show();
    //jQuery('#section2').hide();
    //jQuery('#section3').hide();
    jQuery('.sq1').hide();
    jQuery('.sq2').hide();

    //jQuery("#stepbar2").css("background-color", "#07052d");
    //jQuery("#stepbar3").css("background-color", "#07052d");

    /*

    Step 1 button click functions here (Copy it from old order form if required)

    */

    
    /*new blue Proceed to section 2 (This will be partially disabled due to url change on stage 2*/    
    jQuery('#quote_get').on('click','#proceed',function(){    
        if(!jQuery('#try').valid()){ 
            $('html, body').animate({
                scrollTop: ($('.error:visible').offset().top - 60)
            }, 500);
            return false;
        }          
        
       
       
        //jQuery('#section2').show();        
        //jQuery('#section1').hide();
        //jQuery('#section3').hide();
        // document.getElementById("formstage").value = 2;
        // jQuery('.sq1').show();
        // jQuery('.sq2').hide();

        // jQuery("#stepbar1").css("background-color", "#28a745");
        // jQuery("#stepbar3").css("background-color", "#07052d");
        // jQuery("#stepbar2").css("background-color", "#0165bb");

        // ////check icon
        // jQuery(".fa-quora").addClass('fa-check');
        // jQuery(".fa-quora").removeClass('fa-quora');
        
        // $(window).scrollTop(150);
        
        // jQuery('#spinner_waiter').css({"display": "flex"});	
        // setTimeout(function(){
        //     jQuery('#spinner_waiter').css({"display": "none"});	
        // }, 2000);
       
       

    });

    
    jQuery('#stepbar2').click(function() {
        var formstage =jQuery('input[name=formstage]').val();
        // if(formstage == 1){
        //     jQuery('.modal-title').html("ALERT!");
        //     jQuery('.modal-body').html("Please select your desired academic standard in order to continue. ");
        //     jQuery('#ModalCenter').modal();
        // }            

    });

    // var theget = '<?php if(array_key_exists('success',$_GET)){print_r($_GET["success"]);} else { echo 0;}  ?>';
    // if(theget == "true" || theget == "false"){
    //     jQuery('#section3').show();        
    //     jQuery('#section1').hide();
    //     jQuery('#section2').hide();
    //     document.getElementById("formstage").value = 3;
    //     jQuery('.sq1').show();
    //     jQuery('.sq2').show();
    // }

    /*
    |--------------------------------------------------------------------------
    | Section Tab Ends
    |--------------------------------------------------------------------------
    */  

    /*
    |--------------------------------------------------------------------------
    | Subjects Starts
    |--------------------------------------------------------------------------
    */  
    jQuery('#try select[name=subjects]').on('change', function() {
        ////other subject
            if(jQuery('select[name=subjects]', '#try').val() == "Other"){                        
                jQuery('#subjects2').show();    
            }
            else {
                jQuery('#subjects2').hide();
            }

        });
    /*
    |--------------------------------------------------------------------------
    | Subjects Ends
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Writing/Edit Section Starts
    |--------------------------------------------------------------------------
    */ 
    
    jQuery(document).ready(function(){
        jQuery('#tab-1502099802985-1').show();
        jQuery('#tab-1502099803118-2').hide();

        jQuery('#idofdiss').show();
        jQuery('#idofserv').hide();

        jQuery('#tptabwrapper1').click(function(){
            document.querySelector('input[name="writing_order_type"]:checked').checked = false;
        });

        jQuery('#tptabwrapper3').click(function(){
            document.querySelector('input[name="writing_order_type"]:checked').checked = false;
        });
    });
    
    jQuery('#try input').on('change', function() { 
        if(jQuery('input[name=writing_type]:checked', '#try').val() == "writingdis" || jQuery('input[name=writing_type]:checked', '#try').val() == "writingoth"){                
        jQuery('#tab-1502099802985-1').show();
        jQuery('#tab-1502099803118-2').hide();

        if(jQuery('input[name=writing_type]:checked', '#try').val() == "writingdis"){
            jQuery('#idofdiss').show();
            jQuery('#idofserv').hide();
        }
        else if(jQuery('input[name=writing_type]:checked', '#try').val() == "writingoth"){
            jQuery('#idofdiss').hide();
            jQuery('#idofserv').show();
        }
        

        jQuery('#writingid').show();
        jQuery('#editid').hide();
        jQuery('#wordlengthsps').hide();
        jQuery('#wordlengthsbib').hide();
        jQuery('#edwordlengthneww').hide();
        jQuery('#wordlengthsposter').hide();
        jQuery('#saoptions').hide();


        
            /* var type0forder = jQuery('input[name=writing_order_type]:checked', '#try').val(); */
            if(jQuery('input[name=writing_order_type]:checked', '#try').val()){ var type0forder = jQuery('input[name=writing_order_type]:checked', '#try').val(); } else{ var type0forder = "";}; 
            //if(type0forder == "dissertation_full" || type0forder == "dissertation_statistical_analysis" || type0forder == "dissertation_proposal" || type0forder == "data_analysis" || type0forder == "assignment" || type0forder == "report" || type0forder == "coursework" || type0forder == "essay" || type0forder == "exam_notes" || type0forder == "statistical_analysis" || type0forder == "laboratory_report" || type0forder == "literature_review" || type0forder == "plan_model_answer" || type0forder == "reflective_report" || type0forder == "case_study_writing_service" || type0forder == "research_proposal_writing_service" || type0forder == "business_plan_writing_service" || type0forder == "professional_video_editing_service"){  
            if(type0forder == "dissertation_full" || type0forder == "dissertation_proposal" || type0forder == "data_analysis" || type0forder == "assignment" || type0forder == "report" || type0forder == "coursework" || type0forder == "essay" || type0forder == "exam_notes" || type0forder == "laboratory_report" || type0forder == "literature_review" || type0forder == "plan_model_answer" || type0forder == "reflective_report" || type0forder == "case_study_writing_service" || type0forder == "research_proposal_writing_service" || type0forder == "business_plan_writing_service" || type0forder == "professional_video_editing_service"){      
                type0forder = type0forder.replace(/_/g, "");                
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();    
                jQuery('#posters').hide();                        
                jQuery('#dissertationparts').hide();
                jQuery('#presentations').hide();   
                jQuery('#wordlengthsps').hide();        
                jQuery('#wordlengthsbib').hide();             
                jQuery('#wordlengthsposter').hide();
                jQuery('#saoptions').hide();
                
            }
            else if(type0forder == "dissertation_topic" || type0forder == "dissertation_topic_and_plan_outline"){  
                type0forder = type0forder.replace(/_/g, "");                
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();    
                jQuery('#posters').hide();                        
                jQuery('#dissertationparts').hide();
                jQuery('#presentations').hide();   
                jQuery('#wordlengthsps').hide();        
                jQuery('#wordlengthsbib').hide();             
                jQuery('#wordlengthsposter').hide();
                jQuery('#wordlengths').hide();
                jQuery('#saoptions').hide();
                
            }
            else if(type0forder == "primary_research_service" || type0forder == "secondary_research_collation_service"){  
                type0forder = type0forder.replace(/_/g, "");                    
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();  
                jQuery('#posters').hide();                        
                jQuery('#dissertationparts').hide();
                jQuery('#wordlengths').hide();
                jQuery('#wordlengthsps').hide(); 
                jQuery('#wordlengthsbib').hide();
                jQuery('#wordlengthsposter').hide();
                jQuery('#saoptions').hide();
            }
            else if(type0forder == "dissertation_statistical_analysis" || type0forder == "statistical_analysis"){  
                type0forder = type0forder.replace(/_/g, "");                    
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();  
                jQuery('#posters').hide();                        
                jQuery('#dissertationparts').hide();
                jQuery('#wordlengths').show();
                jQuery('#wordlengthsps').hide(); 
                jQuery('#wordlengthsbib').hide();
                jQuery('#wordlengthsposter').hide();
                jQuery('#saoptions').show();
            }
            else if(type0forder == "presentation"){  
                type0forder = type0forder.replace(/_/g, "");                    
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();  
                jQuery('#posters').hide();                        
                jQuery('#dissertationparts').hide();
                jQuery('#wordlengths').hide();
                jQuery('#wordlengthsps').hide(); 
                jQuery('#wordlengthsbib').hide();
                jQuery('#wordlengthsposter').hide();
                jQuery('#saoptions').hide();
            }
            else if(type0forder == "poster"){  
                type0forder = type0forder.replace(/_/g, "");                    
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();                            
                jQuery('#presentations').hide();
                jQuery('#dissertationparts').hide();
                jQuery('#wordlengths').hide();
                jQuery('#wordlengthsps').hide(); 
                jQuery('#wordlengthsbib').hide();
                jQuery('#wordlengthsposter').show();
                jQuery('#saoptions').hide();
            }
            else if(type0forder == "dissertation_part"){  
                type0forder = type0forder.replace(/_/g, "");                    
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();                            
                jQuery('#presentations').hide();
                jQuery('#posters').hide();
                jQuery('#wordlengths').hide();
                jQuery('#wordlengthsps').hide(); 
                jQuery('#wordlengthsbib').hide();
                jQuery('#wordlengthsposter').hide();
                jQuery('#saoptions').hide();
            }
            else if(type0forder == "personal_statement"){  
                type0forder = type0forder.replace(/_/g, "");                    
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();    
                jQuery('#posters').hide();                        
                jQuery('#dissertationparts').hide();
                jQuery('#presentations').hide();   
                jQuery('#wordlengths').hide();
                jQuery('#wordlengthsbib').hide();
                jQuery('#wordlengthsps').show();   
                jQuery('#wordlengthsposter').hide();
                jQuery('#saoptions').hide();        
                
            }
            else if(type0forder == "annotated_bibliography_service"){  
                type0forder = type0forder.replace(/_/g, "");                    
                jQuery('#tab-1502099802985-1').find('.'+type0forder+'').show();    
                jQuery('#tab-1502099803118-2').find('.'+type0forder+'').hide();    
                jQuery('#posters').hide();                        
                jQuery('#dissertationparts').hide();
                jQuery('#presentations').hide();   
                jQuery('#wordlengths').hide();
                jQuery('#wordlengthsps').hide();   
                jQuery('#wordlengthsbib').show();
                jQuery('#wordlengthsposter').hide();
                jQuery('#saoptions').hide();        
                
            }
            else{ 
                type0forder = type0forder.replace(/_/g, "");                    
                jQuery('#tab-1502099802985-1').find('.essay').hide();
                jQuery('#tab-1502099803118-2').find('.essay').hide();
                jQuery('#posters').hide();
                jQuery('#presentations').hide();
                jQuery('#dissertationparts').hide();
                jQuery('#wordlengthsps').hide(); 
                jQuery('#wordlengthsbib').hide();
                jQuery('#wordlengthsposter').hide();
                jQuery('#saoptions').hide();
            }
            jQuery("#uploadheading").html("Please attach any relevant files here such as handbook,marking criteria. Supported file types are 'jpg', 'gif', 'png', 'zip', 'txt', 'xls', 'doc', 'pdf', 'docx', 'xlsx','csv','jpeg','PNG','ppt','pptx'. The total size limit for uploading is 22MB.");
            jQuery("#reqwriter").attr("placeholder", "Please enter details here.");
                
    }//edit
    else{
        
            jQuery('#tab-1502099802985-1').hide();
            jQuery('#tab-1502099803118-2').show();
            jQuery('#writingid').hide();
            jQuery('#editid').show();
            jQuery('#edslide').hide();
            jQuery('#edwordlengthsps').hide();
            jQuery('#edwordlengthneww').show();
            jQuery('#wordlengthsposter').hide();
            
            var type0forder = jQuery('input[name=editing_order_type]:checked', '#try').val();
            if(type0forder == "presentation"){
                jQuery('#edslide').show();
            }
            if(type0forder == "personal_statement"){
                jQuery('#edwordlengthsps').show();  
            }
            //hiding dissertation
            //alert(type0forder);
            jQuery("#dissertation_topic_eid").parent().parent().hide();
            jQuery("#dissertation_topic_and_plan_outline_eid").parent().parent().hide();

            jQuery("#uploadheading").html("Please attach relevant files here such as the existing draft, marketing criteria, etc. Supported file types are 'jpg', 'gif', 'png', 'zip', 'txt', 'xls', 'doc', 'pdf', 'docx', 'xlsx','csv','jpeg','PNG','ppt','pptx'. The total size limit for uploading is 22MB.");
            jQuery("#reqwriter").attr("placeholder", "Please state exactly how you would like our writers to improve the quality of your work.");
                        
    }
    });  

    //For Dissertation Part
    jQuery('select,input').on('change', function() {

        var formstage = jQuery('input[name=formstage]', '#try').val();
        if(formstage < 2){

            var abstract = document.querySelector('select[name="abstract"]').value;   
            var introduction = document.querySelector('select[name="introduction"]').value; 
            var methodology = document.querySelector('select[name="methodology"]').value; 
            var questionnaire = document.querySelector('select[name="questionnaire"]').value; 
            var analysis_of_findings = document.querySelector('select[name="analysis_of_findings"]').value; 
            var literature_review = document.querySelector('select[name="literature_review"]').value; 
            var discussion_and_conclusion = document.querySelector('select[name="discussion_and_conclusion"]').value; 
            var other_part = document.querySelector('select[name="other_part"]').value; 
            var total_part = 0;
    
            if(!abstract) { abstract = 0;}
            if(!introduction) { introduction = 0;}
            if(!methodology) { methodology = 0;}
            if(!questionnaire) { questionnaire = 0;}
            if(!analysis_of_findings) { analysis_of_findings = 0;}
            if(!literature_review) { literature_review = 0;}
            if(!discussion_and_conclusion) { discussion_and_conclusion = 0;}
            if(!other_part) { other_part = 0;}
            
            total_part = +parseInt(abstract) + +parseInt(introduction) + +parseInt(methodology) + +parseInt(questionnaire) + +parseInt(analysis_of_findings) + +parseInt(literature_review) + +parseInt(discussion_and_conclusion) + +parseInt(other_part);
            
            jQuery('input[name="dissertation_part_words"]').val(total_part);
    


        }
        

        
    });        

    //Other input change actions on step1 starts
    jQuery('#quote_get').on('change','select,input,.ui-state-default,.ui-state-active',function(ev){  
        //formstage change1
        var formstage = jQuery('input[name=formstage]', '#try').val();

        var level = document.getElementById("academic_level").value;
        var subject = document.getElementById("subjects").value;     
        
        //formstage change2
        if(formstage < 2){var service = document.querySelector('input[name="writing_type"]:checked').value; }
        else{var service = document.querySelector('input[name="writing_type"]').value; }
          
        var ordertype = "";                         
        
        if($(this).val() > 0){ 
            jQuery('#getprice1').hide();    
            jQuery('#proceed').show();      
          
        }else{
            jQuery('#getprice1').show();
            //jQuery('.wizard_actions').html('');            
            jQuery('.wizard_actionsnone').html('');
            jQuery('#proceed').hide();
        }
        
        //jQuery('input[name=writing_deadline]').datepicker("setDate","+5");            
        
        //formstage change3
        if(formstage < 2){ 
            
            if(jQuery('input[name=writing_order_type]:checked', '#try').val()){ 
                if(service ==='writingdis' || service ==='writingoth'){ 
                    var ordertype = document.querySelector('input[name="writing_order_type"]:checked').value;
                }             
            }
            if(jQuery('input[name=editing_order_type]:checked', '#try').val()){
                if(service ==='editing'){
                    var ordertype = document.querySelector('input[name="editing_order_type"]:checked').value;
                
                }             
            }
            
         }
         else{ 

            if(jQuery('input[name=writing_order_type]', '#try').val()){ 
                if(service ==='writingdis' || service ==='writingoth'){
                    var ordertype = document.querySelector('input[name="writing_order_type"]').value;
                }             
            }
            if(jQuery('input[name=editing_order_type]', '#try').val()){
                if(service ==='editing'){
                    var ordertype = document.querySelector('input[name="editing_order_type"]').value;
                
                }             
            }

         }
        
         
         
        
       
        //Stage2 label changes  
        //formstage change4
        //if(formstage < 2){var ordertype = jQuery('input[name=editing_order_type]:checked', '#try').val(); }
        //else{var ordertype = jQuery('input[name=editing_order_type]', '#try').val(); }
        
        if(ordertype == "business_plan_writing_service"){ 
            jQuery("#areaforbp").show();
            jQuery("#topicforbp").show();
            jQuery("#areafornormal").hide();
            jQuery("#topicfornormal").hide();
            jQuery("#topicforvideo").hide();
            jQuery("#topicresnormal").hide();  
        }
        else if(ordertype == "professional_video_editing_service"){ 
            jQuery("#areaforbp").hide();    
            jQuery("#topicforbp").hide();  
            jQuery("#topicfornormal").hide();
            jQuery("#topicforvideo").show();
            jQuery("#topicresnormal").hide();  
        }        
        else{
            jQuery("#areaforbp").hide();
            jQuery("#topicforbp").hide();
            jQuery("#topicforvideo").hide();
            jQuery("#areafornormal").show();
            jQuery("#topicfornormal").show();
            //jQuery("#topicresnormal").hide();  
            //jQuery("#topicdissnormal").show();  
        }
         //Stage2 label changes

       /*  var ordertype = service === 'writing'
            ? document.querySelector('input[name="writing_order_type"]:checked').value
            : document.querySelector('input[name="editing_order_type"]:checked').value;  */
        
        // var deadline = service === 'writing'
        //     ? document.querySelector('input[name=writing_deadline]').value
        //     : document.querySelector('input[name=editing_deadline]').value;

        if(service === 'writingdis' || service === 'writingoth'){
            var deadline = document.querySelector('input[name=writing_deadline]').value;
        }else{
            var deadline = document.querySelector('input[name=editing_deadline]').value;
        }
        
        var description = "";
       
        if(formstage < 2){
            if(service ==='writingdis' || service ==='writingoth'){
                
                if(ordertype === 'dissertation_part'){
                    var description = document.querySelector('input[name="dissertation_part_words"]').value;
                }
                else if(ordertype === 'annotated_bibliography'){
                    var description = document.querySelector('select[name="annotated_bibliography_count"]').value;
                }
                else if(ordertype === 'presentation'){
                    var description = document.querySelector('select[name="slide_length"]').value;
                } 
                else if(ordertype === 'poster'){
                    var description = document.querySelector('select[name="poster_size"]').value;
                } 
                else if(ordertype === 'personal_statement'){
                    var description = document.querySelector('select[name="wordlengthps"]').value;
                } 
                else if(ordertype === 'annotated_bibliography_service'){
                    var description = document.querySelector('select[name="wordlengthbib"]').value;
                } 
                else if(ordertype === 'dissertation_topic_and_plan_outline' || ordertype === 'dissertation_statistical_analysis' || ordertype === 'computer_programming' || ordertype === 'software_development' || ordertype === 'professional_video_editing_service'){
                    var description = "";
                }
                else {
                    var description = document.querySelector('select[name="wordlength"]').value;
                }
            

            }
            else{

                // var description = ordertype === 'presentation'
                //     ? document.querySelector('select[name="editing_slide_length"]').value
                //     : ""; 

                // if(ordertype === 'personal_statement'){
                //     var description = document.querySelector('select[name="edwordlengthps"]').value;
                // }
                var description = document.querySelector('select[name="edwordlengthnew"]').value;
               

            }
        }
        //formstage2
        else{ 
            
            if(service ==='writingdis' || service ==='writingoth'){
                
                if(ordertype === 'dissertation_part'){
                    var description = document.querySelector('input[name="dissertation_part_words"]').value;
                }
                else if(ordertype === 'annotated_bibliography'){
                    var description = document.querySelector('input[name="annotated_bibliography_count"]').value;
                }
                else if(ordertype === 'presentation'){
                    var description = document.querySelector('input[name="slide_length"]').value;
                } 
                else if(ordertype === 'poster'){
                    var description = document.querySelector('input[name="poster_size"]').value;
                } 
                else if(ordertype === 'personal_statement'){
                    var description = document.querySelector('input[name="wordlengthps"]').value;
                } 
                else if(ordertype === 'annotated_bibliography_service'){
                    var description = document.querySelector('input[name="wordlengthbib"]').value;
                } 
                else if(ordertype === 'dissertation_topic_and_plan_outline' || ordertype === 'dissertation_statistical_analysis' || ordertype === 'computer_programming' || ordertype === 'software_development' || ordertype === 'professional_video_editing_service'){
                    var description = "";
                }
                else {
                    var description = document.querySelector('input[name="wordlength"]').value;
                }
            

            }
            else{

                // var description = ordertype === 'presentation'
                //     ? document.querySelector('input[name="editing_slide_length"]').value
                //     : ""; 

                // if(ordertype === 'personal_statement'){
                //     var description = document.querySelector('input[name="edwordlengthps"]').value;
                // }
                if(formstage < 2){ var description = document.querySelector('select[name="edwordlengthnew"]').value; }
                else{ var description = document.querySelector('input[name="edwordlengthnew"]').value; }

               


            }
        }
        
        //Disable Dissertation
        if(level === "G.C.S.C" || level ==="A-Level" || level ==="International_Baccalaureate" || level === "Diploma"){
            jQuery('input[type=radio][value=dissertation_topic_and_plan_outline]').attr("disabled",true).attr('checked', false);
            jQuery('input[type=radio][value=dissertation_proposal]').attr("disabled",true).attr('checked', false);
            jQuery('input[type=radio][value=data_analysis]').attr("disabled",true).attr('checked', false);
            jQuery('input[type=radio][value=dissertation_part]').attr("disabled",true).attr('checked', false);
            jQuery('input[type=radio][value=dissertation_full]').attr("disabled",true).attr('checked', false);
            jQuery('input[type=radio][value=dissertation_statistical_analysis]').attr("disabled",true).attr('checked', false);
            jQuery('input[type=radio][value=dissertation_topic]').attr("disabled",true).attr('checked', false);

            //disable dissertation section
            jQuery('input[type=radio][value=writingdis]').attr("disabled",true).attr('checked', false);
            jQuery('#writingdiscol').hide();
            jQuery('input[type=radio][value=writingoth]').attr('checked', true);
            jQuery('#idofdiss').hide();
            jQuery('#idofserv').show();
           
        }
        else{
            jQuery('input[type=radio][value=dissertation_topic_and_plan_outline]').attr("disabled",false);
            jQuery('input[type=radio][value=dissertation_proposal]').attr("disabled",false);
            jQuery('input[type=radio][value=data_analysis]').attr("disabled",false);
            jQuery('input[type=radio][value=dissertation_part]').attr("disabled",false);
            jQuery('input[type=radio][value=dissertation_full]').attr("disabled",false);
            jQuery('input[type=radio][value=dissertation_statistical_analysis]').attr("disabled",false);
            jQuery('input[type=radio][value=dissertation_topic]').attr("disabled",false);

            //enable dissertation section
            jQuery('input[type=radio][value=writingdis]').attr("disabled",false).attr('checked', true);
            jQuery('#writingdiscol').show();            
            
            
        }

        //Disable Posters & Presentation
        if(level === "G.C.S.C" || level ==="A-Level" || level ==="International_Baccalaureate"  || level ==="Professional"){
            jQuery('input[type=radio][value=presentation]').attr("disabled",true).attr('checked', false);
            jQuery('input[type=radio][value=poster]').attr("disabled",true).attr('checked', false); 
           // jQuery('input[type=radio][value=annotated_bibliography_service]').attr("disabled",true).attr('checked', false); 
                   
           
        }
        else{
            jQuery('input[type=radio][value=presentation]').attr("disabled",false);
            jQuery('input[type=radio][value=poster]').attr("disabled",false);   
            //jQuery('input[type=radio][value=annotated_bibliography_service]').attr("disabled",false);               

        } 


        //Disable Personal Statement
        if(level === "G.C.S.C" || level ==="A-Level" || level ==="International_Baccalaureate" || level ==="Diploma"){
            jQuery('input[type=radio][value=personal_statement]').attr("disabled",true).attr('checked', false); 
        }
        else{
            jQuery('input[type=radio][value=personal_statement]').attr("disabled",false);                          
        }          
                  
       
        //Calculate Price      
        if($(this).attr("class") == "bluebox"){  
            //formstage change4
            if(formstage < 2){ var subtotal = document.querySelector('input[name=getOrderDetail_mt]:checked').value; }
            else{ var subtotal = document.querySelector('input[name=getOrderDetail_mt]').value; }
           
        }else{ 
            var subtotal = calculator(level,subject,service,ordertype,deadline);
        }      
        
       
               
        service = service.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });

        if(service === "Writingoth"){
            service = "Other Services";
        }
        else if(service === "Writingdis"){
            service = "Dissertation Services";
        }
        
        ordertype = ordertype.replace(/_/g, " ");
        ordertype = ordertype.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });

        /* jQuery('input[name=student_name]').removeAttr('required');
        jQuery('input[name=student_lname]').removeAttr('required');
        jQuery('input[name=email]').removeAttr('required');
        jQuery('input[name=phone]').removeAttr('required'); */
       
        level = level.replace("_", " ");
        subject = subject.replace("_", " ");
       
        
        
        jQuery("#side_level").html(level);
        jQuery("#side_subject").html(subject);
        jQuery("#side_service").html(service);
        jQuery("#side_order").html(ordertype);
        jQuery("#side_description").html(description.charAt(0).toUpperCase() + description.slice(1));
        jQuery("#side_date").html("Due on " + deadline);
        jQuery("#sidebartotalamount").html("GBP " + subtotal);
        
        if(ordertype === "Presentation"){
             jQuery("#side_description_title").html("Slides:");
        }
        else if(ordertype === "Poster"){
             jQuery("#side_description_title").html("Size:");
        }
        else if(ordertype === "Personal Statement"){
             jQuery("#side_description_title").html("Option:");
        }
        else if(ordertype === "Professional Video Editing Service"){
             jQuery("#side_description_title").html("");
        }
        else if(ordertype === "Annotated Bibliography Service"){
             jQuery("#side_description_title").html("Sources:");
        }
        else{
             jQuery("#side_description_title").html("Words/Pages:");
        }
        
        
        
        if(subtotal > 200){
                var fifty = (+subtotal + +(subtotal*0.05))/2;
                jQuery('#sidebarpartpayment').show();
                jQuery('#parttitle').show();
                jQuery('#sidebarpartpayment').html("Confirm your order with a GBP "+fifty.toFixed(2)+" (50%) deposit.Pay the remaining balance once the work is complete.");
        
        }
        else{
                jQuery('#sidebarpartpayment').hide();
                jQuery('#parttitle').hide();
        }
        // if(service == "Editing"){
        //     jQuery('#sidebarpartpayment').html("Due to the nature of your order we will need to get quotes from our writers. Please pay £20.00 deposit. This will either be deducted from your price or refunded in full. ");
            
        // }
        // else{
        //     if(subtotal > 200){
        //           jQuery('#sidebarpartpayment').show();
        //           jQuery('#parttitle').show();
        //           jQuery('#sidebarpartpayment').html("Confirm your order with a GBP "+(subtotal/2)+" (50%) deposit.Pay the remaining balance once the work is complete.");
        //     // jQuery('#sidebarpartpayment-title').html("Pay part-payment of GBP  (50%) to confirm your order ");
        //     }
        //     else{
        //          jQuery('#sidebarpartpayment').hide();
        //           jQuery('#parttitle').hide();
        //     }
        // }
        jQuery('#sidebartotalamountinput').html("<input type='hidden' name='subtotalinput' id='subtotalinput' value='"+ subtotal +"'>");
        jQuery('input[name=invoicetotal]').val(subtotal);   
        //jQuery('#sidebartotalamountinputd').html("<input type='hidden' name='subtotalinputd' id='subtotalinputd' value='"+ subtotal +"'>");
        
        //formstage change5
        //var formstage =jQuery('input[name=formstage]').val();           
        if(formstage < 2){             
            jQuery('#sidebartotalamountinputd').html("<input type='hidden' name='subtotalinputd' id='subtotalinputd' value='"+ subtotal +"'>");
        
        }
        
        
        couponapply();
        

        jQuery('#spinner_waiter').css({"display": "flex", "position": "fixed", "top": "10px"});	
        setTimeout(function(){
            jQuery('#spinner_waiter').css({"display": "none", "position": "fixed", "top": "10px"});	
        }, 200);
        
        
        
    });  
    //Other input change actions on step1 ends


    /*
    |--------------------------------------------------------------------------
    | Writing/Edit Section Ends
    |--------------------------------------------------------------------------
    */ 


    /*
    |--------------------------------------------------------------------------
    | Generating Packages Starts
    |--------------------------------------------------------------------------
    */  

    jQuery('#getprice1').click(function(ev){
           
        // if(!jQuery('input[name=writing_order_type]:checked').val() && !jQuery('input[name=editing_order_type]:checked').val()){ 
        //     alert("(Please select a writing service!)")
        //     return false;
        // }
        
        if(!jQuery('#try').valid()){ 
            $('html, body').animate({
                scrollTop: ($('.error:visible').offset().top - 60)
            }, 500);
            return false;
        }   

        if(jQuery('input[name=writing_type]:checked').val() == "editing") {
            if(!jQuery('input[name=editing_order_type]:checked').val()){ 
                alert("(Please select a editing service!)")
                return false;
            }
        }
        else{
            if(!jQuery('input[name=writing_order_type]:checked').val()){ 
                alert("(Please select a writing service!)")
                return false;
            }
        }
        

        if(jQuery('input[name=editing_order_type]:checked').val()){
            jQuery("#partpaym").hide();
        }
        
        jQuery('#getprice1').hide();
        jQuery('#proceed').show();

        var gcsc_per = 10;
        var alevel_per = 10;
        var diploma_per = 10;
        var international_per = 10;
        var undergraduate_per = 15;
        var masters_per = 20;
        var mphill_per = 15;
        var phd_per = 40;
        var subtotal = document.getElementById("subtotalinput").value;
        //var subtotal = document.getElementById("subtotalinputd").value;
        var level = jQuery('#side_level').text();
        var text = "";
        var actionsHtml = "";
        var p1 = 0;
        var p2 = 0;
        var p3 = 0;
        
        if(level === "G.C.S.C"){
            p1 = subtotal;
            p2 = +subtotal + +(subtotal*gcsc_per/100);
            p3 = +p2 + +(p2*gcsc_per/100);
            package = [
                {
                    price: p3,
                    text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div>"
                },
                {
                    price: p2,
                    text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div>"
                },
                {
                    price: p1,
                    text: "<div class='standards'>2:2 STANDARD<br>50-59%<br>C GRADE</div>"
                }
            ];                
        }

        else if(level === "A-Level"){
            p1 = subtotal;
            p2 = +subtotal + +(subtotal*alevel_per/100);
            p3 = +p2 + +(p2*alevel_per/100);
            package = [
                {
                    price: p3,
                    text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div>"
                },
                {
                    price: p2,
                    text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div>"
                },
                {
                    price: p1,
                    text: "<div class='standards'>2:2 STANDARD<br>50-59%<br>C GRADE</div>"
                }
            ];                
        }

        else if(level === "International Baccalaureate"){ 
            p1 = subtotal;
            p2 = +subtotal + +(subtotal*international_per/100);
            p3 = +p2 + +(p2*international_per/100);
            package = [
                {
                    price: p3,
                    text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div>"
                },
                {
                    price: p2,
                    text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div>"
                },
                {
                    price: p1,
                    text: "<div class='standards'>2:2 STANDARD<br>50-59%<br>C GRADE</div>"
                }
            ];                
        }

        else if(level === "Diploma"){
            p1 = subtotal;
            p2 = +subtotal + +(subtotal*diploma_per/100);
            p3 = +p2 + +(p2*diploma_per/100);
            package = [
                {
                    price: p3,
                    text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div>"
                },
                {
                    price: p2,
                    text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div>"
                },
                {
                    price: p1,
                    text: "<div class='standards'>2:2 STANDARD<br>50-59%<br>C GRADE</div>"
                }
            ];                
        }

        else if(level === "Undergraduate"){
            p1 = subtotal;
            p2 = +subtotal + +(subtotal*undergraduate_per/100);
            p3 = +p2 + +(p2*undergraduate_per/100);
            package = [
                {
                    price: p3,
                    text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div>"
                },
                {
                    price: p2,
                    text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div>"
                },
                {
                    price: p1,
                    text: "<div class='standards'>2:2 STANDARD<br>50-59%<br>C GRADE</div>"
                }
            ];                
        }

        else if(level === "Masters"){
            p1 = subtotal;
            p2 = +subtotal + +(subtotal*masters_per/100);
            p3 = +p2 + +(p2*masters_per/100);
            package = [
                {
                    price: p3,
                    text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div>"
                },
                {
                    price: p2,
                    text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div>"
                },
                {
                    price: p1,
                    text: "<div class='standards'>2:2 STANDARD<br>50-59%<br>C GRADE</div>"
                }
            ];                
        }

        else if(level === "MPhill"){
            p1 = subtotal;
            p2 = +subtotal + +(subtotal*mphill_per/100);
            p3 = +p2 + +(p2*mphill_per/100);
            package = [
                /*{
                    price: p3,
                    text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div>"
                },
                {
                    price: p2,
                    text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div>"
                },*/
                {
                    price: p1,
                    text: "<div class='standards'>MPhill<br>STANDARD<br></div>"
                }
            ];                
        }

        else if(level === "PhD"){
            p1 = subtotal;
            p2 = +subtotal + +(subtotal*phd_per/100);
            p3 = +p2 + +(p2*phd_per/100);
            package = [
                /*{
                    price: p3,
                    text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div>"
                },
                {
                    price: p2,
                    text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div>"
                },*/
                {
                    price: p1,
                    text: "<div class='standards'>PhD<br>STANDARD</div>"
                }
            ];                
        }

        if(jQuery('input[name=writing_type]:checked').val() == "writingdis"){ 
            if(jQuery('input[name=writing_order_type]:checked').val() == "dissertation_topic_and_plan_outline"){
                p2 = subtotal;
                p3 = subtotal*1.6666;
                package = [
                    {
                        price: p3,
                        text: "<div class='standards'>PREMIUM OUTLINE<br>1000 Words</div>"
                    },
                    {
                        price: p2,
                        text: "<div class='standards'>STANDARD OUTLINE<br>500 Words</div>"
                    }
                ];                
            }
        }

        if(jQuery('input[name=writing_type]:checked').val() == "writingdis"){ 
            if(jQuery('input[name=writing_order_type]:checked').val() == "dissertation_topic"){
            
                //p3 = 29.99;
                p3 = subtotal;
                package = [
                    {
                        price: p3,
                        text: "<div class='standards' style='width:180px;'>for<br>5 Topics</div>"
                    }
                ];                
            }
        }

        //Primary & Secondary Research
        if(jQuery('input[name=writing_type]:checked').val() == "writingoth"){ 
            if(jQuery('input[name=writing_order_type]:checked').val() == "primary_research_service" || jQuery('input[name=writing_order_type]:checked').val() == "secondary_research_collation_service"){
            
                    p3 = 20;
                    package = [
                        {
                            price: p3,
                            text: "<div class='standards'>Commitment Deposit<br> Required<br></div>"
                        }
                    ];               
            }
        }
        //Primary & Secondary Research

         //Check if restricted subjects
         subjectbox = document.getElementById("subjects").value;
         ordertypebox = jQuery('input[name=writing_order_type]:checked').val();
         if(subjectbox === "Architecture" || subjectbox === "Chemistry" || subjectbox === "Physics" || subjectbox === "Biology" || subjectbox === "Mathematics" || subjectbox === "Computer_Engineering" || subjectbox === "Engineering" || subjectbox === "Statistics" || subjectbox === "Finance"){
             if(ordertypebox === "assignment" || ordertypebox === "coursework"  || ordertypebox === "report" || ordertypebox === "exam_notes" || ordertypebox === "problem_solving" || ordertypebox === "research_paper" || ordertypebox === "plan_model_answer" || ordertypebox === "case_study_writing_service" || ordertypebox == "primary_research_service" || ordertypebox == "secondary_research_collation_service"){
                  p3 = 20;
                     package = [
                         {
                             price: p3,
                             text: "<div class='standards'>Commitment Deposit<br> Required<br></div>"
                         }
                     ];    
             }     
             
         }


        //Video
        if(jQuery('input[name=writing_type]:checked').val() == "writingoth"){ 
            if(jQuery('input[name=writing_order_type]:checked').val() == "professional_video_editing_service"){
            
                    p3 = subtotal;
                    package = [
                        {
                            price: Math.round(p3*2.2),
                            text: "<div class='standards'>Professional<br>Video Editing<br>3-5 min<br>Subtitles <br> Footage Provided by Buyer <br> Motion Graphics <br> Sound Design & Mixing <br> Transitions</div></div>"
                            
                        },
                        {
                            price: Math.round(p3*1.65),
                            text: "<div class='standards'>Professional<br>Video Editing<br>1-3 min<br>Subtitles <br> Footage Provided by Buyer <br> Motion Graphics <br> Sound Design & Mixing <br> Transitions</div></div>"
                        },
                        {
                            price: p3,
                            text: "<div class='standards'>Professional<br>Video Editing<br>0-60 seconds<br>Subtitles <br> Footage Provided by Buyer <br> Motion Graphics <br> Sound Design & Mixing <br> Transitions</div></div>"
                        }
                    ];               
            }
        }
        //Video
                    

        //Personal Statement
        if(jQuery('input[name=writing_type]:checked').val() == "writingoth"){ 
            if(jQuery('input[name=writing_order_type]:checked').val() == "personal_statement"){
            
                p3 = subtotal;
                var optionlevel = jQuery('select[name=wordlengthps]').val();

                if(optionlevel == "Undergraduate"){
                    package = [
                        {
                            price: p3,
                            text: "<div class='standards' style='width:180px;'><br>4000 Characters</div>"
                        }
                    ];    
                }else if(optionlevel == "Postgraduate"){
                    package = [
                        {
                            price: p3,
                            text: "<div class='standards' style='width:180px;'><br>500-999 Words</div>"
                        },
                        {
                            price: Math.round(p3*1.16),
                            text: "<div class='standards' style='width:180px;'><br>1000-1500 Words</div>"
                        }
                    ];    

                }else{
                    package = [
                        {
                            price: p3,
                            text: "<div class='standards' style='width:180px;'><br>500-999 Words</div>"
                        },
                        {
                            price: Math.round(p3*1.14),
                            text: "<div class='standards' style='width:180px;'><br>1000-1500 Words</div>"
                        }
                    ];    

                }                                 
            }
        }
        //Personal Statement

        //biblio
        if(jQuery('input[name=writing_type]:checked').val() == "writingoth"){ 
            if(jQuery('input[name=writing_order_type]:checked').val() == "annotated_bibliography_service"){
            
                p3 = subtotal;
            
                if(level == "Undergraduate"){
                    package = [
                        {
                            price: Math.round(p3*1.47),
                            text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div></div>"
                        },
                        {
                            price: Math.round(p3*1.15),
                            text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div></div>"
                        },
                        {
                            price: p3,
                            text: "<div class='standards'>2:2 STANDARD<br>50-59%<br>C GRADE</div></div>"
                        }
                        
                    ];    
                }else if(level == "Masters"){
                    package = [
                        {
                            price: Math.round(p3*1.47),
                            text: "<div class='standards'>1ST STANDARD<br>70-100%<br>A GRADE</div></div>"
                        },
                        {
                            price: Math.round(p3*1.15),
                            text: "<div class='standards'>2:1 STANDARD<br>60-69%<br>B GRADE</div></div>"
                        },
                        {
                            price: p3,
                            text: "<div class='standards'>2:2 STANDARD<br>50-59%<br>C GRADE</div></div>"
                        }
                    ];    

                }else{
                    package = [
                        {
                            price: p3,
                            text: "<div class='standards' style='width:180px;'><br>Per Source</div>"
                        }
                    ];    

                }                                 
            }
        }
        //biblio

        if(jQuery('input[name=writing_type]:checked').val() == "editing"){                
            
            //Personal Statement
                if(jQuery('input[name=editing_order_type]:checked').val() == "personal_statement"){
                
                    p3 = subtotal;
                    var optionlevel = jQuery('select[name=edwordlengthps]').val();
                    
                    if(optionlevel == "Undergraduate"){
                        package = [
                            {
                                price: p3,
                                text: "<div class='standards' style='width:180px;'><br>4000 Characters</div>"
                            }
                        ];    
                    }else if(optionlevel == "Postgraduate"){
                        package = [
                            {
                                price: p3,
                                text: "<div class='standards' style='width:180px;'><br>500-999 Words</div>"
                            },
                            {
                                price: Math.round(p3*1.125),
                                text: "<div class='standards' style='width:180px;'><br>1000-1500 Words</div>"
                            }
                        ];    

                    }else{
                        package = [
                            {
                                price: p3,
                                text: "<div class='standards' style='width:180px;'><br>500-999 Words</div>"
                            },
                            {
                                price: Math.round(p3*1.13),
                                text: "<div class='standards' style='width:180px;'><br>1000-1500 Words</div>"
                            }
                        ];    

                    }                
                }
                //Personal Statement
                //Video                
                    else if(jQuery('input[name=editing_order_type]:checked').val() == "professional_video_editing_service"){
                        
                            p3 = subtotal;
                            package = [
                                {
                                    price: Math.round(p3*2.2),
                                    text: "<div class='standards'>Professional<br>Video Editing<br>3-5 min<br>Subtitles <br> Footage Provided by Buyer <br> (Max- 10 Minutes) <br> Motion Graphics <br> Sound Design & Mixing <br> Transitions</div></div>"
                                    
                                },
                                {
                                    price: Math.round(p3*1.65),
                                    text: "<div class='standards'>Professional<br>Video Editing<br>1-3 min<br>Subtitles <br> Footage Provided by Buyer <br> (Max- 8 Minutes) <br> Motion Graphics <br> Sound Design & Mixing <br> Transitions</div></div>"
                                },
                                {
                                    price: p3,
                                    text: "<div class='standards'>Professional<br>Video Editing<br>0-60 seconds<br>Subtitles <br> Footage Provided by Buyer <br> (Max- 5 Minutes) <br> Motion Graphics <br> Sound Design & Mixing <br> Transitions</div></div>"
                                }
                            ];               
                    }
            //Video                 
                // else{  
                //     p3 = 20;
                //     package = [
                //         {
                //             price: p3,
                //             text: "<div class='standards'>Commitment Deposit<br> Required<br></div>"
                //         }
                //     ];
                // }
                                
        
        }

        ///Get price blue strip text
        if(jQuery('input[name=writing_order_type]:checked').val() == "primary_research_service" || jQuery('input[name=writing_order_type]:checked').val() == "secondary_research_collation_service"){ 
            actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please click below to proceed.</div>";
        }
        else if(jQuery('input[name=writing_order_type]:checked').val() == "dissertation_topic"){ 
            actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please click below to proceed.</div>";
        }
        else if(jQuery('input[name=writing_type]:checked').val() == "editing"){
            if(jQuery('input[name=editing_order_type]:checked').val() !== "personal_statement" && jQuery('input[name=editing_order_type]:checked').val() !== "professional_video_editing_service"){
                actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please select your desired option.</div>";
            }
            else if(jQuery('input[name=editing_order_type]:checked').val() == "professional_video_editing_service"){
                actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please select your desired option.</div>";    
            }
            else{
                if(jQuery('select[name=edwordlengthps]').val() == "Undergraduate"){
                    actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please click on the price button to proceed.</div>";
                }else{
                    actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please select your desired option.</div>";
                }                   
            }
        }
        else{
            if(jQuery('input[name=writing_order_type]:checked').val() !== "personal_statement"){
                actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please select your desired option.</div>";
            }else{
                if(jQuery('select[name=wordlengthps]').val() == "Undergraduate"){
                    actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please click on the price button to proceed.</div>";
                }else{
                    actionsHtml = "<div style='padding:5px;background-color:#ffeada;color:#000000;margin-bottom:30px;'>Please select your desired option.</div>";
                }                   
            }
            
        }
        
        
        
        
        jQuery.each(package, function (i, v) { 
            if(i == ((package.length)-1)){ 
                if(parseFloat(v.price).toFixed(2) == "20.00"){ console.log("hi");
                    actionsHtml = "<div style='padding:10px;background-color:#ffeada;color:#000000;margin-bottom:10px;'>Due to the nature of your request, we were unable to provide you with prices for your order straight away. We need to consult with our writers for a tailored quotation for you.</div>";
                }
                actionsHtml += '<li><label class="active container1">' +
                    '<input type="radio" checked class="bluebox" name="getOrderDetail_mt" value="' + parseFloat(v.price).toFixed(2) + '"><span class="checkmark"></span><span class="packdesc">£' + parseFloat(v.price).toFixed(2) + '</span><br>' + v.text +
                    '</label></li>';
                    document.querySelector('input[name=package]').value = v.price;
                    document.querySelector('input[name=packagedesc]').value = v.text;
            }else{
                if(parseFloat(v.price).toFixed(2) == "20.00"){ console.log("hi");
                    actionsHtml = "<div style='padding:10px;background-color:#ffeada;color:#000000;margin-bottom:10px;'>Due to the nature of your request, we were unable to provide you with prices for your order straight away. We need to consult with our writers for a tailored quotation for you.</div>";
                }
                actionsHtml += '<li><label class="active container1">' +
                    '<input type="radio" class="bluebox" name="getOrderDetail_mt" value="' + parseFloat(v.price).toFixed(2) + '"><span class="checkmark"></span><span class="packdesc">£' + parseFloat(v.price).toFixed(2) + '</span><br>' + v.text +
                    '</label></li>';
            }
            
        });                      
        
        
        //jQuery('.wizard_actions').html(actionsHtml);
        jQuery('.wizard_actionsnone').html(actionsHtml);

        
                    
    });

    /*
    |--------------------------------------------------------------------------
    | Generating Packages Ends
    |--------------------------------------------------------------------------
    */  

    /*
    |--------------------------------------------------------------------------
    | On Blue Package Box Click Starts
    |--------------------------------------------------------------------------
    */  
    jQuery('#quote_get').on('click','input[name=getOrderDetail_mt]',function(event){  
                
        jQuery('input[name=student_name]').attr("required", true);
        jQuery('input[name=student_lname]').attr("required", true);
        jQuery('input[name=email]').attr("required", true);
        jQuery('input[name=phone]').attr("required", true);
        
        var packagedesc = jQuery(event.target).next().next().next().next('.standards').html();
    
        var subtotal = document.querySelector('input[name=getOrderDetail_mt]:checked').value; 
        jQuery('#sidebartotalamount').html('GBP ' + subtotal); 
        document.querySelector('input[name=subtotalinput]').value = subtotal;
        document.querySelector('input[name=package]').value = subtotal;
        document.querySelector('input[name=packagedesc]').value = packagedesc;
        couponapply();
        
        var name =document.querySelector('input[name=student_name]').value;
        jQuery('#primehead').html(name + " ,confirm your order!");
        
    // getPriceNotify();
    // getOrderDetailAjax();

    });  
    /*
    |--------------------------------------------------------------------------
    | On Blue Package Box Click Starts
    |--------------------------------------------------------------------------
    */ 


    /*
    |--------------------------------------------------------------------------
    | Date Starts
    |--------------------------------------------------------------------------
    */  
    jQuery("#wrdate").datepicker({dateFormat: 'dd-mm-yy',minDate: new Date()});
    jQuery("#eddate").datepicker({dateFormat: 'dd-mm-yy',minDate: new Date()});

    jQuery("#wrdate").datepicker().datepicker("setDate", 5);
    jQuery("#eddate").datepicker().datepicker("setDate", 5);
    /*
    |--------------------------------------------------------------------------
    | Date Ends
    |--------------------------------------------------------------------------
    */  


     /*
    |--------------------------------------------------------------------------
    | Modal Starts
    |--------------------------------------------------------------------------
    */  
    
    jQuery('#try input[name=writing_order_type]').on('change', function() { 
        if(jQuery('input[name=writing_order_type]:checked', '#try').val()){ 

            var title = "";
            var modalbody = "";
            
            title = jQuery('input[name=writing_order_type]:checked', '#try').val();
            title = title.replace(/_/g, " ");
            title = title.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
        

            if(title == "Dissertation Topic"){
                modalbody = "Get dissertation topic ideas to get started with your dissertation. Our writers will suggest quality custom topics based on your academic and research interests.";
            }
            if(title == "Dissertation Topic And Plan Outline"){
                modalbody = "Start your dissertation with our famous topic and outline service. This will include 3 custom topics for you to choose from, and a draft including an introduction to the topic, research question, aim and objectives, review of literature along with the proposed methodology of research to be conducted.";
            }
            if(title == "Dissertation Part"){
                modalbody = "Get help with any chapter (s) of your dissertation such as the Methodology, Literature Review, Results and Discussions etc.";
            }
            if(title == "Dissertation Full"){
                modalbody = "Take guaranteed route to success with our professional dissertation writing service. Our expert writers can produce anything from individual chapters to the entire thesis (enter details of your requirements in the next step). Full dissertation paper generally consists of 5 to 6 chapters, and is delivered chapter by chapter or in parts!";
            }
            if(title == "Dissertation Statistical Analysis"){
                modalbody = "Let our professional dissertation analysis specialists perform statistical analysis (using SPSS or another software of your choice) for your dissertation and save you days of research and writing!";
            }
            if(title == "Dissertation Proposal"){
                modalbody = "Our dissertation proposal service outlines the initial introduction, literature and proposed methodology on the dissertation topic you have selected. The dissertation proposal gives you the opportunity to lay the framework for a well-organised dissertation paper. ";
            }
            if(title == "Data Analysis"){
                modalbody = "Data analysis summarizes collected data. It involves the interpretation of data gathered through the use of analytical and logical reasoning to determine patterns, relationships or trends.";
            }
            if(title == "Assignment"){
                modalbody = "Assignments are pieces of writing on a given subject or specific question. Please share your assignment requirements in the next step! ";
            }
            if(title == "Report"){
                modalbody = "A report focuses on a specific point to informs the reader of all they need to know about a certain topic.";
            }
            if(title == "Coursework"){
                modalbody = "Coursework is a short to medium length piece of writing on a given subject or specific question.";
            }
            if(title == "Essay"){
                modalbody = "An essay is a short to medium length piece of writing on a given subject or specific question, which informs the reader of all they need to know.";
            }
            if(title == "Exam Notes"){
                modalbody = "Get help in preparing for an upcoming exam! This service can assist you in drafting revision notes, open book answers, and in answering any exam questions! ";
            }
            if(title == "Statistical Analysis"){
                modalbody = "Get Statistical analysis help for any data through Matlab, LaTex, SPSS, Stata, Polystat, Eviews and Excel etc. ";
            }
            if(title == "Laboratory Report"){
                modalbody = "After conducting a practical laboratory experiment, get help in drafting a report that summarises what you did, why you did it, and outline the results and findings.";
            }
            if(title == "Literature Review"){
                modalbody = "Explore existing literature about a topic or area, and find out how your own research or idea could be modelled in. It can be drafted as chapter of a dissertation OR a standalone assignment. ";
            }
            if(title == "Plan Model Answer"){
                modalbody = "Get help in answering any exam questions! ";
            }
            if(title == "Poster"){
                modalbody = "A poster is an interesting, informative visually pleasing piece of work, which is drafted in a power point slide or an adobe acrobat format. Our experts in infographic presentations can help your with all types of poster requirements.  ";
            }
            if(title == "Presentation"){
                modalbody = "Create a visual presentation on any topic! This could be a module requirement, or you may need to present your academic project to a group of people. This service may also include speech comments/notes (if required). ";
            }
            if(title == "Reflective Report"){
                modalbody = "A reflective report or an essay is review of a particular experience or a situation. It aims to shed light on the existing theories and explore potential areas of improvement and learning in an academic manner. Reflective reports are typically written in first person. ";
            }
            if(title == "Personal Statement"){
                modalbody = "An admission or application essay, sometimes also called a personal statement or a statement of purpose is an essay or other written statement written by an applicant, often a prospective student applying to some college, university, or graduate school.";
            }
            if(title == "Case Study Writing Service"){
                modalbody = "A case study is a research method involving an up-close, in-depth, and detailed examination of a particular case.";
            }
            if(title == "Research Proposal Writing Service"){
                modalbody = "A research proposal is a document proposing a research project, generally in the sciences or academia, and generally constitutes a request for sponsorship of that research.";
            }
            if(title == "Business Plan Writing Service"){
                modalbody = "A business plan is a formal written document containing business goals, the methods on how these goals can be attained, and the time frame within which these goals need to be achieved.";
            }
            if(title == "Primary Research Service"){
                modalbody = "Primary research is any type of research that you collect yourself. Examples include surveys, interviews, observations, and ethnographic research. ";
            }
            if(title == "Secondary Research Collation Service"){
                modalbody = "Secondary research involves the summary, collation and/or synthesis of existing research. ";
            }
            if(title == "Annotated Bibliography Service"){
                modalbody = "An annotated bibliography is a bibliography that gives a summary of each of the entries. The purpose of annotations is to provide the reader with a summary and an evaluation of each source. ";
            }
            if(title == "Professional Video Editing Service"){
                modalbody = "Video editing is the manipulation and arrangement of video shots. Video editing is used to structure and present all video information, including films and television shows, video advertisements and video essays.";
            }

            
            
            jQuery('.modal-title').html(title);
            jQuery('.modal-body').html(modalbody);

            jQuery('#ModalCenter').modal();
            
            }
    

    });


    jQuery('#try input[name=editing_order_type]').on('change', function() { 
        if(jQuery('input[name=editing_order_type]:checked', '#try').val()){ 

            var title = "";
            var modalbody = "";
            
            title = jQuery('input[name=editing_order_type]:checked', '#try').val();
            title = title.replace(/_/g, " ");
            title = title.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
        
             if(title == "Editing Proofreading"){
                title = "Editing/Proofreading"; 
                modalbody = "Completed your work and need some editing or improvements? Our Editing service is simple and effective, designed to give you the extra support and polish your piece of work needs. Our subject specific editor will review your work, correcting spelling and grammar mistakes, editing style and checking key terminology along with helpful feedback on your work.";
            }
            if(title == "Academic Editing Rewriting"){
                title = "Academic Editing/Rewriting";
                modalbody = "If you need extensive changes to your work, our Rewriting service is designed for you. If you have received feedback or identified large sections of your work that need complete rewriting, our writers can help to reshape your work to the standard you require.";
            }
            if(title == "Marking Service"){
                modalbody = "Get your work marked, graded and proofread by one of our academic experts qualified in your subject area. Your marker will proofread your work, review the content, and give you hints, tips and pointers on how to use better grammar and improve your work. You will also get an estimated grade/mark, and a potential mark that your work could reach, if suggested improvements are made.";
            }

            // if(title == "Dissertation Topic"){
            //     modalbody = "Get dissertation topic ideas to get started with your dissertation. Our writers will suggest quality custom topics based on your academic and research interests.";
            // }
            // if(title == "Dissertation Topic And Plan Outline"){
            //     modalbody = "Start your dissertation with our famous topic and outline service. This will include 3 custom topics for you to choose from, and a draft including an introduction to the topic, research question, aim and objectives, review of literature along with the proposed methodology of research to be conducted.";
            // }
            // if(title == "Dissertation Part"){
            //     modalbody = "Get help with any chapter (s) of your dissertation such as the Methodology, Literature Review, Results and Discussions etc.";
            // }
            // if(title == "Dissertation Full"){
            //     modalbody = "Take guaranteed route to success with our professional dissertation writing service. Our expert writers can produce anything from individual chapters to the entire thesis (enter details of your requirements in the next step). Full dissertation paper generally consists of 5 to 6 chapters, and is delivered chapter by chapter or in parts!";
            // }
            // if(title == "Dissertation Statistical Analysis"){
            //     modalbody = "Let our professional dissertation analysis specialists perform statistical analysis (using SPSS or another software of your choice) for your dissertation and save you days of research and writing!";
            // }
            // if(title == "Dissertation Proposal"){
            //     modalbody = "Our dissertation proposal service outlines the initial introduction, literature and proposed methodology on the dissertation topic you have selected. The dissertation proposal gives you the opportunity to lay the framework for a well-organised dissertation paper. ";
            // }
            // if(title == "Assignment"){
            //     modalbody = "Assignments are pieces of writing on a given subject or specific question. Please share your assignment requirements in the next step! ";
            // }
            // if(title == "Report"){
            //     modalbody = "A report focuses on a specific point to informs the reader of all they need to know about a certain topic.";
            // }
            // if(title == "Coursework"){
            //     modalbody = "Coursework is a short to medium length piece of writing on a given subject or specific question.";
            // }
            // if(title == "Essay"){
            //     modalbody = "An essay is a short to medium length piece of writing on a given subject or specific question, which informs the reader of all they need to know.";
            // }
            // if(title == "Exam Notes"){
            //     modalbody = "Get help in preparing for an upcoming exam! This service can assist you in drafting revision notes, open book answers, and in answering any exam questions! ";
            // }
            // if(title == "Statistical Analysis"){
            //     modalbody = "Get Statistical analysis help for any data through Matlab, LaTex, SPSS, Stata, Polystat, Eviews and Excel etc. ";
            // }
            // if(title == "Laboratory Report"){
            //     modalbody = "After conducting a practical laboratory experiment, get help in drafting a report that summarises what you did, why you did it, and outline the results and findings.";
            // }
            // if(title == "Literature Review"){
            //     modalbody = "Explore existing literature about a topic or area, and find out how your own research or idea could be modelled in. It can be drafted as chapter of a dissertation OR a standalone assignment. ";
            // }
            // if(title == "Plan Model Answer"){
            //     modalbody = "Get help in answering any exam questions! ";
            // }
            // if(title == "Poster"){
            //     modalbody = "A poster is an interesting, informative visually pleasing piece of work, which is drafted in a power point slide or an adobe acrobat format. Our experts in infographic presentations can help your with all types of poster requirements.  ";
            // }
            // if(title == "Presentation"){
            //     modalbody = "Create a visual presentation on any topic! This could be a module requirement, or you may need to present your academic project to a group of people. This service may also include speech comments/notes (if required). ";
            // }
            // if(title == "Reflective Report"){
            //     modalbody = "A reflective report or an essay is review of a particular experience or a situation. It aims to shed light on the existing theories and explore potential areas of improvement and learning in an academic manner. Reflective reports are typically written in first person. ";
            // }
            // if(title == "Personal Statement"){
            //     modalbody = "An admission or application essay, sometimes also called a personal statement or a statement of purpose is an essay or other written statement written by an applicant, often a prospective student applying to some college, university, or graduate school.";
            // }
            // if(title == "Case Study Writing Service"){
            //     modalbody = "A case study is a research method involving an up-close, in-depth, and detailed examination of a particular case.";
            // }
            // if(title == "Research Proposal Writing Service"){
            //     modalbody = "A research proposal is a document proposing a research project, generally in the sciences or academia, and generally constitutes a request for sponsorship of that research.";
            // }
            // if(title == "Business Plan Writing Service"){
            //     modalbody = "A business plan is a formal written document containing business goals, the methods on how these goals can be attained, and the time frame within which these goals need to be achieved.";
            // }
            // if(title == "Primary Research Service"){
            //     modalbody = "Primary research is any type of research that you collect yourself. Examples include surveys, interviews, observations, and ethnographic research. ";
            // }
            // if(title == "Secondary Research Collation Service"){
            //     modalbody = "Secondary research involves the summary, collation and/or synthesis of existing research. ";
            // }
            // if(title == "Annotated Bibliography Service"){
            //     modalbody = "An annotated bibliography is a bibliography that gives a summary of each of the entries. The purpose of annotations is to provide the reader with a summary and an evaluation of each source. ";
            // }
            // if(title == "Professional Video Editing Service"){
            //     modalbody = "Video editing is the manipulation and arrangement of video shots. Video editing is used to structure and present all video information, including films and television shows, video advertisements and video essays.";
            // }


            jQuery('.modal-title').html(title);
            jQuery('.modal-body').html(modalbody);

            jQuery('#ModalCenter').modal();
            
            }
    

    });



    jQuery('#try input[name=writing_type]').on('change', function() { 
        if(jQuery('input[name=writing_type]:checked', '#try').val()){ 

            var title = "";
            var modalbody = "";
            
            title = jQuery('input[name=writing_type]:checked', '#try').val();
            title = title.replace(/_/g, " ");
            title = title.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
        
            if(title == "Editing"){
                modalbody = "Select this service type if you have an existing draft which you would like us to edit, improve and proofread. Please select custom writing for all other service types.";
                jQuery('.modal-title').html(title);
                jQuery('.modal-body').html(modalbody);

                jQuery('#ModalCenter').modal();
            }          
        
            
            }    

    });

    /*
    |--------------------------------------------------------------------------
    | Modal Ends
    |--------------------------------------------------------------------------
    */ 
   
    /*
    |--------------------------------------------------------------------------
    | Stage2 Order Specifics Starts
    |--------------------------------------------------------------------------
    */  
  
    //jQuery('#quote_get').on('click','input[name=writing_order_type]:checked',function(){
        if(jQuery('input[name=writing_type]', '#try').val() == "writingdis" || jQuery('input[name=writing_type]', '#try').val() == "writingoth"){        
        //var ordertypes = jQuery('input[name=writing_order_type]:checked', '#try').val();
        var ordertypes = jQuery('input[name=writing_order_type]', '#try').val();
        
        jQuery('#typeofresearch').hide();
        jQuery('#osp-1').hide();
        jQuery('#osp-2').hide();
        jQuery('#osp-3').hide();
        jQuery('#osp-4').hide();
        jQuery('#osp-5').hide();
        jQuery('#osp-6').hide();
        jQuery('#osp-7').hide();
        jQuery('#osp-8').hide();
        jQuery('#osp-9').hide();
        jQuery('#osp-10').hide();
        jQuery('#osp-11').hide();
        jQuery('#osp-12').hide();
        jQuery('#osp-13').hide();
        jQuery('#osp-14').hide();
        jQuery('#osp-15').hide();
        jQuery('#osp-16').hide();
        jQuery('#osp-17').hide();
        jQuery('#osp-18').hide();
        jQuery('#osp-19').hide();
        jQuery('#osp-20').hide();
        jQuery('#osp-21').hide();
        jQuery('#osp-22').hide();
        jQuery('#osp-23').hide();
        jQuery('#osp-24').hide();


        
        
            if(ordertypes == "dissertation_topic"){  
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();                        
            }
            
            if(ordertypes == "dissertation_topic_and_plan_outline"){  
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-4').show();       
                jQuery('#osp-6').show();      
                jQuery('#osp-7').show();                    
            }
        
            if(ordertypes == "dissertation_part"){  
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();       
                jQuery('#osp-8').show();                              
            }

            if(ordertypes == "dissertation_full"){  
                jQuery('#typeofresearch').show();     
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();       
                jQuery('#osp-19').show(); 
                jQuery('#osp-20').show();  
                jQuery('#osp-11').show();                            
            }

            if(ordertypes == "dissertation_statistical_analysis"){                          
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();       
                jQuery('#osp-8').show(); 
                jQuery('#osp-9').show();   
                jQuery('#osp-16').show();                                       
            }

            if(ordertypes == "dissertation_proposal"){                          
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();       
                jQuery('#osp-6').show(); 
                jQuery('#osp-10').show();    
                jQuery('#osp-11').show();
                jQuery('#osp-12').show();                                    
            }

            if(ordertypes == "data_analysis"){                          
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();                                     
            }
            
            if(ordertypes == "assignment" || ordertypes == "report" || ordertypes == "coursework" || ordertypes == "essay" || ordertypes == "personal_statement" || ordertypes == "case_study_writing_service" || ordertypes == "business_plan_writing_service" || ordertypes == "primary_research_service" || ordertypes == "secondary_research_collation_service" || ordertypes == "annotated_bibliography_service" || ordertypes == "professional_video_editing_service"){                          
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();                                              
            }

            if(ordertypes == "research_proposal_writing_service"){
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show(); 
                jQuery('#osp-12').show(); 
                jQuery('#osp-19').show(); 


            }

            if(ordertypes == "exam_notes"){                          
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();       
                jQuery('#osp-13').show(); 
                jQuery('#osp-14').show();                                                            
            }

            if(ordertypes == "statistical_analysis"){                          
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();       
                jQuery('#osp-17').show(); 
                jQuery('#osp-15').show();   
                jQuery('#osp-16').show();                                                            
            }

            if(ordertypes == "laboratory_report"){                          
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();       
                jQuery('#osp-18').show(); 
                jQuery('#osp-16').show();                                                        
            }

            if(ordertypes == "literature_review" || ordertypes == "plan_model_answer" || ordertypes == "poster" || ordertypes == "presentation" || ordertypes == "reflective_report"){                          
                jQuery('#osp-1').show();
                jQuery('#osp-2').show();
                jQuery('#osp-3').show();    
                jQuery('#osp-5').show();                                                                
            }
        
    }
    //});
    else{
    //jQuery('#quote_get').on('click','input[name=editing_order_type]:checked',function(){

        //var ordertypes = jQuery('input[name=editing_order_type]:checked', '#try').val();
        var ordertypes = jQuery('input[name=editing_order_type]', '#try').val();

        jQuery('#typeofresearch').hide();
        jQuery('#osp-1').hide();
        jQuery('#osp-2').hide();
        jQuery('#osp-3').hide();
        jQuery('#osp-4').hide();
        jQuery('#osp-5').hide();
        jQuery('#osp-6').hide();
        jQuery('#osp-7').hide();
        jQuery('#osp-8').hide();
        jQuery('#osp-9').hide();
        jQuery('#osp-10').hide();
        jQuery('#osp-11').hide();
        jQuery('#osp-12').hide();
        jQuery('#osp-13').hide();
        jQuery('#osp-14').hide();
        jQuery('#osp-15').hide();
        jQuery('#osp-16').hide();
        jQuery('#osp-17').hide();
        jQuery('#osp-18').hide();
        jQuery('#osp-19').hide();
        jQuery('#osp-20').hide();
        jQuery('#osp-21').hide();
        jQuery('#osp-22').hide();
        jQuery('#osp-23').hide();
        jQuery('#osp-24').hide();

                
        if(ordertypes == "marking_service"){  
            jQuery('#osp-1').show();
            jQuery('#osp-2').show();
            jQuery('#osp-3').show();    
            jQuery('#osp-5').show();        
            jQuery('#osp-21').show();

        }

        if(ordertypes == "editing_proofreading"){  
            jQuery('#osp-1').show();
            jQuery('#osp-2').show();
            jQuery('#osp-3').show();    
            jQuery('#osp-5').show();        
            jQuery('#osp-22').show();
            jQuery('#osp-24').show();

        }

        if(ordertypes == "academic_editing_rewriting"){  
            jQuery('#osp-1').show();
            jQuery('#osp-2').show();
            jQuery('#osp-3').show();    
            jQuery('#osp-5').show();      
            jQuery('#osp-23').show();
            jQuery('#osp-24').show();

        }
    
        // if(ordertypes == "dissertation_topic"){  
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();                        
        // }
        
        // if(ordertypes == "dissertation_topic_and_plan_outline"){  
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-4').show();       
        //     jQuery('#osp-6').show();      
        //     jQuery('#osp-7').show();                    
        // }
    
        // if(ordertypes == "dissertation_part"){  
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();       
        //     jQuery('#osp-8').show();                              
        // }

        // if(ordertypes == "dissertation_full"){  
        //     jQuery('#typeofresearch').show();     
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();       
        //     jQuery('#osp-19').show(); 
        //     jQuery('#osp-20').show();  
        //     jQuery('#osp-11').show();                            
        // }

        // if(ordertypes == "dissertation_statistical_analysis"){                          
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();       
        //     jQuery('#osp-8').show(); 
        //     jQuery('#osp-9').show();                                        
        // }

        // if(ordertypes == "dissertation_proposal"){                          
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();       
        //     jQuery('#osp-6').show(); 
        //     jQuery('#osp-10').show();    
        //     jQuery('#osp-11').show();
        //     jQuery('#osp-12').show();                                    
        // }
        
        // if(ordertypes == "assignment" || ordertypes == "report" || ordertypes == "coursework" || ordertypes == "essay" || ordertypes == "personal_statement" || ordertypes == "case_study_writing_service" || ordertypes == "research_proposal_writing_service" || ordertypes == "business_plan_writing_service" || ordertypes == "primary_research_service" || ordertypes == "secondary_research_collation_service" || ordertypes == "annotated_bibliography_service" || ordertypes == "professional_video_editing_service"){                          
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();                                           
        // }

        // if(ordertypes == "exam_notes"){                          
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();       
        //     jQuery('#osp-13').show(); 
        //     jQuery('#osp-14').show();                                                            
        // }

        // if(ordertypes == "statistical_analysis"){                          
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();       
        //     jQuery('#osp-17').show(); 
        //     jQuery('#osp-15').show();   
        //     jQuery('#osp-16').show();                                                            
        // }

        // if(ordertypes == "laboratory_report"){                          
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();       
        //     jQuery('#osp-18').show(); 
        //     jQuery('#osp-16').show();                                                        
        // }

        // if(ordertypes == "literature_review" || ordertypes == "plan_model_answer" || ordertypes == "poster" || ordertypes == "presentation" || ordertypes == "reflective_report"){                          
        //     jQuery('#osp-1').show();
        //     jQuery('#osp-2').show();
        //     jQuery('#osp-3').show();    
        //     jQuery('#osp-5').show();                                                                
        // }


    //});
    }

    //Order Specific Input hide
    jQuery('#quote_get').on('click','#boxoptiona',function(){

        jQuery('.bx1').show();
        jQuery('.bx2').hide();
        
    });

    jQuery('#quote_get').on('click','#boxoptionb',function(){

        jQuery('.bx2').show();
        jQuery('.bx1').hide();
        
    });

    jQuery('#quote_get').on('click','input[name=os-dany]:checked',function(){

        var ostype = jQuery('input[name=os-dany]:checked', '#try').val(); 
        if(ostype == "Yes"){
            jQuery('.os-dany-sub').show();
        }
        else{
            jQuery('.os-dany-sub').hide();
        }                
        
    });

    
    jQuery('#quote_get').on('click','input[name=os-diss]:checked',function(){

        var ostype = jQuery('input[name=os-diss]:checked', '#try').val(); 
        if(ostype == "Yes"){
            jQuery('.os-diss-sub').show();
        }
        else{
            jQuery('.os-diss-sub').hide();
        }                
        
    });

    jQuery('#quote_get').on('click','input[name=os-appdiss]:checked',function(){

        var ostype = jQuery('input[name=os-appdiss]:checked', '#try').val(); 
        if(ostype == "Yes"){
            jQuery('.os-appdiss-sub').show();
        }
        else{
            jQuery('.os-appdiss-sub').hide();
        }                

    });

     jQuery('#quote_get').on('click','input[name=os-prop]:checked',function(){

        var ostype = jQuery('input[name=os-prop]:checked', '#try').val(); 
        if(ostype == "Yes"){
            jQuery('.os-prop-sub').show();
        }
        else{
            jQuery('.os-prop-sub').hide();
        }                
        
    });

     jQuery('#quote_get').on('click','input[name=os-apppr]:checked',function(){

        var ostype = jQuery('input[name=os-apppr]:checked', '#try').val(); 
        if(ostype == "Yes"){
            jQuery('.os-apppr-sub').show();
        }
        else{
            jQuery('.os-apppr-sub').hide();
        }                
        
    });

    jQuery('#quote_get').on('click','input[name=os-temp]:checked',function(){

        var ostype = jQuery('input[name=os-temp]:checked', '#try').val(); 
        if(ostype == "Yes"){
            jQuery('.os-temp-sub').show();
        }
        else{
            jQuery('.os-temp-sub').hide();
        }                
        
    });


    /*
    |--------------------------------------------------------------------------
    | Stage2 Order Specifics Ends
    |--------------------------------------------------------------------------
    */ 

    /*
    |--------------------------------------------------------------------------
    | Stage2 Extra Starts
    |--------------------------------------------------------------------------
    */ 
        //jQuery('#try input').on('change', function() {
            //if(jQuery('input[name=writing_type]:checked', '#try').val() == "writing"){  
            if(jQuery('input[name=writing_type]', '#try').val() == "writingdis" || jQuery('input[name=writing_type]', '#try').val() == "writingoth"){                
            jQuery('#extraservicestart').show();
            jQuery('.e-contentpage').hide();
            jQuery('.e-models').hide();
            jQuery('.e-sources').hide();
            jQuery('.e-plag').hide();
            jQuery('.e-summary').hide();
            jQuery('.e-graph').hide();
            jQuery('.e-stats').hide();
            jQuery('.e-reflective').hide();
            jQuery('.e-presentation').hide();
            jQuery('.e-draft').hide();
            jQuery('.e-calculation').hide();
            jQuery('.e-report').hide();
            jQuery('.e-comment').hide();
            jQuery('.e-accompany').hide();
            var type0forder = "";
            
                //type0forder = jQuery('input[name=writing_order_type]:checked', '#try').val();
                type0forder = jQuery('input[name=writing_order_type]', '#try').val();
                if(type0forder == "dissertation_topic" || type0forder == "dissertation_topic_and_plan_outline"){  
                    jQuery('#extraservicestart').hide();
                    
                }


                if(type0forder == "dissertation_part" || type0forder=="dissertation_statistical_analysis"){  
                    jQuery('#extraservicestart').show();
                    jQuery('.e-contentpage').show();
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();
                    jQuery('.e-plag').show();
                    jQuery('.e-reflective').show();
                    jQuery('.e-presentation').show();
                    jQuery('.e-draft').show();
                }

                if(type0forder == "dissertation_full"){  
                    jQuery('#extraservicestart').show();
                    jQuery('.e-contentpage').show();
                    jQuery('.e-summary').show();
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();
                    jQuery('.e-graph').show();
                    jQuery('.e-stats').show();
                    jQuery('.e-plag').show();
                    jQuery('.e-presentation').show();
                    jQuery('.e-reflective').show();
                }

                if(type0forder == "dissertation_proposal" || type0forder =="literature_review" || type0forder == "reflective_report"){  
                    jQuery('#extraservicestart').show();
                    jQuery('.e-contentpage').show();
                    jQuery('.e-summary').show();
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();
                    jQuery('.e-presentation').show();
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                }

                if(type0forder == "data_analysis"){  
                    jQuery('#extraservicestart').show();
                    jQuery('.e-contentpage').show();
                    jQuery('.e-summary').show();
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();
                    jQuery('.e-calculation').show();
                    jQuery('.e-presentation').show();
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                }

                if(type0forder == "assignment" || type0forder == "report" || type0forder == "coursework" || type0forder == "essay" ||  type0forder == "plan_model_answer"  ||  type0forder == "personal_statement" ||  type0forder == "case_study_writing_service" ||  type0forder == "research_proposal_writing_service" ||  type0forder == "business_plan_writing_service" || type0forder == "primary_research_service" || type0forder == "secondary_research_collation_service" || type0forder == "annotated_bibliography_service"){  
                    jQuery('#extraservicestart').show();
                    jQuery('.e-contentpage').show();
                    jQuery('.e-summary').show();
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();
                    jQuery('.e-calculation').show();
                    jQuery('.e-presentation').show();
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                }

                if(type0forder == "professional_video_editing_service"){  
                    jQuery('#extraservicestart').show();                        
                    jQuery('.e-presentation').show();
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                }

                if(type0forder == "exam_notes"){  
                    jQuery('#extraservicestart').show();                        
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();
                    jQuery('.e-calculation').show();                     
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                }

                if(type0forder == "statistical_analysis"){  
                    jQuery('#extraservicestart').show();                        
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();
                    jQuery('.e-report').show();                     
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                }

                
                if(type0forder == "laboratory_report"){  
                    jQuery('#extraservicestart').show(); 
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();                                  
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                    jQuery('.e-calculation').show();    
                    jQuery('.e-presentation').show();
                }

                if(type0forder == "poster"){  
                    jQuery('#extraservicestart').show();                        
                    jQuery('.e-graph').show();
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();
                    jQuery('.e-accompany').show();
                    jQuery('.e-calculation').show();
                    jQuery('.e-report').show();
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                }

                if(type0forder == "presentation"){  
                    jQuery('#extraservicestart').show();                        
                    jQuery('.e-graph').show();
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();
                    jQuery('.e-accompany').show();
                    jQuery('.e-calculation').show();
                    jQuery('.e-comment').show();
                    jQuery('.e-draft').show();
                    jQuery('.e-reflective').show();
                }    

            }             

            //else if(jQuery('input[name=writing_type]:checked', '#try').val() == "editing"){ 
            else if(jQuery('input[name=writing_type]', '#try').val() == "editing"){                
            jQuery('#extraservicestart').show();
            jQuery('.e-contentpage').hide();
            jQuery('.e-models').hide();
            jQuery('.e-sources').hide();
            jQuery('.e-plag').hide();
            jQuery('.e-summary').hide();
            jQuery('.e-graph').hide();
            jQuery('.e-stats').hide();
            jQuery('.e-reflective').hide();
            jQuery('.e-presentation').hide();
            jQuery('.e-draft').hide();
            jQuery('.e-calculation').hide();
            jQuery('.e-report').hide();
            jQuery('.e-comment').hide();
            jQuery('.e-accompany').hide();
            var type0forder = "";
            
                //type0forder = jQuery('input[name=editing_order_type]:checked', '#try').val();
                type0forder = jQuery('input[name=editing_order_type]', '#try').val();
                if(type0forder == "dissertation_topic" || type0forder == "dissertation_topic_and_plan_outline"){  
                    jQuery('#extraservicestart').hide();
                    
                }

                    
                if(type0forder == "editing_proofreading" || type0forder=="academic_editing_rewriting" || type0forder=="marking_service"){  
                    jQuery('#extraservicestart').show();
                    jQuery('.e-contentpage').show();
                    jQuery('.e-summary').show();
                    jQuery('.e-models').show();
                    jQuery('.e-sources').show();                        
                    jQuery('.e-plag').show();
                    jQuery('.e-calculation').show();
                    jQuery('.e-presentation').show();
                    jQuery('.e-draft').show();
                    //jQuery('.e-reflective').show();
                }

                // if(type0forder == "dissertation_part" || type0forder=="dissertation_statistical_analysis"){  
                //     jQuery('#extraservicestart').show();
                //     jQuery('.e-contentpage').show();
                //     jQuery('.e-models').show();
                //     jQuery('.e-sources').show();
                //     jQuery('.e-plag').show();
                //     jQuery('.e-reflective').show();
                // }

                // if(type0forder == "dissertation_full"){  
                //     jQuery('#extraservicestart').show();
                //     jQuery('.e-contentpage').show();
                //     jQuery('.e-summary').show();
                //     jQuery('.e-models').show();
                //     jQuery('.e-sources').show();
                //     jQuery('.e-graph').show();
                //     jQuery('.e-stats').show();
                //     jQuery('.e-plag').show();
                //     jQuery('.e-presentation').show();
                //     jQuery('.e-reflective').show();
                // }

                // if(type0forder == "dissertation_proposal" || type0forder =="literature_review" || type0forder == "reflective_report"){  
                //     jQuery('#extraservicestart').show();
                //     jQuery('.e-contentpage').show();
                //     jQuery('.e-summary').show();
                //     jQuery('.e-models').show();
                //     jQuery('.e-sources').show();                        
                //     jQuery('.e-plag').show();
                //     jQuery('.e-presentation').show();
                //     jQuery('.e-draft').show();
                //     jQuery('.e-reflective').show();
                // }

                // if(type0forder == "assignment" || type0forder == "report" || type0forder == "coursework" || type0forder == "essay" ||  type0forder == "plan_model_answer" ||  type0forder == "personal_statement" ||  type0forder == "case_study_writing_service" ||  type0forder == "research_proposal_writing_service" ||  type0forder == "business_plan_writing_service" || type0forder == "primary_research_service" || type0forder == "secondary_research_collation_service" || type0forder == "annotated_bibliography_service"){  
                //     jQuery('#extraservicestart').show();
                //     jQuery('.e-contentpage').show();
                //     jQuery('.e-summary').show();
                //     jQuery('.e-models').show();
                //     jQuery('.e-sources').show();                        
                //     jQuery('.e-plag').show();
                //     jQuery('.e-calculation').show();
                //     jQuery('.e-presentation').show();
                //     jQuery('.e-draft').show();
                //     jQuery('.e-reflective').show();
                // }

                // if(type0forder == "professional_video_editing_service"){  
                //     jQuery('#extraservicestart').show();                        
                //     jQuery('.e-presentation').show();
                //     jQuery('.e-draft').show();
                //     jQuery('.e-reflective').show();
                // }

                // if(type0forder == "exam_notes"){  
                //     jQuery('#extraservicestart').show();                        
                //     jQuery('.e-models').show();
                //     jQuery('.e-sources').show();                        
                //     jQuery('.e-plag').show();
                //     jQuery('.e-calculation').show();                     
                //     jQuery('.e-draft').show();
                //     jQuery('.e-reflective').show();
                // }

                // if(type0forder == "statistical_analysis"){  
                //     jQuery('#extraservicestart').show();                        
                //     jQuery('.e-models').show();
                //     jQuery('.e-sources').show();                        
                //     jQuery('.e-plag').show();
                //     jQuery('.e-report').show();                     
                //     jQuery('.e-draft').show();
                //     jQuery('.e-reflective').show();
                // }

                
                // if(type0forder == "laboratory_report"){  
                //     jQuery('#extraservicestart').show(); 
                //     jQuery('.e-sources').show();                        
                //     jQuery('.e-plag').show();                                  
                //     jQuery('.e-draft').show();
                //     jQuery('.e-reflective').show();
                // }

                // if(type0forder == "poster"){  
                //     jQuery('#extraservicestart').show();                        
                //     jQuery('.e-graph').show();
                //     jQuery('.e-models').show();
                //     jQuery('.e-sources').show();                        
                //     jQuery('.e-plag').show();
                //     jQuery('.e-accompany').show();
                //     jQuery('.e-calculation').show();
                //     jQuery('.e-report').show();
                //     jQuery('.e-draft').show();
                //     jQuery('.e-reflective').show();
                // }

                // if(type0forder == "presentation"){  
                //     jQuery('#extraservicestart').show();                        
                //     jQuery('.e-graph').show();
                //     jQuery('.e-models').show();
                //     jQuery('.e-sources').show();                        
                //     jQuery('.e-plag').show();
                //     jQuery('.e-accompany').show();
                //     jQuery('.e-calculation').show();
                //     jQuery('.e-comment').show();
                //     jQuery('.e-draft').show();
                //     jQuery('.e-reflective').show();
                // }    

            }             


            else{
            jQuery('#extraservicestart').hide();
            }

        //});



        //Extra Service Sub Input Hide       
        jQuery('#quote_get').on('click','input[name=e-models]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-sources]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-graph]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-stats]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-reflective]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-presentation]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-draft]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-calculation]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-report]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-accompany]:checked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });


        ///// Unchecked Issue

        jQuery('#quote_get').on('click','input[name=e-models]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').hide();                
        });

        jQuery('#quote_get').on('click','input[name=e-sources]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').hide();         
        });

        jQuery('#quote_get').on('click','input[name=e-graph]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').hide();         
        });

        jQuery('#quote_get').on('click','input[name=e-stats]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').hide();         
        });

        jQuery('#quote_get').on('click','input[name=e-reflective]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').hide();         
        });

        jQuery('#quote_get').on('click','input[name=e-presentation]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').hide();         
        });

        jQuery('#quote_get').on('click','input[name=e-draft]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').hide();         
        });

        jQuery('#quote_get').on('click','input[name=e-calculation]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').hide();         
        });

        jQuery('#quote_get').on('click','input[name=e-report]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        jQuery('#quote_get').on('click','input[name=e-accompany]:unchecked',function(event){ 
            var subid = (event.target).value + "-sub";
            jQuery('#'+subid+'').show();         
        });

        //<!--STEP2  EXTRA SERVICE-->



        
        //<!--Type of research required for your dissertation-->

        jQuery('#quote_get').on('click','input[name=rs-presearch]:checked,input[name=rs-pr-sub1]:checked',function(){

            jQuery('#rs-presearch-sub1').show();
            var rstype = jQuery('input[name=rs-pr-sub1]:checked', '#try').val(); 
            if(rstype == 0){
                jQuery('.rs-pr-sub2').show();
            }
            else{
                jQuery('.rs-pr-sub2').hide();
            }
            
        });

        /* jQuery('#quote_get').on('click','input[name=rs-emp]:checked,input[name=rs-emp-sub1]:checked',function(){

            jQuery('#rs-emp-sub1').show();
            var rstype = jQuery('input[name=rs-emp-sub1]:checked', '#try').val();
            if(rstype == 0){
                jQuery('.rs-emp-sub2').show();
            }
            else{
                jQuery('.rs-emp-sub2').hide();
            }
            
        });*/

        jQuery('#quote_get').on('click','input[name=rs-del]:checked',function(){
            
            var rstype = jQuery('input[name=rs-del]:checked', '#try').val();
            if(rstype == "Change"){
                jQuery('.rs-del').show();
            }
            else{
                jQuery('.rs-del').hide();
            }
            
        });

        jQuery('#quote_get').on('click','input[name=rs-presearch]:unchecked',function(){
            jQuery('#rs-presearch-sub1').hide();                           
        });

        jQuery('#quote_get').on('click','input[name=rs-emp]:unchecked',function(){
            jQuery('#rs-emp-sub1').hide();           
        });

        jQuery('#quote_get').on('click','input[name=rs-del]:unchecked',function(){
            jQuery('#rs-del').hide();           
        });

        //<!--Type of research required for your dissertation-->




    /*
    |--------------------------------------------------------------------------
    | Stage2 Extra Ends
    |--------------------------------------------------------------------------
    */ 
    
    /*
    |--------------------------------------------------------------------------
    | File Upload Starts
    |--------------------------------------------------------------------------
    */ 

    jQuery(document).ready(function(){
        var i = 1;
        jQuery('#quote_get').on('change','input[type=file]', function(e){
            var fileName = e.target.files[0].name;
            var filetoid = fileName.slice(0,-4);
            var uploadclass = e.target.outerHTML;            
            
            uploadclass = uploadclass.substring(14,25)+"-input";    


            // if(e.target.files.length < 1) {      
            //     jQuery(e.target).next('label').text(fileName); 
            // }
            // else {
            //     jQuery(e.target).next('label').text(e.target.files.length + ' files attached');
            // }            

            // jQuery("#filehistory").append("<br><div class='filetoid' id='"+filetoid+"'>"+fileName+"<span class='filedelete' id='"+i+"'>DELETE</span></div>");
            // i = i+1;
            
            jQuery(e.target).attr("readonly",true);    
            //jQuery(e.target).attr("disabled",true);    
            jQuery(e.target).next('label').text(fileName); 
            
            i = +i + +1;
            //jQuery(e.target).parent('.custom-file').after("<button type='button' style='padding: 8px;text-align: center;background: #fd6262;position: relative;top: 4px;left: 5px;' class='filedel'>Delete</button>");
            

            if(jQuery(e.target).attr('id') == "custom-file-input_os_upload"){
                jQuery(e.target).parent('.custom-file').after("<button type='button' style='padding: 8px;text-align: center;background: #fd6262;position: relative;top: 4px;left: 5px;' class='filedel'>Delete</button>");
            
                jQuery("#filehistory").append("<div class='custom-file' id='"+ +i +"' style='margin-bottom:5px;'><input class='custom-file-input valid' name='os-upload[]' type='file' id='custom-file-input_os_upload' multiple='' aria-invalid='false'><label class='custom-file-label' for=''><span class=''>Choose file</span></label></div>");
           
            }

           /* jQuery('.'+uploadclass+'').parent().after('<button id="delbutton" class="btn btn-md delbutton">DELETE</button>');
            jQuery('.'+uploadclass+'').parent().css("width","86%"); 
            console.log(uploadclass);

           var uploadhtml = '<div class="col-sm-12"><div class="custom-file" style="width: 86%;"><input class="custom-file-input" name="upload[]" type="file" id="custom-file-input2"><label class="custom-file-label" for="">223259.jpg</label></div><button id="delbutton" class="btn btn-md delbutton">DELETE</button></div>';
            jQuery('.'+uploadclass+'').parent().parent().parent().append(uploadhtml);*/
        });

        jQuery('#quote_get').on('click','.filedel',function(ev){  
            var butid = ev.target;
            jQuery(ev.target).prev('.custom-file').hide();
            jQuery(ev.target).prev('.custom-file').children("input[type='file']:first").attr("disabled",true);   
            jQuery(ev.target).hide();
           
           // console.log(jQuery('#'+butid+'').parent().attr('id'));

            // 
            // jQuery('.'+butid+'').prev('.custom-file').hide();
            // jQuery('.'+butid+'').hide();
            
        });
    });
    /*
    |--------------------------------------------------------------------------
    | File Upload Ends
    |--------------------------------------------------------------------------
    */ 

    /*
    |--------------------------------------------------------------------------
    | Payment Modals Starts
    |--------------------------------------------------------------------------
    */ 

    jQuery('#paybypaypal').on('click', function($e) {    
        $e.preventDefault();
        jQuery('#ModalPaypal').modal();

    });  

    // jQuery('#paybybankpart').on('click', function($e) {    
    //     $e.preventDefault();
    //     jQuery('#ModalBank1').modal();

    // });
    
    jQuery('#paybybankfull').on('click', function($e) {    
        $e.preventDefault();
        jQuery('#ModalBank').modal();

    });

    jQuery('#paybycard').on('click', function($e) {    
        $e.preventDefault();
        /*var paythis = jQuery('.actualamount').html();
       
        jQuery('input[name=paythis]').val(paythis);*/
        jQuery('#ModalCard').modal();

    }); 




    $('#twopay1').on('click', function() { 
       
        jQuery('#partpaym').hide();
        jQuery('#fullpaym').show();
        jQuery('#partpaymstripe').hide();
        jQuery('#fullpaymstripe').show();  
        
        jQuery('#fullbanktransfer').show();
        
        var paythis = jQuery('#topay').val(); 
        jQuery('.paythis').html(parseFloat(paythis).toFixed(2));
        jQuery('.paymentcard').val(parseFloat(paythis).toFixed(2));
        jQuery('.paymentpaypal').val(parseFloat(paythis).toFixed(2));
        
        jQuery('#paymentmethod').show();
        
    });   

    jQuery('#twopay2').on('click', function() { 
       
       jQuery('#partpaym').show();
       jQuery('#fullpaym').hide();
       jQuery('#fullpaymstripe').hide();
       jQuery('#partpaymstripe').show();
        
        jQuery('#fullbanktransfer').show();
        
        var paythis = jQuery('#topaypart').val();   
        jQuery('.paythis').html(parseFloat(paythis).toFixed(2));
        jQuery('.paymentcard').val(parseFloat(paythis).toFixed(2));
        jQuery('.paymentpaypal').val(parseFloat(paythis).toFixed(2));
        
       jQuery('#paymentmethod').show();
       
   });   

   /*
    |--------------------------------------------------------------------------
    | Payment Modals Ends
    |--------------------------------------------------------------------------
    */ 


    /*
    |--------------------------------------------------------------------------
    | Unknown Starts
    |--------------------------------------------------------------------------
    */  
    //Stage2 label changes  
    var ordertypeNew = jQuery('input[name=editing_order_type]', '#try').val();
    if(ordertypeNew == "business_plan_writing_service"){ 
        jQuery("#areaforbp").show();
        jQuery("#topicforbp").show();
        jQuery("#areafornormal").hide();
        jQuery("#topicfornormal").hide();
        jQuery("#topicforvideo").hide();
    }
    else if(ordertypeNew == "professional_video_editing_service"){ 
        jQuery("#areaforbp").hide();    
        jQuery("#topicforbp").hide();  
        jQuery("#topicfornormal").hide();
        jQuery("#topicforvideo").show();
    }
    else{
        jQuery("#areaforbp").hide();
        jQuery("#topicforbp").hide();
        jQuery("#topicforvideo").hide();
        jQuery("#areafornormal").show();
        jQuery("#topicfornormal").show();
    }
    var ordertypeNewWr = jQuery('input[name=writing_order_type]', '#try').val();
    if(ordertypeNewWr == "research_proposal_writing_service"){ 
        jQuery("#topicdissnormal").hide();
        jQuery("#topicresnormal").show();       
    }
    else{
        jQuery("#topicdissnormal").show();
        jQuery("#topicresnormal").hide();  
    }
    //Stage2 label changes
    /*jQuery('#abc').click(function($){	   
		jQuery('#spinner_waiter').css({"display": "flex", "position": "fixed", "top": "10px"});		
    });

    */
    /*
    |--------------------------------------------------------------------------
    | Unknown Ends
    |--------------------------------------------------------------------------
    */  
 });

/*
|--------------------------------------------------------------------------
| Main Document Ready Ends
|--------------------------------------------------------------------------
*/ 

/*
|--------------------------------------------------------------------------
| Calculator Starts
|--------------------------------------------------------------------------
*/  
function calculator(level,subject,service,ordertype,deadline){
    
        var sidebartotalamount = 1;        
        var gcsc_base = 57.99;
        var alevel_base = 59.99;
        var international_base = 64.99;
        var diploma_base = 69.99;
        var undergraduate_base = 84.99;
        var masters_base = 99.99;
        var mphill_base = 154.99;
        var phd_base = 179.99;
        var diploma_presentation_base = 14.99;
        var undergraduate_presentation_base = 19.99;
        var master_presentation_base = 24.99;
        var mphil_presentation_base = 44.99;
        var phd_presentation_base = 44.99;
        var ps_wr_base = 130;
        var ps_ed_base = 75;  
        var bib_und_base = 15.20;    
        var bib_mas_base = 18.99;      
        var bib_phd_base = 34.99;    
        var video_base =  199.99;
        var ep_al_base = 50;
        var ep_und_base = 60;
        var ep_mas_base = 70;
        var ep_phd_base = 95;
        var ms_al_base = 25;
        var ms_und_base = 30;
        var ms_mas_base = 45;
        var ms_phd_base = 70;

        var levelbaseamount = 0;
        var standard500words = 45;
        var fivetopics = 29.99;
        //var fivetopics = 1;
        
        //Calculate writing amount
        if(level === "G.C.S.C"){
            sidebartotalamount = gcsc_base;
            levelbaseamount = gcsc_base;
        }
        else if(level === "A-Level"){
            sidebartotalamount = alevel_base;
            levelbaseamount = alevel_base;
        }
        else if(level === "International_Baccalaureate"){
            sidebartotalamount = international_base;
            levelbaseamount = international_base;
        }
        else if(level === "Diploma"){
            sidebartotalamount = diploma_base;
            levelbaseamount = diploma_base;
        }
        else if(level === "Undergraduate"){
            sidebartotalamount = undergraduate_base;
            levelbaseamount = undergraduate_base; 
        }
        else if(level === "Masters"){
            sidebartotalamount = masters_base;
            levelbaseamount = masters_base;
        }
        else if(level === "MPhill"){
            sidebartotalamount = mphill_base;
            levelbaseamount = mphill_base;
        }
        else if(level === "PhD"){
            sidebartotalamount = phd_base;
            levelbaseamount = phd_base;
        }
        else{
            sidebartotalamount = 0;
            levelbaseamount = 0;
        }

        if(!jQuery('input[name=writing_order_type]:checked').val()){
            sidebartotalamount = 0;
        }
        
       
        //Poster Order
        if(ordertype === "poster"){  
            var wordlengthposter = parseInt(jQuery('select[name=wordlengthposter]').val());
            var postersize = jQuery('select[name=poster_size]').val();
            
            //wordlengthposter = wordlengthposter/250;   alert(wordlengthposter)  
            
            if(wordlengthposter == "750"){
                sidebartotalamount =  (sidebartotalamount*1.3);
            }
            else if(wordlengthposter == "1000"){
                sidebartotalamount =  (sidebartotalamount*1.45);
            }
            else if(wordlengthposter == "1250"){
                sidebartotalamount =  (sidebartotalamount*1.75);
            }
            else if(wordlengthposter == "1500"){
                sidebartotalamount =  (sidebartotalamount*2);
            }
            else if(wordlengthposter == "1750"){
                sidebartotalamount =  (sidebartotalamount*2.25);
            }
            else if(wordlengthposter == "2000"){
                sidebartotalamount =  (sidebartotalamount*2.5);
            }
           
           

                 if(postersize === "a1") { sidebartotalamount = sidebartotalamount*2.5}
            else if(postersize === "a2") { sidebartotalamount = sidebartotalamount*2}
            else if(postersize === "a3") { sidebartotalamount = sidebartotalamount*1.7}
            levelbaseamount = sidebartotalamount;
        } 

         //Presentation Order
         if(ordertype === "presentation"){
             if(level === "Diploma"){
                sidebartotalamount = diploma_presentation_base;
                levelbaseamount = diploma_presentation_base;
            }
            else if(level === "Undergraduate"){
                sidebartotalamount = undergraduate_presentation_base;
                levelbaseamount = undergraduate_presentation_base;
            }
            else if(level === "Masters"){
                sidebartotalamount = master_presentation_base;
                levelbaseamount = master_presentation_base;
            }
            else if(level === "MPhill"){
                sidebartotalamount = mphil_presentation_base;
                levelbaseamount = mphil_presentation_base;
            }
            else if(level === "PhD"){
                sidebartotalamount = phd_presentation_base;
                levelbaseamount = phd_presentation_base;
            }
            var slidelength = parseInt(jQuery('select[name=slide_length]').val());  

            //slidelength = slidelength/3;
            slidelength = slidelength;
            sidebartotalamount =  (sidebartotalamount*slidelength);                     
        }

         //Personal Statement Order
         if(ordertype === "personal_statement"){

            var optionlevel = jQuery('select[name=wordlengthps]').val();

            if(optionlevel === "Undergraduate"){
                sidebartotalamount = ps_wr_base;
                levelbaseamount = ps_wr_base;
            }
            else if(optionlevel === "Postgraduate"){
                sidebartotalamount = ps_wr_base*1.15;
                levelbaseamount = ps_wr_base*1.15;
            }
            else if(optionlevel === "Professional"){
                sidebartotalamount = ps_wr_base*1.07;
                levelbaseamount = ps_wr_base*1.07;         
            }           
            sidebartotalamount = Math.round(sidebartotalamount);
            levelbaseamount = Math.round(levelbaseamount);

            //slidelength = slidelength/3;
            //slidelength = slidelength;
            //sidebartotalamount =  (sidebartotalamount*slidelength);                     
        }

        //Biblography Order
        if(ordertype === "annotated_bibliography_service"){
            var sourcelength = jQuery('select[name=wordlengthbib]').val();;  
            if(level === "Undergraduate" || level === "A-Level" || level === "International_Baccalaureate" || level === "Diploma"){
                sidebartotalamount = bib_und_base;
                levelbaseamount = bib_und_base;
            }
            else if(level === "Masters"){
                sidebartotalamount = bib_mas_base;
                levelbaseamount = bib_mas_base;
            }           
            else if(level === "PhD" || level === "MPhill"){
                sidebartotalamount = bib_phd_base;
                levelbaseamount = bib_phd_base;
            }
            
            sidebartotalamount =  (sidebartotalamount*sourcelength);                     
        }

        //Video Order
        if(ordertype === "professional_video_editing_service"){
           
                sidebartotalamount = Math.round(video_base);
                levelbaseamount = Math.round(video_base);
                                
        }

        //Dissertation Part Order
        if(ordertype === "dissertation_part"){
            var partword = parseInt(jQuery('input[name=dissertation_part_words]').val());  

            partword = partword/250;
            sidebartotalamount =  (sidebartotalamount*0.25*partword);                     
        }

        //Dissertation Topic
        if(ordertype === "dissertation_topic"){            
            sidebartotalamount =  fivetopics;                     
        }

        //Dissertation Topic & Plan Outline
        if(ordertype === "dissertation_topic_and_plan_outline"){            
            sidebartotalamount =  standard500words;                     
        }
        
        //All other Order
        if(ordertype === "dissertation_statistical_analysis" || ordertype === "exam_notes" || ordertype === "assignment" || ordertype === "coursework" || ordertype === "other"
        || ordertype === "laboratory_report" || ordertype === "plan_model_answer" || ordertype === "report" || ordertype === "problem_solving" || ordertype === "dissertation_proposal" || ordertype === "data_analysis"
        || ordertype === "dissertation_full" || ordertype === "essay" || ordertype === "statistical_analysis" || ordertype === "literature_review" || ordertype === "reflective_report"
        || ordertype === "research_paper" || ordertype === "case_study_writing_service" || ordertype == "research_proposal_writing_service" || ordertype == "business_plan_writing_service"){
            
            //|| ordertype == "primary_research_service" || ordertype == "secondary_research_collation_service"
            var wordlength = parseInt(jQuery('select[name=wordlength]').val());  

            wordlength = wordlength/250;
            sidebartotalamount =  (sidebartotalamount*0.25*wordlength);                     
        }



        //Peak Factor Dissertation & Literature Review
        if(ordertype === "dissertation_part" || ordertype === "dissertation_full" || ordertype === "dissertation_proposal" || ordertype === "data_analysis"){
            sidebartotalamount =  sidebartotalamount*1.1;                     
        }

        //Peak Factor Statistics
        if(ordertype === "dissertation_statistical_analysis" || ordertype === "statistical_analysis"){
            sidebartotalamount =  sidebartotalamount*1.4;     
            
            var saoptions = jQuery('select[name=saoptions]').val();
            if(saoptions == "Matlab" || saoptions == "Python" || saoptions == "R"){
                sidebartotalamount = +sidebartotalamount + +295; 
            }
            else if(saoptions == "SPSS" || saoptions == "Excel"  || saoptions == "STATA"){
                sidebartotalamount = +sidebartotalamount + +150;
            }
            else if(saoptions == "Eviews" || saoptions == "Polystat"  || saoptions == "Latex" || saoptions == "Other"){
                sidebartotalamount = +sidebartotalamount + +175;
            }
            else if(saoptions == "SAS"){
                sidebartotalamount = +sidebartotalamount + +250;
            } 
        }

        //Art
        if(subject === "Art"){
            if(ordertype === "dissertation_topic_and_plan_outline" || ordertype === "dissertation_topic"){
                sidebartotalamount = sidebartotalamount*1;
            }
            else{
                sidebartotalamount = sidebartotalamount*1.2;    
            }
            
        }

        //Nursing
        if(subject === "Nursing"){
            if(ordertype === "dissertation_topic_and_plan_outline" || ordertype === "dissertation_topic"){
                sidebartotalamount = sidebartotalamount*1;
            }
            else{
                sidebartotalamount = sidebartotalamount*1.2;
            }
            
        }

        //Business Plan
        if(ordertype === "business_plan_writing_service"){
                sidebartotalamount = sidebartotalamount*1.75;
        }

        

        //Deadline               
        var days = (jQuery('input[name=writing_deadline]').datepicker("getDate")-new Date())/(1000 * 60 * 60 * 24);       
        
       
        
        
        if(days < 0){
            sidebartotalamount = sidebartotalamount*2;
        }
        else if(days >= 0 && days < 1){
            sidebartotalamount = sidebartotalamount*1.75;
        }
        else if(days >= 1 && days < 2){
            sidebartotalamount = sidebartotalamount*1.5;
        } 
        else if(days >= 2 && days < 3){
            sidebartotalamount = sidebartotalamount*1.2;
        }    
        
        /*if(days < 1){
            sidebartotalamount = sidebartotalamount*2;
        }
        else if(days >= 1 && days <= 2){
            sidebartotalamount = sidebartotalamount*1.75;
        }
        else if(days > 2 && days <= 3){
            sidebartotalamount = sidebartotalamount*1.5;
        } 
        else if(days > 3 && days <= 4){
            sidebartotalamount = sidebartotalamount*1.2;
        }      */

        ////////////////////////////////////
        //Check if editing
        if(service === "editing"){
            
            sidebartotalamount = 0;

            if(ordertype === "editing_proofreading"){

                var optionlevel = jQuery('select[name=academic_level]').val();
                var edwordlengthnew = parseInt(jQuery('select[name=edwordlengthnew]').val());  
                edwordlengthnew = edwordlengthnew/250;          

                if(optionlevel === "A-Level" || optionlevel === "International_Baccalaureate" || optionlevel === "Diploma"){
                    sidebartotalamount = ep_al_base;
                    levelbaseamount = ep_al_base;
                }
                else if(optionlevel === "Undergraduate"){
                    sidebartotalamount = ep_und_base;
                    levelbaseamount = ep_und_base;
                }
                else if(optionlevel === "Masters"){
                    sidebartotalamount = ep_mas_base;
                    levelbaseamount = ep_mas_base;         
                }  
                else if(optionlevel === "MPhill" || optionlevel === "PhD"){
                    sidebartotalamount = ep_phd_base;
                    levelbaseamount = ep_phd_base;         
                }         
                
                sidebartotalamount =  (sidebartotalamount*0.25*edwordlengthnew);
                levelbaseamount =  (levelbaseamount*0.25*edwordlengthnew); 
                
                sidebartotalamount = Math.round(sidebartotalamount);
                levelbaseamount = Math.round(levelbaseamount);

                               
            }
            else if(ordertype === "academic_editing_rewriting"){

                var optionlevel = jQuery('select[name=academic_level]').val();
                var edwordlengthnew = parseInt(jQuery('select[name=edwordlengthnew]').val());  
                edwordlengthnew = edwordlengthnew/250;          
                
                if(optionlevel === "A-Level"){
                    sidebartotalamount = alevel_base;
                    levelbaseamount = alevel_base;
                }
                else if(optionlevel === "International_Baccalaureate"){
                    sidebartotalamount = international_base;
                    levelbaseamount = international_base;
                }
                else if(optionlevel === "Diploma"){
                    sidebartotalamount = diploma_base;
                    levelbaseamount = diploma_base;
                }
                else if(optionlevel === "Undergraduate"){
                    sidebartotalamount = undergraduate_base;
                    levelbaseamount = undergraduate_base;
                }
                else if(optionlevel === "Masters"){
                    sidebartotalamount = masters_base;
                    levelbaseamount = masters_base;         
                }  
                else if(optionlevel === "MPhill"){
                    sidebartotalamount = mphill_base;
                    levelbaseamount = mphill_base;         
                }  
                else if(optionlevel === "PhD"){
                    sidebartotalamount = phd_base;
                    levelbaseamount = phd_base;         
                }        
                
                sidebartotalamount =  (sidebartotalamount*0.25*edwordlengthnew);
                levelbaseamount =  (levelbaseamount*0.25*edwordlengthnew); 
                
                sidebartotalamount = Math.round(sidebartotalamount);
                levelbaseamount = Math.round(levelbaseamount);

                               
            }
            else if(ordertype === "marking_service"){

                var optionlevel = jQuery('select[name=academic_level]').val();
                var edwordlengthnew = parseInt(jQuery('select[name=edwordlengthnew]').val());  
                edwordlengthnew = edwordlengthnew/250;          

                if(optionlevel === "A-Level" || optionlevel === "International_Baccalaureate" || optionlevel === "Diploma"){
                    sidebartotalamount = ms_al_base;
                    levelbaseamount = ms_al_base;
                }
                else if(optionlevel === "Undergraduate"){
                    sidebartotalamount = ms_und_base;
                    levelbaseamount = ms_und_base;
                }
                else if(optionlevel === "Masters"){
                    sidebartotalamount = ms_mas_base;
                    levelbaseamount = ms_mas_base;         
                }  
                else if(optionlevel === "MPhill" || optionlevel === "PhD"){
                    sidebartotalamount = ms_phd_base;
                    levelbaseamount = ms_phd_base;         
                }         
                
                sidebartotalamount =  (sidebartotalamount*0.25*edwordlengthnew);
                levelbaseamount =  (levelbaseamount*0.25*edwordlengthnew); 
                
                            

            }          

            var days = (jQuery('input[name=editing_deadline]').datepicker("getDate")-new Date())/(1000 * 60 * 60 * 24);             
        
            if(days < 0){
                sidebartotalamount = sidebartotalamount*2;
            }
            else if(days >= 0 && days < 1){
                sidebartotalamount = sidebartotalamount*1.75;
            }
            else if(days >= 1 && days < 2){
                sidebartotalamount = sidebartotalamount*1.5;
            } 
            else if(days >= 2 && days < 3){
                sidebartotalamount = sidebartotalamount*1.2;
            }    

            sidebartotalamount = Math.round(sidebartotalamount);
            levelbaseamount = Math.round(levelbaseamount);
        } 
        
        
        if(ordertype === "primary_research_service" || ordertype === "secondary_research_collation_service"){
            sidebartotalamount = 20;
        } 

        //Check if restricted subjects
        if(subject === "Architecture" || subject === "Chemistry" || subject === "Physics" || subject === "Biology" || subject === "Mathematics" || subject === "Computer_Engineering" || subject === "Engineering" || subject === "Statistics" || subject === "Finance"){
            if(ordertype === "assignment" || ordertype === "coursework"  || ordertype === "report" || ordertype === "exam_notes" || ordertype === "problem_solving" || ordertype === "research_paper" || ordertype === "plan_model_answer" || ordertype === "case_study_writing_service" || ordertype == "primary_research_service" || ordertype == "secondary_research_collation_service"){
                sidebartotalamount = 20;
            }            
        }
        
        
        
        /////////////////////////////////
        /////////////////////////////////
        //Check If package Selected or Not
        var formstage =jQuery('input[name=formstage]').val();       
        if(formstage > 1){
            //formstage change
            //if(jQuery('input[name=writing_type]:checked').val() == "writing"){
            if(jQuery('input[name=writing_type]').val() == "writingdis" || jQuery('input[name=writing_type]').val() == "writingoth" || jQuery('input[name=writing_type]').val() == "editing"){

            sidebartotalamount =jQuery('input[name=package]').val(); 
            consultation = 0;
            osreflist = 0;
            rsprsub1 = 0;
            rsempsub1 = 0;
            osdcol = 0;
            osdana = 0;
            ereflective = 0;
            epresentation = 0;
            eaccompany = 0;
            rsdp = 0;
            edraft = 0;
            ecalculation = 0;
            ecomments = 0;
            ereport = 0;
                         
            //Stage 2 Consult Calculation
            if(jQuery('input[name=phoneconsult]:checked').val()){
                consultation = jQuery('input[name=phoneconsult]:checked').val();               
            }
            
            //Stage2 Do you also want a reference list of essential sources?
            if(jQuery('input[name=os-reflist]:checked').val()){
                if(jQuery('#osp-7').attr('style') !== "display: none;"){
                    osreflist = jQuery('input[name=os-reflist]:checked').val(); 
                }
            }
            
            //Stage2 The price of the order does not include data Primary
            if(jQuery('input[name=rs-pr-sub1]:checked').val()){
                if(jQuery('#typeofresearch').attr('style') !== "display: none;"){
                    if(jQuery('#rs-presearch-sub1').attr('style') !== "display: none;"){
                        rsprsub1 = jQuery('input[name=rs-pr-sub1]:checked').val(); 
                    }
                }
            }

            //Stage2 The price of the order does not include data Emperical
            if(jQuery('input[name=rs-emp-sub1]:checked').val()){
                if(jQuery('#typeofresearch').attr('style') !== "display: none;"){
                    if(jQuery('#rs-emp-sub1').attr('style') !== "display: none;"){
                        rsempsub1 = jQuery('input[name=rs-emp-sub1]:checked').val(); 
                    }
                }
            }

            //Stage2 Delivery Plan
            if(jQuery('input[name=rs-del]:checked').val()){
                if(jQuery('#typeofresearch').attr('style') !== "display: none;"){
                    rsdp = jQuery('input[name=rs-del]:checked').val(); 
                }
            }

             //Stage2 The price of the order does not include data collection.
             if(jQuery('input[name=os-dcol]:checked').val()){
                if(jQuery('#osp-9').attr('style') !== "display: none;"){
                    osdcol = jQuery('input[name=os-dcol]:checked').val(); 
                }
            }

             //Stage2 Are you able to send us the data for the analysis?
             if(jQuery('input[name=os-dana]:checked').val()){
                if(jQuery('#osp-17').attr('style') !== "display: none;"){
                osdana = jQuery('input[name=os-dana]:checked').val(); 
                }
            }

             //Stage2 Reflective
             if(jQuery('input[name=e-reflective]:checked').val()){
                if(jQuery('.e-reflective').attr('style') !== "display: none;"){
                    ereflective = parseInt(jQuery('select[name=wordlength3]').val());
                    ereflective = 0.25*levelbaseamount*(ereflective/250); 
                }
            } 

             //Stage2 Presentation
             if(jQuery('input[name=e-presentation]:checked').val()){
                if(jQuery('.e-presentation').attr('style') !== "display: none;"){
                    epresentation = parseInt(jQuery('select[name=slide_length2]').val());
                    //epresentation = levelbaseamount*epresentation; 
                    
                        if(level === "Diploma"){
                            epresentation = diploma_presentation_base*epresentation;
                        }
                        else if(level === "Undergraduate"){
                            epresentation = undergraduate_presentation_base*epresentation;
                        }
                        else if(level === "Masters" || level === "Professional"){
                            epresentation = master_presentation_base*epresentation;
                        }
                        else if(level === "MPhill"){
                            epresentation = mphil_presentation_base*epresentation;
                        }
                        else if(level === "PhD"){
                            epresentation = phd_presentation_base*epresentation;
                        }
                }
            }

             //Stage2 Accompany
             if(jQuery('input[name=e-accompany]:checked').val()){
                if(jQuery('.e-accompany').attr('style') !== "display: none;"){
                    eaccompany = parseInt(jQuery('select[name=wordlength4]').val());
                    eaccompany = 0.25*levelbaseamount*(eaccompany/250); 
                }
            }

            //Stage2 Draft
            if(jQuery('input[name=e-draft]:checked').val()){
                if(jQuery('.e-draft').attr('style') !== "display: none;"){
                    edraft = parseInt(jQuery('select[name=e-draft-sub]').val());    
                }
            }

            //Stage2 Calculation
            if(jQuery('input[name=e-calculation]:checked').val()){
                if(jQuery('.e-calculation').attr('style') !== "display: none;"){
                    ecalculation = 75; 
                }                            
            }

            //Stage2 Report
            if(jQuery('input[name=e-report]:checked').val()){
                if(jQuery('.e-report').attr('style') !== "display: none;"){
                    ereport = 50; 
                }                            
            }

         
            //Stage2 Comment
            if(jQuery('input[name=e-comment]:checked').val()){
                if(jQuery('.e-comment').attr('style') !== "display: none;"){
                    ecomments = 75;                
                }
            }

            // console.log("sidebar"+sidebartotalamount);
            // console.log("consult"+consultation);
            // console.log("osref"+osreflist);
            // console.log("respr"+rsprsub1);
            // console.log("rsemp"+rsempsub1);
            // console.log("osdcol"+osdcol);
            // console.log("osdana"+osdana);
            // console.log("present"+epresentation);
            // console.log("ecompany"+eaccompany);
            // console.log("rsdp"+rsdp);
            // console.log("drft"+edraft);
            // console.log("calculation"+ecalculation);
            // console.log("comments"+ecomments);
            // console.log("ereports"+ereport);            
            
            sidebartotalamount = +sidebartotalamount + +consultation + +osreflist + +rsprsub1 + +rsempsub1 + +osdcol + +osdana + +ereflective + +epresentation + +eaccompany + +rsdp + +edraft + +ecalculation + +ecomments + +ereport;
                  
             //primary and research order 20 pounch commmitment     
            if(ordertype === "primary_research_service" || ordertype === "secondary_research_collation_service"){
                sidebartotalamount = 20;
            }   
            //primary and research order 20 pounch commmitment  

             //Check if restricted subjects
             if(subject === "Architecture" || subject === "Chemistry" || subject === "Physics" || subject === "Biology" || subject === "Mathematics" || subject === "Computer_Engineering" || subject === "Engineering" || subject === "Statistics" || subject === "Finance"){
                if(ordertype === "assignment" || ordertype === "coursework"  || ordertype === "report" || ordertype === "exam_notes" || ordertype === "problem_solving" || ordertype === "research_paper" || ordertype === "plan_model_answer" || ordertype === "case_study_writing_service" || ordertype == "primary_research_service" || ordertype == "secondary_research_collation_service"){
                    sidebartotalamount = 20;
                }            
            }
            
        }
        
        }

        
        
        return sidebartotalamount.toFixed(2);

}

/*
|--------------------------------------------------------------------------
| Calculator Ends
|--------------------------------------------------------------------------
*/  
/*
|--------------------------------------------------------------------------
| Coupon Starts
|--------------------------------------------------------------------------
*/  

function couponapply(){  

            var subtotalinput = document.getElementById("subtotalinput").value;
            //var subtotalinput = document.getElementById("subtotalinputd").value;
            var couponcode = document.getElementById("couponcode").value;
            var discount_percentage = 0;
            var discount = 0;
            
            switch (couponcode) {
            case 'RP83649':
                discount_percentage = 5;
                break;
            case 'RP3430U':
                discount_percentage = 7;
                break;
            case 'RP84638':
                discount_percentage = 8;
                break;
            case 'RP09949':
                discount_percentage = 9;
                break;
            case 'RP23720':
                discount_percentage = 11;
                break;
            case 'RP3948U':
                discount_percentage = 12;
                break;
            case 'RP28720':
                discount_percentage = 13;
                break;
            case 'RP09378':
                discount_percentage = 14;
                break;
            case 'RP0582K':
                discount_percentage = 15;
                break;
            case 'RP04743':
                discount_percentage = 20;
                break;
            case 'RP39646':
                discount_percentage = 25;
                break;
             case 'RP3u646':
                discount_percentage = 30;
                break;
             case 'RP396x6':
                discount_percentage = 50;
                break;

        }

        if(discount_percentage > 0){

            discount = (subtotalinput*discount_percentage/100);
            jQuery('#sidebartotalamountcoupon').html("<br><div class='spanlabelsidebar'>AFTER DISCOUNT GBP " + (subtotalinput-discount).toFixed(2) + "</div>");
            jQuery('#sidebartotalamountinputcoupon').html("<input type='hidden' name='discount' id='discount' value='"+ (discount).toFixed(2) +"'>");
            jQuery('#couponerror').html("Coupon Applied..");
            jQuery('#couponerror').css("color","green");
            jQuery('input[name=couponcode]').prop("readonly", true);
            return discount_percentage;
        }
        // else{
        //     if(couponcode){
        //         jQuery('#sidebartotalamountinputcoupon').html("<input type='hidden' name='discount' id='discount' value='0'>");
        //         jQuery('#sidebartotalamountcoupon').html("");
        //         jQuery('#couponerror').html("Invalid Coupon..");
        //         jQuery('#couponerror').css("color","#ce2626");
        //     }  
        //     else{
        //         jQuery('#couponerror').html("");
        //     } 
                    
        // }
        return discount_percentage;
        
}
/*
|--------------------------------------------------------------------------
| Coupon ends
|--------------------------------------------------------------------------
*/  
    
/*
|--------------------------------------------------------------------------
| Ajax Starts
|--------------------------------------------------------------------------
*/  
// jQuery('#getprice1').on('click', function() {    
//     var student_name = document.getElementById("student_name").value;	
//     var email = document.getElementById("student_email").value;	
//     var phone = document.getElementById("phone").value;	
//     var country = document.getElementById("country").value;	
//     var academic_level = document.getElementById("academic_level").value;	
//     var subjects = document.getElementById("subjects").value;	
//     var writing_type = jQuery('input[name=writing_type]:checked', '#try').val();	

//     if(writing_type == "writing"){
//       var order_type = jQuery('input[name=writing_order_type]:checked', '#try').val();		
//     }
//     else {
//       var order_type = jQuery('input[name=editing_order_type]:checked', '#try').val();	
//     }
//           if(email && student_name && phone){
//             $.ajax({
//               type: "post",
//               data:{"student_name":student_name,"email":email,"phone":phone,"country":country,"academic_level":academic_level,"writing_type":writing_type,"order_type":order_type,"subjects":subjects},
//               dataType: "html",
//               url: "Orderform/stage1data",
//               cache: false,               
//               /*beforeSend: function() {
//                   // setting a timeout
//                   $('.loader').show();
//               },*/
//               success: function(htmldata) {
//                  $('#payables').html(htmldata);
//                  //$('.loader').hide();
//               }
//             });              
//           }
// });  

///////////////////////////////////////
//////////////////////////////////////

// jQuery('#gostage3').on('click', function() {    
         
//     if(!jQuery('#try').valid()){
//            $('html, body').animate({
//                scrollTop: ($('.error:visible').offset().top - 60)
//            }, 1000);
//            return false;
//        }
 
//    var agree = jQuery('input[name=agree]:checked', '#try').val(); 
      //var actualamount = document.getElementById("subtotalinput").value;
      //var actualamount = parseFloat(jQuery("#subtotalinput").val());
//    jQuery('input[name=invoicetotal]').val(actualamount);
//    jQuery('#finalgrandtotalfull').html(actualamount);
   
   
//    var install = +actualamount + +actualamount*0.05;
//    install = install.toFixed(2);
//    jQuery('#finalgrandtotalfull2').html(install);
//    install = install/2;
//    install = install.toFixed(2);
   
//    jQuery('#finalgrandtotalpart1').html(install);
//    jQuery('#finalgrandtotalpart2').html(install);
//    jQuery('.actualamount3').html(install);
//    jQuery('.actualamount4').html(install);
//    jQuery('.actualamount5').html(install);

//    if(actualamount < 200){
//         jQuery('#twopayheader2').hide();
//         jQuery('#twopayheader1').removeClass("col-lg-6");
//         jQuery('#twopayheader1').addClass("col-12");

//    }

//    var data = new FormData(this.form);
   
//    if(jQuery('#try').valid()){
//    if(agree){
//          $.ajax({
//            type: "post",
//            data:data,
//            processData: false,
//            contentType: false,
//            url: "Orderform/stage2data",
//            cache: false,        
//            beforeSend: function() {
//                // setting a timeout
//                jQuery('#spinner_waiter').css({"display": "flex"});	
              
//            },
//            success: function(htmldata) {
//               $('#ajaxordernumber').html(htmldata);
//               $('#ajaxordernumber2').html(htmldata);
//               $('#ajaxordernumber21').html(htmldata);
//               $('#ajaxordernumber2-1').html(htmldata);
//               $('#ajaxordernumber21-1').html(htmldata);
//               jQuery('#spinner_waiter').css({"display": "none"});	
//               window.location = "https://order.researchprospect.com/Orderform/order/"+htmldata;
//            }
//          });              
//        }}
//    });   

////////////////////////
///////////////////////

// jQuery('#paynow').on('click', function() {    
      
//     var payamount = jQuery('input[name=payamount]', '#try').val();         
//     var data = new FormData(this.form);
//     if(payamount){
//           $.ajax({
//             type: "post",
//             data:data,
//             processData: false,
//             contentType: false,
//             url: "Orderform/pay",
//             cache: false,               
//             success: function(htmldata) {
//                $('#payables').html(htmldata);
//             }
//           });              
//         }
//     });   

///////////////////////
//////////////////////
jQuery(document).ready(function(){ 

        //Apply Coupon Code
        jQuery('#couponcodebutton').on('click', function() { 
                
            var discount_percentage = couponapply();
            var formstage = document.getElementById("formstage").value;
            var subtotalplus = 0;

            //REferal
           
            // if(discount_percentage < 1){
            //     var couponcode = document.getElementById("couponcode").value;
            // }
            
            //Referel
            if(formstage > 0){
                    if(discount_percentage > 0){    
                        var couponcode = document.getElementById("couponcode").value;
                        var ordernum = document.getElementById("ordernum").value;
                        document.getElementById("couponcodebutton").disabled = true;

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                            });
                            $.ajax({
                                url:"/coupon",
                                type:"get",
                                data:{"discount_percentage":discount_percentage,"couponcode":couponcode,"ordernum":ordernum},
                                dataType:"html",
                                beforeSend:function(){
                                    jQuery('#spinner_waiter').css({"display": "flex", "position": "fixed", "top": "10px", "left": "30%"});
                                },
                                success: function (response) {
                                    location.reload();
                                },
                                error: function (error) {
                                    // $(".alert-not-found").removeClass("d-none");
                                },
                                complete:function () {
                                    jQuery('#spinner_waiter').css({"display": "none", "position": "fixed", "top": "10px", "left": "30%"});
                                }
                            });                       
                
                    }
            }
            
        });  
    });  

///////////////////////
//////////////////////

jQuery(document).ready(function(){  
    jQuery('#changecurrency').on('click', function() { 
       
        jQuery("#ModalCurrency").modal();

        jQuery("#currencyid").on('click', function() { 

            var currency = document.getElementById("currencyid").value; 
            var actualamount = parseFloat(jQuery("#subtotalinput").val());
            var conversion = 0;
            // console.log(currency);

            var settings = {
                "url": "https://api.exchangeratesapi.io/latest?base=GBP&symbols="+currency+"",
                "method": "GET",
                "timeout": 0,
            };

            
        

            $.ajax(settings).done(function (response) {
                var conversion = JSON.stringify(response.rates);
                conversion = conversion.substring(7, 12);
                conversion = parseFloat(conversion);

                var displayamount = (conversion*actualamount).toFixed(2);
                jQuery("#currencytotal").html(currency+" "+displayamount);
            
            });

            

            // $('#ModalCurrency').modal('toggle');
        });

        
        
    });   

    ///Part pay and full pay box size
    var actualamount = parseFloat(jQuery("#subtotalinput").val());
    var paidamount = parseFloat(jQuery("#paidamount").val());

    if(actualamount < 200 || paidamount > 0){
        jQuery('#twopayheader2').hide();
        jQuery('#twopayheader1').removeClass("col-lg-6");
        jQuery('#twopayheader1').addClass("col-12");

   }

});  


/*
|--------------------------------------------------------------------------
| Ajax Ends
|--------------------------------------------------------------------------
*/ 

/*
|--------------------------------------------------------------------------
| By Umair Starts
|--------------------------------------------------------------------------
*/ 
if($(window).innerWidth() < 768) {
    jQuery('#my-order-btn-head').prependTo('.myMainMenu');
    jQuery('#my-order-btn-head').insertAfter('.navbar-toggler');
}
/*
|--------------------------------------------------------------------------
| By Umair Ends
|--------------------------------------------------------------------------
*/ 

