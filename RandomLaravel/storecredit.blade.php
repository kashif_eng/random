@extends('backend.layouts.bklayout')

@section('content')
      		
		<!-- BEGIN #content -->
		<div id="content" class="app-content">
			<!-- BEGIN container -->
			<div class="container">
				<!-- BEGIN row -->
				<div class="row justify-content-center">

							<!-- BEGIN col-12 -->
							<div class="col-xl-12">							
								
								<h1 class="page-header">
									Store Credit <small></small>
								</h1>
								
                                <hr class="mb-4" />
                                
                                <p>
                                    This is the money you can use to pay for your future orders. Simply choose "Store Credit" as the payment option when placing an order or contact our Support Team and ask them to apply your Store Credit via live chat.
                                </p>
                                <div class="alert alert-primary">
                                    <strong>Your Store Credit balance:</strong> GBP {{$credit}}
                                </div>

                                <div class="row">
                                    <div class="col-xl-6">
                                        <h1 class="page-header">
                                            How to earn <small></small>
                                        </h1>
                                        Give a discount to your friend and earn money with our Referral Program!
                                        Transfer the bonuses you get from our Loyalty Program!
                                        Receive money back for price recalculations or cancellations of your orders.	
                                    </div>
                                    <div class="col-xl-6">	
                                        <h1 class="page-header">
                                            Benefits <small></small>
                                        </h1>
                                        No waiting. Instant payment for your order.
                                        No additional transaction fees.
                                        No payment verification needed.
                                        Life-time availability of the money.
                                        Immediate transfer to Store Credit, while refund takes 5-7 days.
                                    </div>                                   
                                   
                                </div>

                                <hr class="mb-4" />
                                
                                <div class="row">    
                                    <div class="col-xl-12">	
                                        <h1 class="page-header">
                                            My Store Credit Statistics	 <small></small>
                                        </h1>
                                        Here you can track your store credit transactions. Please note that the sums stored as Credit are not subject to refund, which means that you can only use them as payment for your orders.
                                    </div>
                                </div>
                            
                            </div>
							<!-- END col-12-->
							
						</div>
						<!-- END row -->				
			</div>
			<!-- END container -->
		</div>
		<!-- END #content -->

 
@endsection
