<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Utils\AppConst;
use App\Models\Api;
use App\Models\Variant;
use App\Models\ProductListing;
use App\Models\ProductMeta;
use App\Models\ProductCat;
use App\Models\Store;
use App\Models\ShippingMethod;
use App\Models\PaymentMethod;
use App\Models\Purchase;
use App\Models\PurchaseItems;
use App\Models\AccountsMeta;
use App\Models\Vendor;






class PurchaseController extends Controller
{
    
    private $api,$productMeta;
    public function __construct(Api $api,productMeta $productMeta)
    {
        $this->middleware('auth');
        $this->api = $api;
        $this->productMeta = $productMeta;

    }

    

    public function purchasePage()
    {           
        return view('backend.purchase.purchasepage');     
    }

    public function purchaseAjax()
    {   
        $purchaseInfo = Purchase::get();
        foreach($purchaseInfo as $k=>$purchase){
            $purchaseInfo[$k]->vendor_name = Vendor::where('id',$purchase->vendor_id)->pluck('vendor_name')->first();            
            $purchaseInfo[$k]->purchase_item_count = PurchaseItems::where('purchase_id',$purchase->id)->sum('order_quantity');
        }
        
        return view('backend.purchase.purchaseajax',compact('purchaseInfo'));     
    }

    public function purchaseAddPage()
    {           
        $vendors = Vendor::where('status','Enabled')->get();
        $products = ProductListing::where('product_status','Enabled')->get();
        foreach($products as $k=>$product){
            $products[$k]->variant_value = json_decode(ProductMeta::where('product_id',$product->id)->where('meta_key','variant_value')->pluck('meta_value')->first());
        }
        $payment_method = PaymentMethod::where('status','Enabled')->get();
       
        $store = Store::first();
        
        return view('backend.purchase.purchaseaddpage',compact('store','vendors','products','payment_method'));     
    }

    public function purchaseAdd(Request $request)
    {        
        //dd($request->all());
        if(!$request->product_ids){
             return redirect()->back()->with('failed',AppConst::noproducts)->withInput();
        }              
        //Validation        
        //dd($request->all());  

        //Store
        $store = Store::first();

        //Order Number
        $latest_purchase = Purchase::latest()->first();
        if($latest_purchase){
            $purchase_number = $latest_purchase->id+1;
            $purchase_name = $store->prefix_purchase.$purchase_number;
        }
        else{
            $purchase_number = 1;
            $purchase_name = $store->prefix_purchase.$purchase_number;
        } 

        //Vendor Info
        $vendor = Vendor::where('id',$request->vendor_id)->first();
        
        $purchaseInfo = Purchase::create([
            'purchase_transaction_number' => $purchase_name,               
            'vendor_id' => $request->vendor_id,   
            'purchase_date' => $request->purchase_date,   
            'purchase_amount' => $request->grand_total,                          
            'delivery_amount' => $request->delivery_amount,            
            'other_amount' => $request->other_amount,            
            'purchase_payment_mode' => $request->purchase_payment_mode,     
            'purchase_reference' => $request->purchase_reference,            
            'purchase_description' => $request->purchase_description,           
         
        ]);

        //Products
        foreach($request->product_ids as $k=>$product){

            $product_id = explode("_",$product)[0];
            $variant_position = explode("_",$product)[1];

            $the_product = ProductListing::where("id",$product_id)->first();
            if($the_product->product_type == "simple_product"){
                $variant_title = NULL;
                $variant_value = NULL;
                $variant_sku = NULL;
                $variant_barcode = NULL;
               
            }
            else{
                $variant_title = json_decode(ProductMeta::where("product_id",$product_id)->where('meta_key','variant_title')->pluck('meta_value')->first())[$variant_position];
                $variant_value = json_decode(ProductMeta::where("product_id",$product_id)->where('meta_key','variant_value')->pluck('meta_value')->first())[$variant_position];
                $variant_sku = json_decode(ProductMeta::where("product_id",$product_id)->where('meta_key','variant_sku')->pluck('meta_value')->first())[$variant_position];
                $variant_barcode = json_decode(ProductMeta::where("product_id",$product_id)->where('meta_key','variant_barcode')->pluck('meta_value')->first())[$variant_position];
               
            }

            $price = $request->line_total[$k];

            $purchaseItems = PurchaseItems::create([
                'purchase_id' => $purchaseInfo->id,  
                'purchase_transaction_number' => $purchase_name,   
                'product_id' => $product_id,           
                'variant_position' => $variant_position,                          
                'product_type' => $the_product->product_type,            
                'product_title' => $the_product->title,            
                'product_sku' => $the_product->sku,     
                'variant_title' => $variant_title,            
                'variant_value' => $variant_value,            
                'variant_sku' => $variant_sku,            
                'variant_barcode' => $variant_barcode,            
                'order_quantity' => $request->line_qty[$k],            
                'price' => $price,                                               
            ]);


        }    


        return redirect()->route('purchasePage')->with('success',AppConst::success);
    
    }

    public function purchaseUpdatePage($id)
    {           
        $recordExist = Purchase::where('id',$id)->first();
        if(!$recordExist){
            return redirect()->back()->with('failed',AppConst::norecord)->withInput();
        }

        $vendors = Vendor::where('status','Enabled')->get();
        // $products = ProductListing::where('product_status','Enabled')->get();
        // foreach($products as $k=>$product){
        //     $products[$k]->variant_value = json_decode(ProductMeta::where('product_id',$product->id)->where('meta_key','variant_value')->pluck('meta_value')->first());
        // }
        $payment_method = PaymentMethod::where('status','Enabled')->get();       
        $store = Store::first();
               
        $purchaseInfo = Purchase::where('id',$id)->first();
        $purchaseItem = PurchaseItems::where('purchase_id',$id)->get();
        
        foreach($purchaseItem as $query){

            $product_id = $query->product_id;
            $variant_position = $query->variant_position;

            $the_product = ProductListing::where('id',$product_id)->where('product_status','Enabled')->first();
            $the_product->variant_value = json_decode(ProductMeta::where('product_id',$product_id)->where('meta_key','variant_value')->pluck('meta_value')->first())[$variant_position];
            $the_product->variant_price = json_decode(ProductMeta::where('product_id',$product_id)->where('meta_key','variant_price')->pluck('meta_value')->first())[$variant_position];
            $the_product->variant_sku = json_decode(ProductMeta::where('product_id',$product_id)->where('meta_key','variant_sku')->pluck('meta_value')->first())[$variant_position];
            $the_product->primary_image = ProductMeta::where('product_id',$product_id)->where('meta_key','primary_image')->pluck('meta_value')->first();
            $the_product->variant_position = $variant_position;
            $the_product->order_quantity = $query->order_quantity;
            $the_product->purchase_item_id = $query->id;
            $the_product->price = $query->price;



            //dd($the_product->product_meta);
            $products[] = $the_product;
        }
        //dd($products);
        $metaGallaryImages = AccountsMeta::where('meta_type','purchase_attachment')->where('meta_id',$id)->where('meta_key','purchase_attachment')->get();       

        if($metaGallaryImages){
            $metaGallaryImagesImplode = "";
            foreach($metaGallaryImages as $k=>$gImage){
                if($metaGallaryImagesImplode){
                    $metaGallaryImagesImplode = $metaGallaryImagesImplode."|".$gImage->meta_value;
                }
                else{
                    $metaGallaryImagesImplode = $metaGallaryImagesImplode.$gImage->meta_value;
                }
            }
            
        }
       
        return view('backend.purchase.purchaseupdatepage',compact('store','vendors','products','payment_method','purchaseInfo','metaGallaryImages','metaGallaryImagesImplode'));      
    }

    public function purchaseUpdate(Request $request)
    {        
        
       if(!$request->product_ids){
            return redirect()->back()->with('failed',AppConst::noproducts)->withInput();
       }              
       //Validation        
       
       //Store
       $store = Store::first();

       //Purchase Name
       $purchase_name = Purchase::where('id',$request->id)->pluck('purchase_transaction_number')->first();       

        //Vendor Info
        $vendor = Vendor::where('id',$request->vendor_id)->first();
            
        $purchaseInfo = Purchase::where('id',$request->id)->update([                        
            'vendor_id' => $request->vendor_id,   
            'purchase_date' => $request->purchase_date,   
            'purchase_amount' => $request->grand_total,                          
            'delivery_amount' => $request->delivery_amount,            
            'other_amount' => $request->other_amount,            
            'purchase_payment_mode' => $request->purchase_payment_mode,     
            'purchase_reference' => $request->purchase_reference,            
            'purchase_description' => $request->purchase_description,           
        
        ]);

       //Products
       foreach($request->product_ids as $k=>$product){

            $product_id = explode("_",$product)[0];
            $variant_position = explode("_",$product)[1];
            $purchase_item_id = $request->purchase_item_id[$k];

            $the_product = ProductListing::where("id",$product_id)->first();
            if($the_product->product_type == "simple_product"){
                $variant_title = NULL;
                $variant_value = NULL;
                $variant_sku = NULL;
                $variant_barcode = NULL;
            
            }
            else{
                $variant_title = json_decode(ProductMeta::where("product_id",$product_id)->where('meta_key','variant_title')->pluck('meta_value')->first())[$variant_position];
                $variant_value = json_decode(ProductMeta::where("product_id",$product_id)->where('meta_key','variant_value')->pluck('meta_value')->first())[$variant_position];
                $variant_sku = json_decode(ProductMeta::where("product_id",$product_id)->where('meta_key','variant_sku')->pluck('meta_value')->first())[$variant_position];
                $variant_barcode = json_decode(ProductMeta::where("product_id",$product_id)->where('meta_key','variant_barcode')->pluck('meta_value')->first())[$variant_position];
            
            }

            $price = $request->line_total[$k];

            $purchaseItems = PurchaseItems::where('id',$purchase_item_id)->update([  
                'order_quantity' => $request->line_qty[$k],            
                'price' => $price,                                               
            ]);


        }     

        //Gallary Images       
        if($request->gallary_images_value){
            $gallary = explode("|",$request->gallary_images_value);
            foreach($gallary as $image){
                $data = array(
                    'meta_type' => 'purchase_attachment',
                    'meta_id' => $request->id,
                    'meta_info' => 'Purchase Attachment',
                    'meta_key' => 'purchase_attachment',
                    'meta_value' => $image,
                 );
     
                 if($request->id){
                    $matchThese = ['meta_type'=>$data["meta_type"],'meta_id'=>$data["meta_id"],'meta_key'=>$data["meta_key"],'meta_value'=>$data["meta_value"]];
                    AccountsMeta::updateOrCreate($matchThese,$data);                   
                 }
            }
            
        }   
       

        return redirect()->route('purchaseUpdatePage',$request->id)->with('success',AppConst::success);
    
    }

    // public function paymentAdd(Request $request)
    // {           
    //     $recordExist = OrdersInfo::where('id',$request->order_id_for_payment)->first();
    //     if(!$recordExist){
    //         return redirect()->back()->with('failed',AppConst::norecord)->withInput();
    //     }

    //     $orderInfo = $recordExist;
    //     $store = Store::first();

    //     //Invoice Number
    //     $latest_invoice = OrdersPayment::latest()->first();
    //     if($latest_invoice){
    //         $invoice_number = $latest_invoice->id+1;
    //         $invoice_name = $store->prefix_transaction.$invoice_number;
    //     }
    //     else{
    //         $invoice_number = 1;
    //         $invoice_name = $store->prefix_transaction.$invoice_number;
    //     } 

    //     $payment_method_title = PaymentMethod::where('id',$request->payment_method_id)->pluck('title')->first();

    //     //dd($request->all());
        
    //     $ordersPayment = OrdersPayment::create([               
    //         'order_id' => $request->order_id_for_payment,   
    //         'order_number' => $orderInfo->order_number,           
    //         'invoice_number' => $invoice_name,                          
    //         'invoice_status' => 'Paid',            
    //         'order_grand_total' => $orderInfo->order_grand_total,            
    //         'invoice_amount' => $request->amount,     
    //         'payment_method_id' => $request->payment_method_id,           
    //         'payment_method_title' => $payment_method_title,         
    //         'payment_notes' => $request->notes,                                         
    //     ]);

    //     $ordersInfo = OrdersInfo::where('id',$request->order_id_for_payment)->update([               
    //         'order_bill_number' => $request->bill_number, 
    //         'order_payment_status' => 'Paid',                                        
    //     ]);

    //     return redirect()->route('purchaseUpdatePage',$request->order_id_for_payment)->with('success',AppConst::success);

    // }


    public function purproductSearchAjax(Request $request)
    {           
        foreach($request->searchQuery as $query){

            $product_id = explode('_',$query)[0];
            $variant_position = explode('_',$query)[1];

            $the_product = ProductListing::where('id',$product_id)->where('product_status','Enabled')->first();
            $the_product->variant_value = json_decode(ProductMeta::where('product_id',$product_id)->where('meta_key','variant_value')->pluck('meta_value')->first())[$variant_position];
            $the_product->variant_price = json_decode(ProductMeta::where('product_id',$product_id)->where('meta_key','variant_price')->pluck('meta_value')->first())[$variant_position];
            $the_product->variant_sku = json_decode(ProductMeta::where('product_id',$product_id)->where('meta_key','variant_sku')->pluck('meta_value')->first())[$variant_position];
            $the_product->primary_image = ProductMeta::where('product_id',$product_id)->where('meta_key','primary_image')->pluck('meta_value')->first();
            $the_product->variant_position = $variant_position;

            //dd($the_product->product_meta);
            $products[] = $the_product;
        }
        
        $store = Store::first();

        return view('backend.purchase.purproductsearchajax',compact('products','store'));     
    }
    

    public function fileUploadingPurchase(Request $request)
    {   
        //dd($_FILES);
        $myFile = $_FILES["fileInfo"]; 
        $fileCount = count($myFile);   
        $time = time();
        
        //for ($i = 0; $i < $fileCount; $i++) {
                    
            $fileTmpPath = $myFile["tmp_name"];
            $fileName = $myFile["name"];
            $fileSize = $myFile["size"];
            $fileType = $myFile["type"];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));

            $newFileName = $fileNameCmps[0] . '-' . $time . '.' . $fileExtension;

            // directory in which the uploaded file will be moved
            $uploadFileDir = './assets/purchase_images/';
            $dest_path = $uploadFileDir . $newFileName;

            $allowedfileExtensions = array('jpg','png','jpeg','JPG','JPEG','PNG');
            if (in_array($fileExtension, $allowedfileExtensions)) {
                
                if($fileSize < 2000000) {
                    if(move_uploaded_file($fileTmpPath, $dest_path))
                    {
                        $message ='File is successfully uploaded.';
                        $data = array(
                            'status'=>'success',
                            'message'=>$message,
                            'filename'=>$newFileName
                        );
                       

                        $response = json_encode($data);
                        echo $response;
                    }
                    else
                    {
                        $message = 'There was some error moving the file to upload directory.';
                        $data = array(
                            'status'=>'failed',
                            'message'=>$message,
                            'filename'=>''
                        );
                        $response = json_encode($data);
                        echo $response;
                    }
                }


            }
            else{
                $message = 'There was some error moving the file to upload directory.';
                $data = array(
                    'status'=>'failed',
                    'message'=>$message,
                    'filename'=>''
                );
                $response = json_encode($data);
                echo $response;
            }
                                
        //}        
        
    }

}