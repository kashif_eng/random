@extends('backend.layouts.bklayout')

@section('content')
      		
<div id="content" class="app-content p-0">

    <form action="{{route('productUpdate')}}" method="POST"  enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$productInfo->id}}">
        <div class="d-block d-md-flex align-items-stretch h-100">
      
       <div class="gallery-content-container">
                    
          <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
             <div data-scrollbar="true" data-height="100%" data-init="true" style="overflow: hidden; width: auto; height: 100%;">
                <div class="gallery-content">
                   <div class="gallery">
                      <div class="d-flex align-items-center mb-3">
                        <h1 class="page-header" style="margin:unset;">
                         {{$productInfo->title}}   <small>Update Product</small>
                        </h1>  
                      </div>
                      <hr>

                      <!-------Errors------>
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-success  alert-dismissible fade show" role="alert">
                                {{session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('failed'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{session()->get('failed')}}
                            </div>
                        @endif                   
                        <!-------Errors------>
                                              

                        
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Title</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="title" class="form-control" value="{{$productInfo->title}}" required>
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Description</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <textarea type="text" name="description" class="form-control summernote" value="" >{{$productInfo->description}}</textarea>
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                </div>
                            </div>
                            

                            <hr>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Regular Price</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="number" name="regular_price" class="form-control" value="{{$productInfo->regular_price}}" min="0" step="0.1"  required>
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	 
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Special Price</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="number" name="special_price" class="form-control" value="{{$productInfo->special_price}}" min="0" step="0.1" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Cost Per Item</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="number" name="cost_price" class="form-control" value="{{$productInfo->cost_price}}" min="0" step="0.1" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Sku</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="sku" class="form-control" value="{{$productInfo->sku}}" required>
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	 
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Barcode</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="barcode" class="form-control" value="{{$productInfo->barcode}}" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Overselling</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <select name="overselling" class="form-control" required>
                                                            <option value="Enabled" @if($productInfo->overselling == "Enabled") {{"selected"}} @endif>Enabled</option>
                                                            <option value="Disabled" @if($productInfo->overselling == "Disabled") {{"selected"}} @endif>Disabled</option>                                                           
                                                         </select>
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Quantity</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="number" name="quantity" class="form-control" value="{{$productInfo->quantity}}" required min="0" step="1" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                </div>
                            </div>

                            
                            <hr>
                            <div class="form-group">
                                <label>Product Type</label>
                                <div class="row row-space-10">
                                    <div class="col-12">
                                        {{-- <select name="product_type" id="product_type" class="form-control" required >
                                            <option value="simple_product" >Simple Product</option>
                                            <option value="variable_product">Variable Product</option>
                                         </select> --}}
                                         <input type="text" name="product_type" id="product_type" class="form-control" value="{{$productInfo->product_type}}" required readonly>
                                    </div>								
                                </div>
                            </div>
                            <div class="card" id="configsection" style="display:none;">
                                <div class="card-body">
                                    <div class="rowVar" style="display: none;">
                                    <div class="row"  style=" border:1px dotted;margin-bottom:5px;">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Variant Title</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <select name="variant_title[]" class="form-control variant_title" >
                                                            @foreach($variants as $variant)
                                                                <option value="{{$variant->id}}" >{{$variant->variant_title}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	 
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Variant Value</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="variant_value[]" class="form-control variant_value" value="" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Variant Sku</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="variant_sku[]" class="form-control variant_sku" value="" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Variant Barcode</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="variant_barcode[]" class="form-control variant_barcode" value="" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	
                                        <div class="col-3">
                                            
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Variant Price</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="number" name="variant_price[]" class="form-control variant_price" value="0"  min="0" step="0.1">
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Variant Quantity</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="number" name="variant_quantity[]" class="form-control variant_quantity" value="0"  min="0" step="0.1">
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>                                        
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label></label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                       <button type="button" class="btn btn-danger btnDelVar" style="float:right;">Delete</button>
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>    
                                    </div>       


                                    <div class="moreVar">
                                        @foreach($metaVariantTitleName as $k=>$variantone)
                                        <div class="row"  style=" border:1px dotted;margin-bottom:5px;">
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label>Variant Title</label>
                                                    <div class="row row-space-10">
                                                        <div class="col-12">
                                                            <select name="variant_title[]" class="form-control variant_title" >
                                                                @foreach($variants as $variant)
                                                                    <option value="{{$variant->id}}" @if($variant->id == $metaVariantTitleId[$k]) {{"selected"}} @endif>{{$variant->variant_title}} </option>
                                                                @endforeach
                                                            </select>
                                                        </div>								
                                                    </div>
                                                </div>	
                                            </div>	 
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label>Variant Value</label>
                                                    <div class="row row-space-10">
                                                        <div class="col-12">
                                                            <input type="text" name="variant_value[]" class="form-control variant_value" value="{{$metaVariantValue[$k]}}" >
                                                        </div>								
                                                    </div>
                                                </div>	
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label>Variant Sku</label>
                                                    <div class="row row-space-10">
                                                        <div class="col-12">
                                                            <input type="text" name="variant_sku[]" class="form-control variant_sku" value="{{$metaVariantSku[$k]}}" >
                                                        </div>								
                                                    </div>
                                                </div>	
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label>Variant Barcode</label>
                                                    <div class="row row-space-10">
                                                        <div class="col-12">
                                                            <input type="text" name="variant_barcode[]" class="form-control variant_barcode" value="{{$metaVariantBarcode[$k]}}" >
                                                        </div>								
                                                    </div>
                                                </div>	
                                            </div>	
                                            <div class="col-3">
                                                
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label>Variant Price</label>
                                                    <div class="row row-space-10">
                                                        <div class="col-12">
                                                            <input type="number" name="variant_price[]" class="form-control variant_price" value="{{$metaVariantPrice[$k]}}"  min="0" step="0.1">
                                                        </div>								
                                                    </div>
                                                </div>	
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label>Variant Quantity</label>
                                                    <div class="row row-space-10">
                                                        <div class="col-12">
                                                            <input type="number" name="variant_quantity[]" class="form-control variant_quantity" value="{{$metaVariantQuantity[$k]}}"  min="0" step="0.1">
                                                        </div>								
                                                    </div>
                                                </div>	
                                            </div>                                        
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label></label>
                                                    <div class="row row-space-10">
                                                        <div class="col-12">
                                                           <button type="button" class="btn btn-danger btnDelVar" style="float:right;">Delete</button>
                                                        </div>								
                                                    </div>
                                                </div>	
                                            </div>	                                
                                        </div>    
                                        @endforeach
                                    </div>
                                    
                                    <button type="button" class="btn btn-link" id="btnAddVar">+ Add Variation</button>

                                </div>
                            </div>


                            <hr>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Weight</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="number" name="weight" class="form-control" value="{{$productInfo->weight}}" min="0" step="0.1" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	 
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Unit</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <select name="unit" class="form-control" required>
                                                            @foreach($units as $unit)
                                                                <option value="{{$unit->id}}" @if($unit->id == $productInfo->unit) {{"selected"}} @endif>{{$unit->unit_symbol}}</option>
                                                            @endforeach
                                                         </select>
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Notes</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="notes" class="form-control" value="{{$productInfo->notes}}" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                                                    
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>SEO Title</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="seo_title" class="form-control" value="{{$productInfo->seo_title}}" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	 
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>SEO Description</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="seo_description" class="form-control" value="{{$productInfo->seo_description}}" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>SEO Url</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="text" name="seo_url" class="form-control" value="{{$productInfo->seo_url}}" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                                                    
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <span id="uploadresponse" style="color:red;font-weight:700;"></span>
                                            <br>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Primary Image</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="file" name="primary_image" class="form-control" value="" id="primary_image">
                                                        <input type="hidden" name="primary_image_value" class="form-control" value="@if($metaPrimaryImage){{$metaPrimaryImage->meta_value}}@endif" id="primary_image_value">
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Gallary Images</label>
                                                <div class="row row-space-10">
                                                    <div class="col-12">
                                                        <input type="file" name="gallary_images" class="form-control" value="" id="gallary_images" >
                                                        <input type="hidden" name="gallary_images_value" id="gallary_images_value" class="form-control" value="@if($metaGallaryImagesImplode){{$metaGallaryImagesImplode}}@endif" >
                                                    </div>								
                                                </div>
                                            </div>	
                                        </div>	                                
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="gallery-image">
                                                <ul class="gallery-image-list  primary_image_list" data-pswp-uid="1">
                                                    @if($metaPrimaryImage)
                                                        <li><a href="#" itemprop="contentUrl" data-size="752x502"><img src="/assets/product_images/{{$metaPrimaryImage->meta_value}}" itemprop="thumbnail" alt="" class="img-portrait"></a></li>
                                                    @endif
                                                 </ul>
                                                <ul class="gallery-image-list gallary_images_list" data-pswp-uid="1">
                                                    @if($metaGallaryImages)
                                                        @foreach($metaGallaryImages as $glal)
                                                                <li><a href="#" itemprop="contentUrl" data-size="752x502"><img src="/assets/product_images/{{$glal->meta_value}}" itemprop="thumbnail" alt="" class="img-portrait"></a></li>
                                                        @endforeach
                                                   @endif
                                                </ul>
                                             </div>
                                        </div>	                                
                                    </div>
                                </div>
                            </div>



                        

                      
                   </div>
                </div>
             </div>
             <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 38.8239px;"></div>
             <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
          </div>
       </div>



       <div class="gallery-menu-container" style="min-width: 20rem;">
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
           <div data-scrollbar="true" data-height="100%" data-skip-mobile="true" data-init="true" style="overflow: hidden; width: auto; height: 100%;margin-right:10px;">
              <div class="gallery-menu">

                 <div class="gallery-menu-header">&nbsp;</div>
                 {{-- <div class="gallery-menu-item">
                    <div class="form-group">
                        <label><b>Product Type</b></label>
                        <div class="row row-space-10">
                            <div class="col-12">
                                <select name="product_type" id="product_type" class="form-control" required>
                                    <option value="simple_product">Simple Product</option>
                                    <option value="variable_product">Variable Product</option>
                                 </select>
                            </div>								
                        </div>
                    </div>
                 </div>                 --}}
                 <div class="gallery-menu-item">
                    <div class="form-group">
                        <label><b>Product Status</b></label>
                        <div class="row row-space-10">
                            <div class="col-12">
                                <select name="product_status" class="form-control" required>
                                    <option value="Enabled" @if($productInfo->product_status == "Enabled") {{"selected"}} @endif>Enabled</option>
                                    <option value="Disabled" @if($productInfo->product_status == "Disabled") {{"selected"}} @endif>Disabled</option>
                                    <option value="Drafted" @if($productInfo->product_status == "Drafted") {{"selected"}} @endif>Drafted</option>
                                 </select>
                            </div>								
                        </div>
                    </div>
                 </div>
                 <div class="gallery-menu-item">
                    <div class="form-group">
                        <label><b>Product Categories</b></label>
                        <div class="row row-space-10">
                            <div class="col-12">
                                <select type="text" value="" name="product_categories[]" id="product_categories" class="selectpicker form-control" data-live-search="true"  data-size="5" data-actions-box="true" multiple placeholder="" required>                                    
                                    @foreach($cat as $ct)
                                        <option value="{{$ct->id}}" @if(in_array($ct->id,$metaCategoriesSP)) {{"selected"}} @endif>{{$ct->cat_title}}</option>
                                    @endforeach
                                </select>
                            </div>								
                        </div>
                    </div>
                 </div>                 
                 <div class="gallery-menu-item">
                    <div class="form-group">
                        <label><b>Product Brand</b></label>
                        <div class="row row-space-10">
                            <div class="col-12">
                                <select name="brand" class="form-control">
                                    <option value="Qarshi" @if($productInfo->brand == "Qarshi") {{"selected"}} @endif>Qarshi</option>
                                    <option value="Hamdard" @if($productInfo->brand == "Hamdard") {{"selected"}} @endif>Hamdard</option>                               
                                 </select>
                            </div>								
                        </div>
                    </div>
                 </div>
                 <div class="gallery-menu-item">
                    <div class="form-group">
                        <label><b>Product Tags</b></label>
                        <div class="row row-space-10">
                            <div class="col-12">
                                <ul id="jquery-tagit" class="tagit form-control" style="height: fit-content;max-width: fit-content;min-width: -webkit-fill-available;" >
                                    @if($metaTags)
                                        @php $tags = json_decode($metaTags->meta_value); @endphp
                                        @foreach($tags as $tag)
                                            <li>{{$tag}}</li>
                                        @endforeach 
                                    @endif
                                </ul>
                            </div>								
                        </div>
                    </div>
                 </div>


                 <div class="gallery-menu-item">
                    <div class="form-group">                        
                        <div class="row row-space-10">
                            <div class="col-12">
                                <button type="submit" class="btn btn-success btn-block">Update Product</button>
                            </div>								
                        </div>
                    </div>
                 </div>

                 
              </div>
           </div>
           <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 543px;"></div>
           <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
        </div>
     </div>

    </div>
    </form>
 </div>
 
@endsection

@section('script')
<script>
$('.summernote').summernote({
    height: 200,
    toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'insert', [ 'link'] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
});
</script>
<script>
    $(document).ready(function() {
      $('#jquery-tagit').tagit({
        fieldName: 'tags[]',
        availableTags: ['heart', 'headace', 'spinal', 'treatment', 'pain', 'therapay', 'exercise'],
        autocomplete: {
          delay: 0, 
          minLength: 2
        }
      });
    });
</script>
<script>
    $(document).ready(function(){
        var productType = $("#product_type").val();
        if(productType === "simple_product"){
            $("#configsection").hide();
        }
        else{
            $("#configsection").show();
        }        
    });
</script>
<script>
$("#product_type").change(function(){
    var productType = $("#product_type").val();
    if(productType === "simple_product"){
        $("#configsection").hide();
    }
    else{
        $("#configsection").show();
    }        
});
</script>
<script>
    $("#btnAddVar").click(function(){        
        var rowVar = $(".rowVar").html();        
        $(".moreVar").append(rowVar);       
    });
</script>
<script>
    $(".moreVar").on('click','.btnDelVar', function(){   
        var rowVar = $(this).parent().parent().parent().parent().parent().remove();  
    });
</script>
<script>
    jQuery(document).ready(function(){
           var i = 1;
           jQuery('.gallery').on('change','#primary_image', function(e){           
              
               var fileData = $('#primary_image').prop('files')[0];   
               var form_data = new FormData();                  
               form_data.append('fileInfo', fileData);
   
               
               $.ajaxSetup({
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       }
               });
               $.ajax({
                       url:"{{route('fileUploading')}}",
                       type:"post",
                       contentType: false,
                       processData: false,    
                       data:form_data,
                       success: function (response) {
                           var returnedData = JSON.parse(response);
                           if(returnedData.status == "success"){
                               $(".primary_image_list").html("<li><a href='#' itemprop='contentUrl' data-size='752x502'><img src='/assets/product_images/"+returnedData.filename+"' itemprop='thumbnail' alt='Wedding Image 1' class='img-portrait'></a></li>");
                               $("#primary_image_value").val(returnedData.filename);
                           }        
                           else{
                               $("#uploadresponse").html(returnedData.message);
                           }               
                           
                       },
                       error: function (error) {
                           $("#uploadresponse").html(returnedData.message);
                       }
               }); 
   
           });
   
    });
   </script>
   <script>
       jQuery(document).ready(function(){
              var i = 1;
              jQuery('.gallery').on('change','#gallary_images', function(e){           
                 
                  var fileData = $('#gallary_images').prop('files')[0];   
                  var form_data = new FormData();                  
                  form_data.append('fileInfo', fileData);
   
                  
                  $.ajaxSetup({
                          headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          }
                  });
                  $.ajax({
                          url:"{{route('fileUploading')}}",
                          type:"post",
                          contentType: false,
                          processData: false,    
                          data:form_data,
                          success: function (response) {
                              var returnedData = JSON.parse(response);
                              if(returnedData.status == "success"){
                                  $(".gallary_images_list").append("<li><a href='#' itemprop='contentUrl' data-size='752x502'><img src='/assets/product_images/"+returnedData.filename+"' itemprop='thumbnail' alt='Wedding Image 1' class='img-portrait'></a></li>");
                                  var gallaryImagesOld = $("#gallary_images_value").val();
                                  if(gallaryImagesOld){
                                       var gallaryImagesNew = gallaryImagesOld+'|'+returnedData.filename;
                                  }
                                  else{
                                       var gallaryImagesNew = returnedData.filename;
                                  }
                                 
                                  $("#gallary_images_value").val(gallaryImagesNew);
                              }        
                              else{
                                  $("#uploadresponse").html(returnedData.message);
                              }               
                              
                          },
                          error: function (error) {
                              $("#uploadresponse").html(returnedData.message);
                          }
                  }); 
      
              });
      
       });
      </script>
@endsection