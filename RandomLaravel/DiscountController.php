<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Utils\AppConst;
use App\Models\Api;
use App\Models\Discount;




class DiscountController extends Controller
{
    
    private $api;
    public function __construct(Api $api)
    {
        $this->middleware('auth');
        $this->api = $api;
    }


    public function discountPage()
    {           
        return view('backend.discount.discountpage');     
    }

    public function discountAjax()
    {   
        $discount = Discount::get();
        return view('backend.discount.discountajax',compact('discount'));     
    }

    public function discountAdd(Request $request)
    {        
        //dd($request->all());          
         //Validation
        $validator = Validator::make($request->all(), [
            'discount_type' => ['nullable', 'string', 'max:50'],               
            'coupon_type' => ['nullable', 'string', 'max:50'],   
            'coupon_code' => ['nullable', 'string', 'max:50'],   
            'coupon_value' => ['nullable'],          
            'coupon_requirement_amount' => ['nullable'],             
            'coupon_usage_limit' => ['nullable'],             
            'coupon_status_status' => ['nullable', 'string', 'max:50'],             

        ]);
         
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        if($request->discount_type == "automatic_discount"){
            $recordExist = Discount::where('discount_type','automatic_discount')->first();
            if($recordExist){
                return redirect()->back()->with('failed',AppConst::duplicate)->withInput();
            }
        }        

        $recordExist = Discount::where('coupon_code',$request->coupon_code)->first();
        if($recordExist){
            return redirect()->back()->with('failed',AppConst::duplicate)->withInput();
        }
        //Validation
               
        Discount::create([
            'discount_type' => $request->discount_type,               
            'coupon_type' => $request->coupon_type,   
            'coupon_code' => $request->coupon_code,   
            'coupon_value' => $request->coupon_value,          
            'coupon_amount' => $request->coupon_value,            
            'coupon_requirement_amount' => $request->coupon_requirement_amount,            
            'coupon_usage_limit' => $request->coupon_usage_limit,            
            'coupon_usage_remaining' => $request->coupon_usage_limit,      
            'coupon_status' => $request->coupon_status,          

        ]);

        return redirect()->route('discountPage')->with('success',AppConst::success);
    
    }

    public function discountUpdate(Request $request)
    {        
                  
                
         //Validation
         $validator = Validator::make($request->all(), [
            'discount_type' => ['nullable', 'string', 'max:50'],               
            'coupon_type' => ['nullable', 'string', 'max:50'],   
            'coupon_code' => ['nullable', 'string', 'max:50'],   
            'coupon_value' => ['nullable'],          
            'coupon_requirement_amount' => ['nullable'],             
            'coupon_usage_limit' => ['nullable'],             
            'coupon_status_status' => ['nullable', 'string', 'max:50'],             

        ]);
         
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $recordExist = Discount::where('id','!=',$request->id)->where('coupon_code',$request->coupon_code)->first();
        if($recordExist){
            return redirect()->back()->with('failed',AppConst::duplicate)->withInput();
        }
        //Validation
               
        Discount::where('id',$request->id)->update([            
            'coupon_type' => $request->coupon_type,   
            'coupon_code' => $request->coupon_code,   
            'coupon_value' => $request->coupon_value,          
            'coupon_amount' => $request->coupon_value,            
            'coupon_requirement_amount' => $request->coupon_requirement_amount,            
            'coupon_usage_limit' => $request->coupon_usage_limit,        
            'coupon_status' => $request->coupon_status,                  
        ]);

        return redirect()->route('discountPage')->with('success',AppConst::success);
    
    }

    public function discountDelete(Request $request)
    {      
                  
        Discount::where('id',$request->id)->delete();

        return redirect()->route('discountPage')->with('success',AppConst::success);
    
    }



}