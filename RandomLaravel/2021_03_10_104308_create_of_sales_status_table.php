<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfSalesStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('of_sales_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number')->nullable();
            $table->string('current_status')->nullable();
            $table->string('awaiting_for_payment')->nullable();
            $table->string('academic_review')->nullable();            
            $table->string('in_progress')->nullable();
            $table->string('in_review')->nullable();
            $table->string('accepted')->nullable();
            $table->string('completed')->nullable();
            $table->string('cf1')->nullable();
            $table->string('cf2')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('of_sales_status');
    }
}
