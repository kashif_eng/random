<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use App\Http\Utils\AppConst;
use App\Models\Api;
use App\Models\Profile;
use App\Models\User;
use auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\Delivery;
use Storage;


// use App\Models\Box;
// use App\Models\Meta;


/*Crypt::encryptString($saleResult->order_number);
Crypt::decryptString($order_number_enc);
$cryptorder = \Crypt::encryptString($orderDetail->order_number);
*/

class OrderController extends Controller
{
    
    
    private $api;
    public function __construct(Api $api)
    {
        $this->middleware('auth');
        $this->api = $api;
        // $userid = Auth::user()->id;dd($userid);
        // $profile = Profile::where('user_id',$userid)->first();
        // if($profile->cf1 == "Partial"){
        //     return redirect()->route("profile")->with('failed','Oops! Your Profie Is Currently Under Review.');
        // }
    }
 
    public function orders()
    {   
        $userid = Auth::user()->id;
        $profile = Profile::where('user_id',$userid)->first();
        if($profile->cf5 == "Partial"){
            return redirect()->route("profile")->with('failed','Oops! Your Profie Is Currently Under Review.');
        }
        return view('backend.order.orders');  
       
    }

    public function orderDetails($id)
    {   
        $api = Api::where('id',1)->first();
        $api_url = $api->orderapp;
        $action = "writer_order_detail";
        $order_number = Crypt::decryptString($id);

        $token = $this->getApiResponseToken($api_url);        
        $orderDetail = $this->getApiResponse($api_url,$token,$action,'','',$order_number,auth()->user()->id,'');
        
        if(!$orderDetail){
            return redirect()->back()->with('failed',AppConst::wrong);
        }

        
        $profile = Profile::where('user_id',auth()->user()->id)->first();
        $orderDetail->profile = $profile;
        
        $action = "customers";
        $customerDetail = $this->getApiResponse($api_url,$token,$action,'','','',$orderDetail->customer_email,'');
        
        $action = "order_items_detail";
        $orderItemsDetail = $this->getApiResponse($api_url,$token,$action,'','',$order_number,'','');

        $action = "payments_detail";
        $paymentDetail = $this->getApiResponse($api_url,$token,$action,'','',$order_number,'','');

        $action = "order_attachment";
        $attachmentDetails = $this->getApiResponse($api_url,$token,$action,'','',$order_number,'','');
        if(!$attachmentDetails) { $attachmentDetails = array();}
        $action = "order_status";
        $orderStatus = $this->getApiResponse($api_url,$token,$action,'','',$order_number,'','');

        $token2 = $this->getApiResponseToken($api->adminapp);     
        $action = "messages_box_writer";          
        
        $box = $this->getApiResponse($api->adminapp,$token2,$action,'','',$order_number,'','');
        
        $action = "messages_meta";
        $meta = $this->getApiResponse($api->adminapp,$token2,$action,'','',$order_number,'','');

        $action = "admin_details";
        $admin = $this->getApiResponse($api->adminapp,$token2,$action,'','','','','');

        if($orderDetail->cf1){
            $parameters = array(
                'writer_id' => $orderDetail->cf1,
                'subject' => $orderDetail->subject,

            );
            $parameters = (object) $parameters;
            $api = Api::where('id',1)->first();
            $api_url = $api->writerapp; 
            $token3 = $this->getApiResponseToken($api_url);  
            $action = "writer_mask";
            $mask = $this->getApiResponse($api_url,$token3,$action,'','','','',$parameters);
        }
        else{
            $mask = array();
        }

        return view('backend.order.orderdetails',compact('orderDetail','customerDetail','orderItemsDetail','paymentDetail','attachmentDetails','orderStatus','box','meta','admin','mask'));
    }


    public function ordersAjax(Request $request)
    {
        $profile = Profile::where('user_id',auth()->user()->id)->first();
        $parameters = json_decode($profile->subjects);
        // $parameters = array([
        //     'subjects' => $profile->subjects
        // ]);
        // dd($subjects);

        if($request->fromdate){
            $api = Api::where('id',1)->first();
            $api_url = $api->orderapp;
            $action = "writer-orders";

            $token = $this->getApiResponseToken($api_url);        
            $orders = $this->getApiResponse($api_url,$token,$action,$request->fromdate,$request->todate,'','',$parameters);
        }
        else{
            $api = Api::where('id',1)->first();
            $api_url = $api->orderapp;
            $action = "writer-orders";

            $token = $this->getApiResponseToken($api_url);        
            $orders = $this->getApiResponse($api_url,$token,$action,'','','',auth()->user()->id,$parameters);
        }
        
       
        foreach($orders as $k=>$item){
            $orders[$k]->cryptOrder = Crypt::encryptString($item->order_number);
            $orders[$k]->profile = $profile;

        }
        
        
        return view('backend.order.ordersajax',compact('orders'));  
       
    }

  

   public function sendMessage(Request $request){

        $api = Api::where('id',1)->first();
        //Validation
        $validator = Validator::make($request->all(), [
            'order_number' => ['required', 'string', 'max:100'],    
            'to' => ['required', 'string', 'max:100'],              
            'from' => ['required', 'string', 'max:100'],   
            'from_name' => ['required', 'string', 'max:100'],   
            'from_type' => ['required', 'string', 'max:100'],   
            'message' => ['required'],   

        ]);
            
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        //Validation

        

        
        if(!empty($_FILES['os-upload']['name'][0])){$attachment =  count($_FILES['os-upload']['name']);}else{$attachment = 0;}
        $approval = Profile::where('user_id',auth()->user()->id)->pluck('message_approval')->first();
        
        $exp = explode("#",$request->to);
        $customer_user_id_for_notify = Null;
        if($exp[0] == "support"){
            $message_array = array(          
                'order_number' => $request->order_number,
                'to' => $exp[1],
                'to_type' => "support",
                'from' => $request->from,
                'from_type' => $request->from_type,
                'attachment' => $attachment,
                'message' => $request->message,
                'reply' => Null,
                'parent' => Null,
                'approval' => 'Yes',
                'status' => 'Unread',
                'cf1' => $request->from_name,
                'cf2' => 'support',
                'cf3' => $request->service_type,      
                'cf4' => $request->order_type,                    
                'cf5' => Null,                    
    
            );
        }
        else{
            $message_array = array(          
                'order_number' => $request->order_number,
                'to' => $exp[1],
                'to_type' => "customer",
                'from' => $request->from,
                'from_type' => $request->from_type,
                'attachment' => $attachment,
                'message' => $request->message,
                'reply' => Null,
                'parent' => Null,
                'approval' => $approval,
                'status' => 'Unread',
                'cf1' => $request->from_name,
                'cf2' => $exp[2],
                'cf3' => $request->service_type,      
                'cf4' => $request->order_type,                    
                'cf5' => Null,                       
    
            );
            $customer_user_id_for_notify = $request->customer_id_msg_delivery;
        }

        

       ///Message array is being embeded into order number variable
       $order_number = json_encode($message_array);
        
       $token2 = $this->getApiResponseToken($api->adminapp);     
       $action = "messages_send";  
       $msg = $this->getApiResponse($api->adminapp,$token2,$action,'','',$order_number,'','');

       //Notification
       $notify_array = array(          
        'type' => 'message_notification',
        'subject' => 'New message',
        'provision' => 'super_admin',
        'event_type' => 'create',
        'event' => 'message_recieved',
        'notification' => 'Dear user,you have recieved a new message for Order#'.$order_number,
        'status' => 0,
        'action' => 0,
        'user_type' => 'writer',
        'customer_user_id' => $customer_user_id_for_notify,     
        'writer_user_id' => Null,            
        'admin_user_id' => 1,            
        'cf1' => Null,
        'cf2' => Null,
        'cf3' => Null, 
        );
        $action = "send_notification";  
        $notify_response = $this->getApiResponse($api->adminapp,$token2,$action,'','','','',$notify_array);
        
        // try{
        //     $r = Mail::to($writeruseremail_notification)->send(new Delivery($request->order_number));                         
        // }
        // catch(\Exception $e){                    
        // }

       $therequest = $request;
       $allfiles = $_FILES['os-upload'];
       $ordernumber = $request->order_number;
       $pref = "msg";
       $key = "os-upload";  
       $message_id = $msg->id;   
            

       if($message_id){
            if(count($_FILES['os-upload']['name']) > 0){
                $this->fileuploading($therequest,$allfiles,$ordernumber,$pref,$key,$message_id); 
            }

            $id = Crypt::encryptString($request->order_number);
            return redirect()->route('orderDetails',$id)->with('success',AppConst::success);
        }

        
        $id = Crypt::encryptString($request->order_number);
        return redirect()->route('orderDetails',$id)->with('failed',AppConst::success);

    }

    public function sendDelivery(Request $request){

        $api = Api::where('id',1)->first();
        //Validation
        $validator = Validator::make($request->all(), [
            'order_number' => ['required', 'string', 'max:100'],    
            'to' => ['required', 'string', 'max:100'],              
            'from' => ['required', 'string', 'max:100'],   
            'from_name' => ['required', 'string', 'max:100'],   
            'from_type' => ['required', 'string', 'max:100'],   
            'message' => ['required'],   

        ]);
            
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        //Validation

        

        
        if(!empty($_FILES['os-upload']['name'][0])){$attachment =  count($_FILES['os-upload']['name']);}else{$attachment = 0;}
        $approval = Profile::where('user_id',auth()->user()->id)->pluck('delivery_approval')->first();
        
        if($request->is_partial == "partially_paid"){
            $approval = "No";
        }


        $exp = explode("#",$request->to);
        if($exp[0] == "support"){
            $message_array = array(          
                'order_number' => $request->order_number,
                'to' => $exp[1],
                'to_type' => "support",
                'from' => $request->from,
                'from_type' => $request->from_type,
                'attachment' => $attachment,
                'message' => $request->message,
                'reply' => Null,
                'parent' => Null,
                'approval' => 'Yes',
                'status' => 'Unread',
                'cf1' => $request->from_name,
                'cf2' => 'support',
                'cf3' => $request->service_type,      
                'cf4' => $request->order_type,                    
                'cf5' => Null,                    
    
            );
        }
        else{
            $message_array = array(          
                'order_number' => $request->order_number,
                'to' => $exp[1],
                'to_type' => "customer",
                'from' => $request->from,
                'from_type' => $request->from_type,
                'attachment' => $attachment,
                'message' => $request->message,
                'reply' => Null,
                'parent' => Null,
                'approval' => $approval,
                'status' => 'Unread',
                'cf1' => $request->from_name,
                'cf2' => $exp[2],
                'cf3' => $request->service_type,      
                'cf4' => $request->order_type,                    
                'cf5' => Null,                       
    
            );
            if($approval == "Yes"){
                $tokenstatus = $this->getApiResponseToken($api->orderapp);     
                $action = "delivery_send_status";  
                $delivery_send_status = $this->getApiResponse($api->orderapp,$tokenstatus,$action,'','',$request->order_number,'','');
            }
        }

       ///Message array is being embeded into order number variable
       $order_number = json_encode($message_array);
        
       $token2 = $this->getApiResponseToken($api->adminapp);     
       $action = "delivery_send";  
       $msg = $this->getApiResponse($api->adminapp,$token2,$action,'','',$order_number,'','');

        //Notification
        $notify_array = array(          
            'type' => 'delivery_notification',
            'subject' => 'Writer delivers the customer order',
            'provision' => 'super_admin',
            'event_type' => 'create',
            'event' => 'writer_delivers_order',
            'notification' => 'Dear user,your Order#'.$request->order_number.' has been delivered',
            'status' => 0,
            'action' => 0,
            'user_type' => 'writer',
            'customer_user_id' => $request->customerid_notification,     
            'writer_user_id' => Null,            
            'admin_user_id' => 1,            
            'cf1' => Null,
            'cf2' => Null,
            'cf3' => Null, 
        );
        $action = "send_notification";  
        $notify_response = $this->getApiResponse($api->adminapp,$token2,$action,'','','','',$notify_array);

        try{
            $r = Mail::to($writeruseremail_notification)->send(new Delivery($request->order_number));                         
        }
        catch(\Exception $e){                    
        }
        /////

       $therequest = $request;
       $allfiles = $_FILES['os-upload'];
       $ordernumber = $request->order_number;
       $pref = "msg";
       $key = "os-upload";  
       $message_id = $msg->id;   
            

       if($message_id){
            if(count($_FILES['os-upload']['name']) > 0){
                $this->fileuploading($therequest,$allfiles,$ordernumber,$pref,$key,$message_id); 
            }

            $id = Crypt::encryptString($request->order_number);
            return redirect()->route('orderDetails',$id)->with('success',AppConst::success);
        }

        
        $id = Crypt::encryptString($request->order_number);
        return redirect()->route('orderDetails',$id)->with('failed',AppConst::success);

    }

    public function fileuploading($therequest,$allfiles,$ordernumber,$pref,$key,$message_id)
    {   
        $api = Api::where('id',1)->first();
        $myFile = $allfiles;
        $fileCount = count($myFile["name"]);   

        $token2 = $this->getApiResponseToken($api->adminapp);     
        $action = "messages_count";  
        $count = $this->getApiResponse($api->adminapp,$token2,$action,'','',$ordernumber,'','');
        //$count = Meta::where('order_number',$ordernumber)->count();
        //$count = "75";
        
        for ($i = 0; $i < $fileCount; $i++) {
                    
            $fileTmpPath = $myFile["tmp_name"][$i];
            $fileName = $myFile["name"][$i];
            $fileSize = $myFile["size"][$i];
            $fileType = $myFile["type"][$i];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));

            $newFileName = $ordernumber . '-' . $pref . $count . $i . '.' . $fileExtension;
            
            // directory in which the uploaded file will be moved
            $uploadFileDir = './assets/msg_uploads/';
            $dest_path = $uploadFileDir . $newFileName;

            //dd($dest_path);

            $allowedfileExtensions = array('jpg', 'gif', 'png', 'zip', 'txt', 'xls', 'doc', 'pdf', 'docx', 'xlsx','csv','jpeg','PNG','ppt','pptx');
            if (in_array($fileExtension, $allowedfileExtensions)) {
                
                if($fileSize < 10000000) {

                    $storage_respone = Storage::put($newFileName,file_get_contents($fileTmpPath));
                    
                    if($storage_respone)
                    {
                        $message ='File is successfully uploaded.';
                        $exp = explode("#",$therequest->to);
                        if($exp[0] == "support"){
                            $meta_array = array(
                                'message_id' => $message_id,
                                'order_number' => $ordernumber,
                                'to' => $exp[1],
                                'to_type' => 'support',
                                'from' => $therequest->from,
                                'from_type' => $therequest->from_type,
                                'meta_description' => 'Attachment',
                                'meta_value' => $newFileName,
                                'file_size' => $fileSize,
                                'old_filename' => $fileName,
                                'cf1' => $therequest->service_type,
                                'cf2' => $therequest->order_type,
                                'cf3' => Null,
    
                            );
                        }
                        else{
                            $meta_array = array(
                                'message_id' => $message_id,
                                'order_number' => $ordernumber,
                                'to' => $exp[1],
                                'to_type' => 'customer',
                                'from' => $therequest->from,
                                'from_type' => $therequest->from_type,
                                'meta_description' => 'Attachment',
                                'meta_value' => $newFileName,
                                'file_size' => $fileSize,
                                'old_filename' => $fileName,
                                'cf1' => $therequest->service_type,
                                'cf2' => $therequest->order_type,
                                'cf3' => Null,
    
                            );
                        }
                        
                        $ordernumber = json_encode($meta_array);

                        $action = "messages_attachment";  
                        $count = $this->getApiResponse($api->adminapp,$token2,$action,'','',$ordernumber,'','');
                        
                        //return true;
                    }
                    else
                    {
                        $message = 'There was some error moving the file to upload directory. Please make sure the upload directory is writable by web server.';
                    }
                }


            }
                                
        }        
        
    }

    
    public function biddingUpdate(Request $request)
    {   
        $api = Api::where('id',1)->first();
        $api_url = $api->orderapp;
        $action = "bidding_update";
        $order_number = $request->order_number;

        $thisid = Auth::user()->id;
        $profile = Profile::where('user_id',$thisid)->first();
        $request->request->add(['commission'=>$profile->commission]);

        

        $token = $this->getApiResponseToken($api_url);        
        $biddingDetail = $this->getApiResponse($api_url,$token,$action,'','',$order_number,'',$request->all());
        
        //Notification
        $notify_array = array(          
            'type' => 'bidding_notification',
            'subject' => 'Writer bids on order',
            'provision' => 'super_admin',
            'event_type' => 'create',
            'event' => 'writer_bidds_order',
            'notification' => 'Writer'.$thisid.' bids on Order#'.$request->order_number,
            'status' => 0,
            'action' => 0,
            'user_type' => 'writer',
            'customer_user_id' => Null,     
            'writer_user_id' => Null,            
            'admin_user_id' => 1,            
            'cf1' => Null,
            'cf2' => Null,
            'cf3' => Null, 
        );
        $action = "send_notification";  
        $notify_response = $this->getApiResponse($api->adminapp,$token2,$action,'','','','',$notify_array);


        $id = Crypt::encryptString($request->order_number);
        return redirect()->route('orderDetails',$id)->with('success',AppConst::success);
    }


    public function getApiResponseToken($api_url){

        $api = Api::where('id',1)->first();
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url."login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('email' => $api->username,'password' => $api->password),         
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response);        
        if($response){
            if($response->status == true){
               $token = $response->token;
               return $token;
            }
        }

        return false;
    }


    public function getApiResponse($api_url,$token,$action,$fromdate=0,$todate=0,$order_number=0,$email=0,$parameters=0){

        if($parameters){
            $parameters = json_encode($parameters);
        } 
       
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url.$action,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('fromdate' => $fromdate,'todate' => $todate,'order_number' => $order_number,'email' => $email,'parameters' => $parameters),
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $token",
        ),
        ));

        $response = curl_exec($curl); 
        
       
        curl_close($curl);       
        
        $response = json_decode($response); 
        return $response;
    

    }

}
