<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\OfCustomer;
use App\Models\OfSale;
use App\Models\OfSalesItem;
use App\Models\Payment;
use App\Models\OfSaleWriter;
use App\Models\OfSaleStatus;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\ApiUserController@login')->name('login');
Route::post('newregister', 'API\ApiUserController@newRegister')->name('newRegister');
Route::post('newlogin', 'API\ApiUserController@newLogin')->name('newLogin');

/*
|--------------------------------------------------------------------------
| CUSTOMERS
|--------------------------------------------------------------------------|
*/
Route::middleware('auth:api')->post('customers_count', function (Request $request) {    
    return OfCustomer::count();
});

Route::middleware('auth:api')->post('customers', function (Request $request) {
    if($request->customer_id){
        return OfCustomer::where('id',$request->customer_id)->first();
    }
    else if($request->email){
        return OfCustomer::where('email',$request->email)->first();
    }
    else if($request->fromdate){         
        return OfCustomer::whereBetween('created_at', [$request->fromdate, $request->todate])->get();
    }
    else{
        return OfCustomer::orderby('id','DESC')->limit(500)->get();
    }
});

Route::middleware('auth:api')->post('customer_toggle', function (Request $request) {  
    $customer = OfCustomer::where('id',$request->customer_id)->first();
    if($customer->status == "Active"){
        OfCustomer::where('id',$request->customer_id)->update([
            'status'=>'Deactive'
        ]);
    }
    else{
        OfCustomer::where('id',$request->customer_id)->update([
            'status'=>'Active'
        ]);
    }
    return true;
});

Route::middleware('auth:api')->post('update_profile', function (Request $request) {    
    
    $updateProfile = OfCustomer::where('email',$request->email)->update([
        'name' => $request->fullname,
        'phone' => $request->phone,
        'cf2' => $request->cf2,
        'cf3' => $request->cf3,
        'cf4' => $request->cf4,
    ]);
    
    return $updateProfile;
});

Route::middleware('auth:api')->post('customr_update', function (Request $request) {    
    
    $data = json_decode($request->parameters);
    $updateProfile = OfCustomer::where('email',$request->email)->update([
        'name' => $data->fullname,
        'phone' => $data->phone,
        'cf2' => $data->cf2,
        'cf3' => $data->cf3,
        'cf4' => $data->cf4,
    ]);
    
    return $updateProfile;
});


/*
|--------------------------------------------------------------------------
| ORDERS
|--------------------------------------------------------------------------|
*/
Route::middleware('auth:api')->post('orders_count', function (Request $request) {    
    return OfSale::count();
});

Route::middleware('auth:api')->post('orders', function (Request $request) {   
    if($request->fromdate){         
        return OfSale::whereBetween('sale_date', [$request->fromdate, $request->todate])->get();
    }
    else{
        return OfSale::orderby('id','DESC')->limit(500)->get();
    }
    
});

Route::middleware('auth:api')->post('writer-orders', function (Request $request) {   
    $parameters = json_decode($request->parameters);
    
    if($request->fromdate){         
        $sales =  OfSale::whereIn('subject',$parameters)->whereBetween('sale_date', [$request->fromdate, $request->todate])->get();
    }
    else{
        $sales = OfSale::whereIn('subject',$parameters)->orderby('id','DESC')->limit(500)->get();
    }
    
    foreach($sales as $k=>$item){
            $sales[$k]->saleWriter = OfSaleWriter::where('order_number', $item->order_number)->where('writer_id', $request->email)->first();
    }
    
    return $sales;
    
});

Route::middleware('auth:api')->post('customer_orders', function (Request $request) {
    if($request->fromdate){         
        $ofsale =  OfSale::where('customer_email',$request->email)->whereBetween('sale_date', [$request->fromdate, $request->todate])->get();
    }
    else{
        $ofsale =  OfSale::where('customer_email',$request->email)->orderby('id','DESC')->limit(500)->get();
    }
    
    /*foreach($ofsale as $k=>$sale){
        $ofsale[$k]->assignedWriter = ;
    }*/
    
    return $ofsale;
    
});

Route::middleware('auth:api')->post('order_detail', function (Request $request) {    
    return OfSale::where('order_number', $request->order_number)->first();
});

Route::middleware('auth:api')->post('order_status', function (Request $request) {    
    return OfSaleStatus::where('order_number', $request->order_number)->first();
});


Route::middleware('auth:api')->post('writer_order_detail', function (Request $request) {    
    $sales = OfSale::where('order_number', $request->order_number)->first();
    $sales->saleWriter = OfSaleWriter::where('order_number', $request->order_number)->where('writer_id', $request->email)->first();
    /*foreach($sales as $k=>$item){
            $sales[$k]->saleWriter = OfSaleWriter::where('order_number', $item->order_number)->where('writer_id', $request->email)->first();
    }*/
    return $sales;
});

Route::middleware('auth:api')->post('order_items_detail', function (Request $request) {    
    return OfSalesItem::where('order_number', $request->order_number)->get();
});

Route::middleware('auth:api')->post('bidding_update', function (Request $request) {    
     $parameters = json_decode($request->parameters);
    $bidDetails = OfSaleWriter::create([
            'order_number' => $request->order_number,
            'writer_id' => $parameters->writer_id,
            'status' => $parameters->action,
            'cf1' => $parameters->writer_name,
    ]);
    
    return $bidDetails;
});

Route::middleware('auth:api')->post('update_order_parameter', function (Request $request) {    
    $parameters = json_decode($request->parameters);
    $update_order_parameter = OfSale::where('order_number',$parameters->order_number)->update([
            'total' => $parameters->order_amount,
            'deadline' => $parameters->order_deadline,
    ]);
    
    return $update_order_parameter;
});

Route::middleware('auth:api')->post('bidding_details', function (Request $request) {    
    return OfSaleWriter::where('order_number', $request->order_number)->get();
});

Route::middleware('auth:api')->post('assign_writer', function (Request $request) {    
    $parameters = json_decode($request->parameters);
    $exp = explode('_',$parameters->action);
    $bidCreate = OfSaleWriter::create([
            'order_number' => $parameters->order_number,
            'writer_id' => $exp[0],
            'status' => 'assigned',
            'cf1' => $exp[1],
    ]);
    $bidUpdate = OfSale::where('order_number',$parameters->order_number)->update([
            'cf1' => $exp[0],
            'cf2' => $exp[1],
    ]);
    
    $currentSaleStatus = OfSaleStatus::where('order_number', $parameters->order_number)->first();
    $paymentStatus = OfSale::where('order_number', $parameters->order_number)->first();
    if($paymentStatus->payment_status == "pending"){
        $pstatus = null;
    }
    else{
        $pstatus = $paymentStatus->payment_status;
    }
    if(!$currentSaleStatus){
        $status = OfSaleStatus::create([
                            'order_number' => $parameters->order_number,
                            'current_status' => 'academic_review',
                            'awaiting_for_payment' => $pstatus,
                            'academic_review' => 'Yes',
                            // 'awaiting_more_info' => 'pending',            
                            // 'in_progress' => '1',
                            // 'in_review' => '1',
                            // 'complete' => '1',
                            
                        ]);
    }
    else{
        $status = OfSaleStatus::where('order_number',$parameters->order_number)->update([
                            'current_status' => 'academic_review',
                            'academic_review' => 'Yes',
                            // 'awaiting_more_info' => 'pending',            
                            // 'in_progress' => '1',
                            // 'in_review' => '1',
                            // 'complete' => '1',
                            
                        ]);
    }
    
    return $bidDetails;
});

/*
|--------------------------------------------------------------------------
| PAYMENTS
|--------------------------------------------------------------------------|
*/
Route::middleware('auth:api')->post('payments_sum', function (Request $request) {    
    if($request->fromdate){
        return Payment::whereBetween('created_at', [$request->fromdate, $request->todate])->sum('amount');
    }
    else{
        return Payment::sum('amount');
    }    
});

Route::middleware('auth:api')->post('payments', function (Request $request) {    
    if($request->fromdate){         
        $items = Payment::where('payment_of','!=','writer_payment')->whereBetween('created_at', [$request->fromdate, $request->todate])->get();
        return $items;
    }
    else{
        $items = Payment::where('payment_of','!=','writer_payment')->orderby('id','DESC')->limit(500)->get();
        return $items;
    }
});

Route::middleware('auth:api')->post('transactions', function (Request $request) {    
    if($request->fromdate){         
        $items = Payment::whereBetween('created_at', [$request->fromdate, $request->todate])->get();
        return $items;
    }
    else{
        $items = Payment::orderby('id','DESC')->limit(500)->get();
        return $items;
    }
});

Route::middleware('auth:api')->post('payments_detail', function (Request $request) {    
    return Payment::where('order_number', $request->order_number)->get();    
});

Route::middleware('auth:api')->post('customer_payments', function (Request $request) {    
    $sales = OfSale::where('customer_email',$request->email)->orderby('id','DESC')->get();
    foreach($sales as $k=>$item){
            $sales[$k]->paymentDetail =Payment::where('order_number', $item->order_number)->get();
    }
    return $sales;
    //return Payment::where('customer_email', $request->email)->get();    
});

Route::middleware('auth:api')->post('add_payment', function (Request $request) { 
    
    $parameters = json_decode($request->parameters);
    
    //return Payment::where('order_number', $parameters->order_number)->get();
    
        $paymentDetail = Payment::where('order_number',$parameters->order_number)->orderBy('id', 'DESC')->get();
        if(count($paymentDetail) > 0){
            $transaction = "TR".strtoupper($parameters->payment_mathod).$parameters->order_number.(count($paymentDetail)+1);
        }
        else{
            $transaction = "TR".strtoupper($parameters->payment_mathod).$parameters->order_number."1";
        }
        
        
        $payment = Payment::create([
        'sale_id'=>$parameters->sale_id,
        'order_number'=>$parameters->order_number,
        'invoice_number'=>$parameters->order_number,
        'payment_of'=>'admin_payment',
        'transaction_no'=>$transaction,
        'amount'=>$parameters->amount,
        'payment_mathod'=>$parameters->payment_mathod,
        'cheque_no'=>$parameters->reference,
        
    ]);
        $alreadypaid = OfSale::where('order_number',$parameters->order_number)->pluck('paid')->first();
        $paid = $alreadypaid+$parameters->amount;
        
        OfSale::where('order_number',$parameters->order_number)->update([
                'paid' => $paid 
        ]);
        
    
    return $payment;
    //return Payment::where('customer_email', $request->email)->get();    
});

/*
|--------------------------------------------------------------------------
| ATTACHMENTS
|--------------------------------------------------------------------------|
*/
/*Route::middleware('auth:api')->post('attachment_count', function (Request $request) {    
    return OfSale::count();
});*/

Route::middleware('auth:api')->post('order_attachment', function (Request $request) {    
    $url = env("APP_URL", "https://rporderapp.looptestingserver.com/");
    $items = OfSalesItem::where('order_number', $request->order_number)->where('meta_description','Attachments')->orderby('id','DESC')->limit(500)->get(); 
    foreach($items as $k=>$item){
        $items[$k]->url = $url;
    }
    return $items;
});

Route::middleware('auth:api')->post('attachments', function (Request $request) {   
    //$url = env("APP_URL", "https://researchprospect.com/");
    $url = env("APP_URL", "https://rporderapp.looptestingserver.com/");
        
    
    if($request->fromdate){         
        $items = OfSalesItem::where('meta_description','Attachments')->whereBetween('created_at', [$request->fromdate, $request->todate])->get();
        foreach($items as $k=>$item){
            $items[$k]->url = $url;
        }
        return $items;
    }
    else{
        $items = OfSalesItem::where('meta_description','Attachments')->orderby('id','DESC')->limit(500)->get();
        foreach($items as $k=>$item){
            $items[$k]->url = $url;
        }
        return $items;
    }
    
});

Route::middleware('auth:api')->post('customer_attachments', function (Request $request) {   
    //$url = env("APP_URL", "https://researchprospect.com/");
    $url = env("APP_URL", "https://rporderapp.looptestingserver.com/");
        
    $customer_info = OfCustomer::where('email',$request->email)->first();
    if($request->fromdate){         
        $items = OfSalesItem::where('meta_description','Attachments')->where('customer_id',$customer_info->id)->whereBetween('created_at', [$request->fromdate, $request->todate])->get();
        foreach($items as $k=>$item){
            $items[$k]->url = $url;
        }
        return $items;
    }
    else{
        $items = OfSalesItem::where('meta_description','Attachments')->where('customer_id',$customer_info->id)->orderby('id','DESC')->limit(500)->get();
        foreach($items as $k=>$item){
            $items[$k]->url = $url;
        }
        return $items;
    }
    
});
