<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use App\Http\Utils\AppConst;
use App\Models\Api;
use App\Models\Country;
use App\Models\Language;
use App\Models\Software;
use App\Models\Subject;
use App\Models\Notification;
use Illuminate\Support\Facades\Mail;
use App\Mail\WriterInvoice;

class WriterController extends Controller
{
    private $notification;
    public function __construct(Notification $notification)
    {
        $this->middleware('auth');
        $this->notification = $notification;
    }

 
    public function writers()
    {
        return view('backend.writer.writers');
    }

    public function writerDetails($id)
    {   
        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;       
        $writer_id = Crypt::decryptString($id);
        //dd($writer_id);

        $token = $this->getApiResponseToken($api_url);        
        $action = "writer_detail";
        $profile = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'','');

        $token = $this->getApiResponseToken($api_url);        
        $action = "store_credits";
        $store_credits = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'','');

        $token = $this->getApiResponseToken($api_url);        
        $action = "store_credits_all";
        $store_credits_all = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'','');
        

        $action = "writer_masks";
        $mask = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'','');

        $token = $this->getApiResponseToken($api->orderapp);        
        $action = "invoice_list";
        $invoicelist = $this->getApiResponse($api->orderapp,$token,$action,'','',$writer_id,'','');
        if(!$invoicelist){
            $invoicelist = array();
        }
        
        if(!$profile){
            return redirect()->back()->with('failed',AppConst::wrong);
        }
        
        $countries = Country::get();
        $languages = Language::get();
        $softwares = Software::get();
        $subjects = Subject::get();
        
        return view('backend.writer.writerdetails',compact('profile','mask','countries','languages','softwares','subjects','invoicelist','store_credits','store_credits_all'));
    }

    public function writersAjax(Request $request)
    {
        if($request->fromdate){
            $api = Api::where('id',1)->first();
            $api_url = $api->writerapp;
            $action = "writer_list";

            $token = $this->getApiResponseToken($api_url);        
            $writers = $this->getApiResponse($api_url,$token,$action,$request->fromdate,$request->todate);
        }
        else{
            $api = Api::where('id',1)->first();
            $api_url = $api->writerapp;
            $action = "writer_list";

            $token = $this->getApiResponseToken($api_url);        
            $writers = $this->getApiResponse($api_url,$token,$action);
        }
        
        foreach($writers as $k=>$item){
            $writers[$k]->cryptOrder = Crypt::encryptString($item->id);
        }
        
        return view('backend.writer.writersajax',compact('writers'));  
       
    }


    public function pdUpdate(Request $request)
    {   
         //Validation
         $validator = Validator::make($request->all(), [
            'id' => ['required'], 
            'user_id' => ['required'], 
            'first_name' => ['required', 'string', 'max:50'],  
            'last_name' => ['required', 'string', 'max:50'],               
            'dob' => ['required', 'string', 'max:50'],               
            'gender' => ['required', 'string', 'max:50'],               
            'country' => ['required', 'string', 'max:50'],               
            'city' => ['required', 'string', 'max:50'],               
            'email' => ['required', 'email'],               
            'phone' => ['required', 'string', 'max:50'],               
            'alternate_email' => ['nullable', 'email'],               
            'alternate_phone' => ['nullable', 'string', 'max:50'],               
        ]);
         
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }             
        //Validation

        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;       
        $writer_id = $request->id;

        $token = $this->getApiResponseToken($api_url);        
        $action = "writer_pd_update";
        $profile = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'',$request->all());
        
        //dd($customerDetail);
                
        $id = Crypt::encryptString($writer_id);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);

    }


    public function qeUpdate(Request $request)
    {   
          //Validation
          $validator = Validator::make($request->all(), [
            'id' => ['required'], 
            'user_id' => ['required'], 
            'native_language' => ['required', 'string', 'max:50'],    
            'academic_degree' => ['required', 'string', 'max:50'],               
            'major' => ['required', 'string', 'max:50'],               
                         
        ]);
         
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }             
        //Validation
        
        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;       
        $writer_id = $request->id;

        $token = $this->getApiResponseToken($api_url);        
        $action = "writer_qe_update";
        $profile = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'',$request->all());
        
        //dd($customerDetail);
                
        $id = Crypt::encryptString($writer_id);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);

    }

    public function aiUpdate(Request $request)
    {   
          //Validation
          $validator = Validator::make($request->all(), [
            'id' => ['required'], 
            'user_id' => ['required'], 
            'available' => ['nullable', 'string', 'max:50'],  
            'urgent_orders' => ['nullable', 'string', 'max:50'], 
            'message_approval' => ['nullable', 'string', 'max:50'],  
            'delivery_approval' => ['nullable', 'string', 'max:50'], 
            'about_me' => ['required', 'string'],  
            'motivation_letter' => ['required', 'string'],  
            'website' => ['required', 'string', 'max:50'],  
            'social_media' => ['required', 'string', 'max:50'],          
        ]);
         
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }             
        //Validation
        
        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;       
        $writer_id = $request->id;

        $token = $this->getApiResponseToken($api_url);        
        $action = "writer_ai_update";
        $profile = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'',$request->all());
        
        //dd($customerDetail);
                
        $id = Crypt::encryptString($writer_id);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);

    }

    public function maskCreate(Request $request)
    {   
         //Validation
         $validator = Validator::make($request->all(), [
            'profile_id' => ['required'], 
            'user_id' => ['required'], 
            'type' => ['required', 'string', 'max:50'],  
            'subject' => ['required', 'string', 'max:150'],               
            'pseudo' => ['required', 'string', 'max:50'],               
            'mask' => ['required', 'string', 'max:50'],   
                      
        ]);
         
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }             
        //Validation
       
        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;       
        $writer_id = $request->profile_id;

        $token = $this->getApiResponseToken($api_url);        
        $action = "writer_masks_create";
        $profile = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'',$request->all());
        
        //dd($customerDetail);
         
        $id = Crypt::encryptString($writer_id);

        if($profile == false){
            return redirect()->route('writerDetails',$id)->with('failed',AppConst::wrong);
        }
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);

    }

    public function maskDelete(Request $request)
    {   
         //Validation
         $validator = Validator::make($request->all(), [
            'profile_id' => ['required'], 
            'user_id' => ['required'], 
            'mask_id' => ['required'],  
                                 
        ]);
         
        if ($validator->fails()) { 
            $request->session()->flash('failed', $validator->messages()->first());
            return redirect()->back()->withInput();
        }             
        //Validation
       
        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;       
        $writer_id = $request->profile_id;

        $token = $this->getApiResponseToken($api_url);        
        $action = "writer_masks_delete";
        $profile = $this->getApiResponse($api_url,$token,$action,'','',$writer_id,'',$request->all());
        
        //dd($customerDetail);
         
        $id = Crypt::encryptString($writer_id);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);

    }

    public function invoiceOrdersAjax(Request $request)
    {
        
        $api = Api::where('id',1)->first();
        $api_url = $api->orderapp;
        $action = "invoice_orders_list";

        $token = $this->getApiResponseToken($api_url);        
        $invoiceorders = $this->getApiResponse($api_url,$token,$action,'','','','',$request->all());
       
        //dd($request);
        // foreach($invoiceorders as $k=>$item){
        //     $orders[$k]->cryptOrder = Crypt::encryptString($item->order_number);
        // }
        
        
        return view('backend.order.invoiceordersajax',compact('invoiceorders'));  
       
    }

    public function generateWriterInvoice(Request $request)
    {
        if(!$request->invoiceorders){
            $id = Crypt::encryptString($request->id);
            return redirect()->route('writerDetails',$id)->with('failed','Oops!!Something went wrong.');
        }
        //dd($request->all());
        $api = Api::where('id',1)->first();
        $api_url = $api->orderapp;
        $action = "generate_writer_invoice";

        $token = $this->getApiResponseToken($api_url);        
        $generate_writer_invoice = $this->getApiResponse($api_url,$token,$action,'','','','',$request->all());
        
        $notification_parameters = array(
            'type' => 'invoice_notification',
            'subject' => 'Admin has created the invoice',            
            'event_type' => 'create',
            'event' => 'admin_created_invoice',
            'notification' => 'Dear Writer,Your invoice from '.$request->fromdate.'-'.$request->todate.'has been generated. Please check and confirm so we could arrange for the transfer of funds. ',            
            'user_type' => 'admin',
            'customer_user_id' => Null,        
            'writer_user_id' => $request->user_id,        
            'admin_user_id' => Null,    
        );
        $this->notification->sendNotification($notification_parameters);      

        try{
            $r = Mail::to($request->useremail)->send(new WriterInvoice($request->fromdate,$request->todate));                         
        }
        catch(\Exception $e){                    
        }
        
        $id = Crypt::encryptString($request->id);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);  
       
    }

    public function delInvoice(Request $request)
    {
        
        
        //dd($request->all());
        $api = Api::where('id',1)->first();
        $api_url = $api->orderapp;
        $action = "del_invoice";

        $token = $this->getApiResponseToken($api_url);        
        $del_invoice = $this->getApiResponse($api_url,$token,$action,'','','','',$request->all());
        //dd($del_invoice);
                
        
        $id = Crypt::encryptString($request->writerid);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);  
       
    }

    public function payInvoice(Request $request)
    {
       // dd($request->all());        
       
        $api = Api::where('id',1)->first();
        $api_url = $api->orderapp;
        $action = "pay_invoice";

        $token = $this->getApiResponseToken($api_url);        
        $pay_invoice = $this->getApiResponse($api_url,$token,$action,'','','','',$request->all());
        

         $request->request->add(['first_order_total' => $pay_invoice->cf1]);
         $request->request->add(['writer_id' => $pay_invoice->writer_id]);

        //dd($request->all());   
        
        
        //check if referral
        $api_url = $api->writerapp;
        $action = "pay_referral";

        $token = $this->getApiResponseToken($api_url);        
        $del_invoice = $this->getApiResponse($api_url,$token,$action,'','','','',$request->all());
        //dd($del_invoice);   
        //
        
        $id = Crypt::encryptString($request->writerid);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);  
       
    }

    public function payInvoiceCredit(Request $request)
    {
       
        //dd($request->all());   
        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;
        $action = "pay_referral_amount";

        $token = $this->getApiResponseToken($api_url);        
        $payInvoiceCredit = $this->getApiResponse($api_url,$token,$action,'','','','',$request->all());
        

        
        
        $id = Crypt::encryptString($request->writerid);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);  
       
    }

    public function giveBonus(Request $request)
    {
       
       
        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;
        $action = "give_bonus";

        $token = $this->getApiResponseToken($api_url);        
        $payInvoiceCredit = $this->getApiResponse($api_url,$token,$action,'','','','',$request->all());
        
        $notification_parameters = array(
            'type' => 'bonus_notification',
            'subject' => 'Admin has added the bonus',            
            'event_type' => 'create',
            'event' => 'admin_added_bonus',
            'notification' => 'A bonus of GBP '.$request->amount.' has been added to your wallet.Tap on your wallet to withdraw the funds. ',            
            'user_type' => 'admin',
            'customer_user_id' => Null,        
            'writer_user_id' => $request->writerid,        
            'admin_user_id' => Null,    
        );
        $this->notification->sendNotification($notification_parameters);
        
        $id = Crypt::encryptString($request->writerid);
        return redirect()->route('writerDetails',$id)->with('success',AppConst::success);  
       
    }

    public function myOrderListAjax(Request $request)
    {
                
       
        $api = Api::where('id',1)->first();
        $api_url = $api->orderapp;
        $action = "my_orderlist_ajax";

        $token = $this->getApiResponseToken($api_url);        
        $my_orderlist_ajax = $this->getApiResponse($api_url,$token,$action,'','','','',$request->all());
        //dd($del_invoice);
                       
        
        return view('backend.writer.myorderlistajax',compact('my_orderlist_ajax')); 
       
    }

    public function writerToggle($id=0){
        $api = Api::where('id',1)->first();
        $api_url = $api->writerapp;
        $action = "writer_toggle";

        $token = $this->getApiResponseToken($api_url); 
        $writerToggle = $this->getApiResponse($api_url,$token,$action,'','',$id,'','');
        
        $notification_parameters = array(
            'type' => 'toggle_notification',
            'subject' => 'Admin has created the invoice',            
            'event_type' => 'update',
            'event' => 'profile_status_updated',
            'notification' => 'Dear Writer, Your profile status has been updated',            
            'user_type' => 'admin',
            'customer_user_id' => Null,        
            'writer_user_id' => $id,        
            'admin_user_id' => Null,    
        );
        $this->notification->sendNotification($notification_parameters);   

        $id = Crypt::encryptString($id);
        return redirect()->route('writerDetails',$id);
    }

    public function getApiResponseToken($api_url){

        $api = Api::where('id',1)->first();

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url."login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('email' => $api->username,'password' => $api->password),        
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response);        
        if($response){
            if($response->status == true){
               $token = $response->token;
               return $token;
            }
        }

        return false;
    }


    public function getApiResponse($api_url,$token,$action,$fromdate=0,$todate=0,$writer_id=0,$email=0,$parameters=0){
        
        if($parameters){
            $parameters = json_encode($parameters);
        } 
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url.$action,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('fromdate' => $fromdate,'todate' => $todate,'writer_id' => $writer_id,'email' => $email,'parameters' => $parameters),
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $token",
        ),
        ));
        
        $response = curl_exec($curl); 
        curl_close($curl);       
        
        $response = json_decode($response); 
        return $response;

    }



}
