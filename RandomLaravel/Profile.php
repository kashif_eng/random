<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\StoreCredit;
/*
|--------------------------------------------------------------------------
| Custom Field Notes
|--------------------------------------------------------------------------
| cf5 = Visibility (For New Writers)
| cf4 = Referral Code (For New Writers)
| cf3 = Payment Email

*/


class Profile extends Model
{
   
    protected $table='settings_profile';
    protected $guarded =['id','_token'];
    
    public function saveProfile($data){

        $saveUser = User::create([
            'name' => $data->first_name,              
            'email' => $data->email,              
            'password' => Hash::make($data->password), 
        ]);

        $data->subjects = json_encode($data->subjects);
        $data->other_language = json_encode($data->other_language);
        $data->software = json_encode($data->software);

        
        $saveProfile = $this::create([
            'user_id' => $saveUser->id,  
            'first_name' => $data->first_name,  
            'last_name' => $data->last_name,          
            'dob' => $data->dob,          
            'gender' => $data->gender,          
            'country' => $data->country,          
            'city' => $data->city,          
            'email' => $data->email,              
            'phone' => $data->phone,          
            'alternate_email' => $data->alternate_email,          
            'alternate_phone' => $data->altername_phone,
            'password' => Hash::make($data->password),      
            'native_language' =>$data->native_language,             
            'other_language' =>$data->other_language,             
            'academic_degree' =>$data->academic_degree,             
            'major' =>$data->major,             
            'subjects' =>$data->subjects,             
           
            'software' => $data->software,  
            'available' => $data->available, 
            'urgent_orders' => $data->urgent_orders, 
            'message_approval' => "No", 
            'delivery_approval' => "No", 
            'about_me' => $data->about_me,

            'motivation_letter' => $data->motivation_letter,
            'website' => $data->website,
            'social_media' => $data->social_media,
            'i_agree_telegram' => $data->i_agree_telegram,
            'i_agree_policy' => $data->i_agree_policy,

            'rating' => null,
            'stars' => null,
            'commission' => 5,
            'commission_type' =>'Percentage',
            'total_earnings' =>0,
            'total_dues' =>0,
            'total_paid' =>0,
            'avatar' =>'writer.jpg',
            'status' =>'Dective',
            'cf1' =>null,
            'cf2' =>null,
            'cf3' =>null,
            'cf4' =>null,
            'cf5' =>"Partial",


        ]);


        if($data->referral_code){
            $today = date('Y-m-d');
            $seller = $this::where('cf4',$data->referral_code)->first();
            if($seller){
                $check_duplicate = StoreCredit::where('customer_user_id',$saveUser->id)->first();

                if(!$check_duplicate){
                    $storeCredit = StoreCredit::create([
                        'type' => 'referral_code',  
                        'seller_user_id' => $seller->user_id,  
                        'seller_profile_id' => $seller->id,          
                        'customer_user_id' => $saveUser->id,          
                        'customer_profile_id' => $saveProfile->id,          
                        'store_credit_user_id' => Null,   
                        'store_credit_profile_id' => Null,     
                        'referral_code' => $data->referral_code,     
                        'consumption_date' => $today,     
                        'consumption_status' => 'consumed',     
                        'payment' => Null,     
                        'payment_status' => 'pending',     
                        'payment_date' => Null,     
                        'description' => Null,     
                        'conversion' => 'pending',     
                        'cf1' => Null,     
                        'cf2' => Null,     
                        'cf3' => Null,     
                        
                    ]);
                }
                
            }

        }
      
        return $saveProfile;

    }


    public function pdUpdate($data){

        
        if($data->password){
            $pdUpdate = $this::where('id',$data->id)->update([           
                'first_name' => $data->first_name,  
                'last_name' => $data->last_name,          
                'dob' => $data->dob,          
                'gender' => $data->gender,          
                'country' => $data->country,          
                'city' => $data->city,          
                'email' => $data->email,              
                'phone' => $data->phone,          
                'alternate_email' => $data->alternate_email,          
                'alternate_phone' => $data->alternate_phone,
                'password' => Hash::make($data->password),
    
            ]);

            $pdUpdate = User::where('id',$data->user_id)->update([           
                'name' => $data->first_name." ".$data->last_name,                  
                'password' => Hash::make($data->password),    
            ]);

        }
        else{
            $pdUpdate = $this::where('id',$data->id)->update([           
                'first_name' => $data->first_name,  
                'last_name' => $data->last_name,          
                'dob' => $data->dob,          
                'gender' => $data->gender,          
                'country' => $data->country,          
                'city' => $data->city,          
                'email' => $data->email,              
                'phone' => $data->phone,          
                'alternate_email' => $data->alternate_email,          
                'alternate_phone' => $data->alternate_phone,              
    
            ]);
            $pdUpdate = User::where('id',$data->user_id)->update([           
                'name' => $data->first_name." ".$data->last_name,  
            ]);
        }
        
        
        return $pdUpdate;

    }


    public function qeUpdate($data){

            $data->subjects = json_encode($data->subjects);
            $data->other_language = json_encode($data->other_language);
            $data->software = json_encode($data->software);
            $qeUpdate = $this::where('id',$data->id)->update([           
                'native_language' =>$data->native_language,             
                'other_language' =>$data->other_language,             
                'academic_degree' =>$data->academic_degree,             
                'major' =>$data->major,             
                'subjects' =>$data->subjects,                  
                'software' => $data->software,  
    
            ]);
         
        
        return $qeUpdate;

    }

    public function aiUpdate($data){

        $aiUpdate = $this::where('id',$data->id)->update([           
            'available' => $data->available, 
            'urgent_orders' => $data->urgent_orders, 
            'about_me' => $data->about_me,
            'motivation_letter' => $data->motivation_letter,
            'website' => $data->website,
            'social_media' => $data->social_media,

        ]);
     
    
    return $aiUpdate;

}

}
