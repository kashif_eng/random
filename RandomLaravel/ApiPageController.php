<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Token;
use Illuminate\Support\Facades\URL;
use phpDocumentor\Reflection\Types\This;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\CmsInnerpage;


class ApiPageController extends Controller
{
    private $successStatus = 200;
    private $badRequest = 401;
    
    public function __construct()
    {
      
    }

    
    // api function
    public function tc(Request $request)
    { 
        $data = CmsInnerpage::where('template_type','inner_page')->first();
        //return $data;
        if ($data){            
            return response()->json(['status'=>'true','data'=>$data],$this->successStatus);
        }
        return response()->json(['success'=>false,'message'=>'No Data Found!!']);
    }
    
    
    
}
