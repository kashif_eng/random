<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



///Backend
Route::get('/', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/forget-password', 'Auth\LoginController@forgetPassword')->name('forgetPassword');
Route::post('/forget-password-email', 'Auth\LoginController@forgetPasswordEmail')->name('forgetPasswordEmail');
Route::get('reset-link/{id}', 'Auth\LoginController@resetLink')->name('resetLink');
Route::post('/reset-password', 'Auth\LoginController@resetPassword')->name('resetPassword');

Auth::routes();

Route::group(['middleware'=>'auth'],function (){

    Route::get('/orders', 'OrderController@orders')->name('orders');
    Route::get('/ordersajax', 'OrderController@ordersAjax')->name('ordersAjax');
    Route::get('order/{id}','OrderController@orderDetails')->name('orderDetails');
    Route::post('/addpayment', 'OrderController@addPayment')->name('addPayment');
    Route::post('/sendmessage', 'OrderController@sendMessage')->name('sendMessage');
    Route::post('/assign_writer', 'OrderController@assignWriter')->name('assignWriter');
    Route::get('approve_msg/{id}', 'OrderController@approveMsg')->name('approveMsg');
    Route::post('/update_order_parameter', 'OrderController@updateOrderParameter')->name('updateOrderParameter');
    Route::post('/update_commission', 'OrderController@updateCommission')->name('updateCommission');
    Route::post('/close-order', 'OrderController@closeOrder')->name('closeOrder');
    Route::get('/invoiceordersajax', 'WriterController@invoiceOrdersAjax')->name('invoiceOrdersAjax');
    Route::post('/generatewriterinvoice', 'WriterController@generateWriterInvoice')->name('generateWriterInvoice');
    Route::post('/del-invoice', 'WriterController@delInvoice')->name('delInvoice');
    Route::post('/pay-invoice', 'WriterController@payInvoice')->name('payInvoice');
    Route::get('/my_order_list_ajax', 'WriterController@myOrderListAjax')->name('myOrderListAjax');

    Route::post('/pay-invoice-credit', 'WriterController@payInvoiceCredit')->name('payInvoiceCredit');
    Route::post('/give-bonus', 'WriterController@giveBonus')->name('giveBonus');
    

    Route::post('/search', 'SearchController@search')->name('search');
    


    Route::get('/customers', 'CustomerController@customers')->name('customers');
    Route::get('/customersajax', 'CustomerController@customersAjax')->name('customersAjax');
    Route::get('customer/{id}','CustomerController@customerDetails')->name('customerDetails');
    Route::get('customertoggle/{id}','CustomerController@customerToggle')->name('customerToggle');
    Route::post('/customerupdate', 'CustomerController@customerUpdate')->name('customerUpdate');

    Route::post('/pay-customer-credit', 'CustomerController@payCustomerCredit')->name('payCustomerCredit');


    Route::get('/writers', 'WriterController@writers')->name('writers');
    Route::get('/writersajax', 'WriterController@writersAjax')->name('writersAjax');
    Route::get('writer/{id}','WriterController@writerDetails')->name('writerDetails');
    Route::post('/pd_update', 'WriterController@pdUpdate')->name('pdUpdate');
    Route::post('/qe_update', 'WriterController@qeUpdate')->name('qeUpdate');
    Route::post('/ai_update', 'WriterController@aiUpdate')->name('aiUpdate');
    Route::post('/mask_create', 'WriterController@maskCreate')->name('maskCreate');
    Route::post('/mask_delete', 'WriterController@maskDelete')->name('maskDelete');
    Route::get('writertoggle/{id}','WriterController@writerToggle')->name('writerToggle');



    Route::get('/inbox', 'InboxController@inbox')->name('inbox');
    Route::get('/inboxajax', 'InboxController@inboxAjax')->name('inboxAjax');
    Route::get('/msgajax', 'InboxController@msgAjax')->name('msgAjax');
    Route::post('/msgupdate', 'InboxController@msgUpdate')->name('msgUpdate');




    Route::get('/files', 'FilesController@files')->name('files');
    Route::get('/filesajax', 'FilesController@filesAjax')->name('filesAjax');

    Route::get('/payments', 'PaymentController@payments')->name('payments');
    Route::get('/paymentsajax', 'PaymentController@paymentsAjax')->name('paymentsAjax');

    Route::get('/transactions', 'PaymentController@transactions')->name('transactions');
    Route::get('/transactionsajax', 'PaymentController@transactionsAjax')->name('transactionsAjax');
    
    // Route::get('/referral', 'StoreCreditController@referral')->name('referral');
    // Route::get('/bonuses', 'StoreCreditController@bonuses')->name('bonuses');
    // Route::get('/storecredit', 'StoreCreditController@storecredit')->name('storecredit');

    Route::get('/profilepage', 'ProfileController@profilePage')->name('profilePage');
    Route::post('/profileupdate', 'ProfileController@profileUpdate')->name('profileUpdate');


    Route::get('/apipage', 'SettingsController@apiPage')->name('apiPage');
    Route::post('/apiupdate', 'SettingsController@apiUpdate')->name('apiUpdate');
    
    Route::get('/notifications', 'NotificationController@notifications')->name('notifications');
    Route::get('/notificationsajax', 'NotificationController@notificationsAjax')->name('notificationsAjax');

    
    // Route::get('/editWeb', 'SystemController@editWeb')->name('editWeb');
    // Route::post('/updateWeb', 'SystemController@updateWeb')->name('updateWeb');

    // Route::get('/editStore', 'SystemController@editStore')->name('editStore');
    // Route::post('/updateStore', 'SystemController@updateStore')->name('updateStore');

    // Route::get('/editContact', 'SystemController@editContact')->name('editContact');
    // Route::post('/updateContact', 'SystemController@updateContact')->name('updateContact');

    // Route::get('/viewSlider', 'CmsController@viewSlider')->name('viewSlider');
    // Route::get('/addSlider', 'CmsController@addSlider')->name('addSlider');
    // Route::post('/createSlider', 'CmsController@createSlider')->name('createSlider');
    // Route::get('editSlider/{id}','CmsController@editSlider')->name('editSlider');
    // Route::post('/updateSlider', 'CmsController@updateSlider')->name('updateSlider');
    // Route::get('deleteSlider/{id}','CmsController@deleteSlider')->name('deleteSlider');

    // Route::get('/viewPage', 'CmsController@viewPage')->name('viewPage');
    // Route::get('/addPage', 'CmsController@addPage')->name('addPage');
    // Route::post('/createPage', 'CmsController@createPage')->name('createPage');
    // Route::get('editPage/{id}','CmsController@editPage')->name('editPage');
    // Route::post('/updatePage', 'CmsController@updatePage')->name('updatePage');
    // Route::get('deletePage/{id}','CmsController@deletePage')->name('deletePage');

    // Route::get('/editHeader', 'CmsController@editHeader')->name('editHeader');
    // Route::post('/updateHeader', 'CmsController@updateHeader')->name('updateHeader');

    // Route::get('/editFooter', 'CmsController@editFooter')->name('editFooter');
    // Route::post('/updateFooter', 'CmsController@updateFooter')->name('updateFooter');


    // Route::get('/viewMenu', 'CmsController@viewMenu')->name('viewMenu');
    // Route::get('/addMenu', 'CmsController@addMenu')->name('addMenu');
    // Route::post('/createMenu', 'CmsController@createMenu')->name('createMenu');
    // Route::get('editMenu/{id}','CmsController@editMenu')->name('editMenu');
    // Route::post('/updateMenu', 'CmsController@updateMenu')->name('updateMenu');
    // Route::get('deleteMenu/{id}','CmsController@deleteMenu')->name('deleteMenu');

});
