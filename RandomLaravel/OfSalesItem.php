<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
|--------------------------------------------------------------------------
| Custom Field Notes
|--------------------------------------------------------------------------
| cf1 = Attachment Size
| cf2 = 
| cf3 = 
*/


class OfSalesItem extends Model
{
    protected $table='of_sales_items';
    protected $guarded = [
        'id','_token'
    ];

    public function saveSaleItems($saleResult,$formdata)
    {
       
        $test = array();
        foreach($formdata as $key=>$item){

            $itemDescription = $this->itemDescription($key);
            
            if($itemDescription != false){
                
                $isAllow = $this->isAllow($key,$item,$formdata,$saleResult);
                if($isAllow){
                
                        $itemValue = $this->itemValue($key,$item,$formdata);
                        if(!$itemValue){
                            $itemValue = $item;
                        }
                            if($itemValue){
                            
                                $saleItemData = array(
                                    'sale_id'=> $saleResult->id,
                                    'order_number'=> $saleResult->order_number,
                                    'customer_id'=> $saleResult->customer_id,			
                                    'service_type'=> $saleResult->service_type,
                                    'order_type'=> $saleResult->order_type,
                                    'meta_key'=> $key,
                                    'meta_description'=> $itemDescription,			
                                    'meta_value'=> $itemValue,	
                                    'cf1'=> Null,
                                    'cf2'=> Null,			
                                    'cf3'=> Null,
                                    'cf4'=> Null,
                                    'cf5'=> Null   
                                );
            
                                $this->create($saleItemData);

                            }

                }
                    

            }
            

        }
       
        
    }

    public function itemDescription($key){

        if($key == "wordlength" || $key == "wordlengthps" ||  $key == "edwordlengthnew" ||  $key == "wordlengthposter"){
            return "Word Count";
        }

        if($key == "wordlengthbib"){
            return "Source Count";
        }

        if($key == "dissertation_part_words"){
            return "Dissertation Part Word Count";
        }

        if($key == "abstract"){
            return "Abstract";
        }

        if($key == "saoptions"){
            return "SA Options";
        }

        if($key == "introduction"){
            return "Introduction";
        }

        if($key == "methodology"){
            return "Methodology";
        }

        if($key == "questionnaire"){
            return "Questionnaire";
        }

        if($key == "analysis_of_findings"){
            return "Analysis of Findings";
        }

        if($key == "literature_review"){
            return "Literature Review";
        }

        if($key == "discussion_and_conclusion"){
            return "Discussion and Conclusion";
        }

        if($key == "other_part"){
            return "Other Part";
        }

        if($key == "poster_size"){
            return "Poster Size";
        }

        if($key == "slide_length"){
            return "Slide Length";
        }

        if($key == "os-prefarea"){
            return "Area of study";
        }

        if($key == "os-wc"){
            return "Maximum word count";
        }

        if($key == "os-reqwriter"){
            return "Instructions for the writer";
        }

        if($key == "os-refstyle"){
            return "Referencing style";
        }

        if($key == "os-topic"){
            return "I already have an approved topic";
        }

        if($key == "os-reqtopic"){
            return "I need custom made topic";
        }

        if($key == "os-primres"){
            return "Will the dissertation require primary research?";
        }

        if($key == "os-primres"){
            return "Will the dissertation require primary research?";
        }
        
        if($key == "os-reflist"){
            return "Do you also want a reference list of essential sources?";
        }
        
        if($key == "os-dany"){
            return "Would you like the writer to perform any data analysis?";
        }

        if($key == "os-diss"){
            return "Did we complete a Dissertation Topic and Outline for you?";
        }

        if($key == "os-appdiss"){
            return "Do you have an approved dissertation topic?";
        }

        if($key == "os-prop"){
            return "Would you like a Proposal?";
        }

        if($key == "os-temp"){
            return "Do you have a proposal template to follow?";
        }

        if($key == "rs-presearch"){
            return "Primary Research (questionnaires, interviews, focus groups etc.)";
        }

        if($key == "rs-pr-sub1"){
            return "Primary Research Price?";
        }

        if($key == "rs-emp"){
            return "Secondary Research (Journals, Books, Magazines, Thesis Papers, etc.)";
        }

        if($key == "rs-del"){
            return "Delivery of Your Dissertation Paper!";
        }

        if($key == "os-dcol"){
            return "Data Collection Price";
        }

        if($key == "os-statsoft1" || $key == "os-statsoft2" || $key == "os-statsoft3" || $key == "os-statsoft4"){
            return "Statistical analysis software";
        }

        if($key == "os-exformat"){
            return "Exam notes format";
        }

        if($key == "os-exnotes0" || $key == "os-exnotes1" || $key == "os-exnotes2" || $key == "os-exnotes3" || $key == "os-exnotes4" || $key == "os-exnotes5" || $key == "os-exnotes6"){
            return "Various Exam Notes";
        }

        if($key == "os-statsany"){
            return "Does this statistical analysis relate to your dissertation?";
        }

        if($key == "os-dana"){
            return "Data Collection Price";
        }

        if($key == "os-lab"){
            return "Have you done the laboratory experiment?";
        }

        if($key == "e-contentpage"){
            return "Do you require content pages?";
        }

        if($key == "e-models"){
            return "Specific Models";
        }

        if($key == "e-sources"){
            return "Specific sources";
        }

        if($key == "e-plag"){
            return "Plagiarism Report";
        }

        if($key == "e-summary"){
            return "An abstract or executive summary";
        }

        if($key == "e-stats"){
            return "Statistical analysis of data";
        }

        if($key == "e-reflective"){
            return "Reflective Log/Diary";
        }

        if($key == "e-presentation"){
            return "Presentation";
        }

        if($key == "e-draft"){
            return "Draft";
        }

        if($key == "e-calculation"){
            return "Calculation";
        }

        if($key == "e-graph"){
            return "Graphs, charts or specific images.";
        }

        if($key == "e-comment"){
            return "Comments/speech for your poster.";
        }

        if($key == "e-accompany"){
            return "A report to accompany your presentation";
        }

        if($key == "e-report"){
            return "A written report/explanations";
        }

        if($key == "phoneconsult"){
            return "Consult your academic over the phone";
        }

        return false;

        
    }

    public function itemValue($key,$item,$formdata){

        $result = "";
        
        // if($item == Null){
        //     return false;
        // }
        
        // if($item < 1){
        //     return false;
        // }
        
        if($key == "os-reflist"){
            if($item < 1){$result = "No Thanks";}
            if($item == 9.99){$result = "Reference List of 10 sources Price: £9.99";}
            if($item == 19.99){$result = "Reference List of 10 sources Price: £19.99";}
            if($item == 29.99){$result = "Reference List of 10 sources Price: £29.99";}
            if($item == 39.99){$result = "Reference List of 10 sources Price: £39.99";}
            if($item == 49.99){$result = "Reference List of 10 sources Price: £49.99";}           
            return $result;
        }

        if($key == "os-dany"){            
            if($item == "Yes"){$result = $formdata["os-dny-sub"];}               
            return $result;
        }

        if($key == "os-diss"){            
            if($item == "Yes"){$result = $formdata["os-diss-sub"];}               
            return $result;
        }

        if($key == "os-appdiss"){            
            if($item == "Yes"){$result = $formdata["os-appdiss-sub"];}               
            return $result;
        }

        if($key == "os-apppr"){            
            if($item == "Yes"){$result = $formdata["os-apppr-sub"];}               
            return $result;
        }

        if($key == "os-dcol"){            
            if($item == "0"){$result = "FREE: You will provide the data";}  
            if($item == "195"){$result = "EXTRA CHARGE. The writer will provide made up data (GBP 195)";}               
            return $result;
        }

        if($key == "rs-presearch"){            
            if($formdata["rs-pr-sub1"] == "0"){$result = "FREE: You will provide the data";}  
            if($formdata["rs-pr-sub1"] == "195"){$result = "EXTRA CHARGE. The writer will provide made up data (GBP 195)";}               
            return $result;
        }

        if($key == "rs-emp"){            
            $result = "Secondary Research (Journals, Books, Magazines, Thesis Papers, etc)";             
            return $result;
        }

        if($key == "rs-del"){            
            if($item == "0"){$result = "FREE: Standard Delivery";}  
            if($item == "125"){$result = "EXTRA CHARGE. Chapter By Chapter Delivery (GBP 125)";}               
            return $result;
        }

        if($key == "os-dana"){            
            if($item == "0"){$result = "FREE: You will provide the data";}  
            if($item == "195"){$result = "EXTRA CHARGE. The writer will provide made up data (GBP 195)";}               
            return $result;
        }


        if($key == "e-contentpage"){            
            $result = "Yes";               
            return $result;
        }

        if($key == "e-models"){            
            $result = $formdata["e-models-sub"];               
            return $result;
        }

        if($key == "e-sources"){            
            $result = $formdata["e-sources-sub"];               
            return $result;
        }

        if($key == "e-plag"){            
            $result = "Plagiarism Report";               
            return $result;
        }

        if($key == "e-graph"){            
            $result = $formdata["e-graph-sub"];               
            return $result;
        }

        if($key == "e-summary"){            
            $result = "An abstract or executive summary";               
            return $result;
        }

        if($key == "e-stats"){            
            $result = $formdata["e-stats-sub"];               
            return $result;
        }

        if($key == "e-reflective"){            
            $result = $formdata["wordlength3"]." - ".$formdata["e-reflective-subtext"];               
            return $result;
        }

        if($key == "e-draft"){            
            $result = ($formdata["e-draft-sub"]/50)." Draft - GBP:".$formdata["e-draft-sub"];               
            return $result;
        }

        if($key == "e-calculation"){            
            $result = $formdata["e-calculation-sub"];               
            return $result;
        }

        if($key == "e-comment"){            
            $result = "Comments/speech for your poster";               
            return $result;
        }

        if($key == "e-accompany"){            
            $result = $formdata["wordlength4"];                 
            return $result;
        }

        if($key == "e-report"){            
            $result = $formdata["e-report-sub"];               
            return $result;
        }

        if($key == "e-presentation"){            
            $result = $formdata["slide_length2"];                 
            return $result;
        }
        
        if($key == "phoneconsult"){            
            if($item < 50){$result = "No";}
            if($item > 49){$result = "Planning Call (upto 30 minutes) GBP 50";}             
            return $result;
        }
       
    }




    public function isAllow($key,$item,$formdata,$saleResult){

        $orderType = $saleResult->order_type;

        if($orderType == "Dissertation Topic"){
            $allow = array("os-prefarea","os-reqwriter","phoneconsult");
        }

        if($orderType == "Dissertation Topic And Plan Outline"){
            $allow = array("os-prefarea","os-reqwriter","phoneconsult","os-topic","os-reqtopic","os-primres","os-reflist");
        }

        if($orderType == "Dissertation Part"){
            $allow = array("abstract","introduction","methodology","questionnaire","analysis_of_findings","literature_review","discussion_and_conclusion","other_part","dissertation_part_words","os-prefarea","os-reqwriter","os-refstyle","phoneconsult","os-dany",
            "e-contentpage","e-models","e-sources","e-plag",
            "e-reflective","e-presentation","e-draft");
        }

        if($orderType == "Dissertation Full"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult","os-diss","os-appdiss","os-prop",
            "rs-presearch","rs-emp","rs-del",
            "e-contentpage","e-models","e-sources","e-plag","e-summary","e-graph","e-stats",
            "e-reflective","e-presentation");
        }
       
        if($orderType == "Dissertation Statistical Analysis"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult","os-dany","os-dcol","os-statsoft1","os-statsoft2","os-statsoft3","os-statsoft4",           
            "e-contentpage","e-models","e-sources","e-plag",
            "e-reflective","saoptions");
        }

        if($orderType == "Dissertation Proposal"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult","os-primres","os-diss","os-apppr","os-temp",        
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-reflective","e-presentation","e-draft");
        }

        if($orderType == "Data Analysis"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",      
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-draft","e-presentation","e-calculation");
        }

        if($orderType == "Editing Proofreading" || $orderType == "Academic Editing Rewriting"){
            $allow = array("edwordlengthnew","os-prefarea","os-reqwriter","os-wc","os-refstyle","phoneconsult",      
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-draft","e-presentation","e-calculation");
        }

        if($orderType == "Marking Service"){
            $allow = array("edwordlengthnew","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",      
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-draft","e-presentation","e-calculation");
        }

        if($orderType == "Assignment" || $orderType == "Report" || $orderType == "Coursework" || $orderType == "Essay"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",      
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-draft","e-presentation","e-calculation");
        }

        if($orderType == "Exam Notes"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult","os-exformat","os-exnotes0","os-exnotes1","os-exnotes2","os-exnotes3","os-exnotes4","os-exnotes5","os-exnotes6",      
            "e-models","e-sources","e-report",
            "e-draft","e-calculation");
        }

        if($orderType == "Statistical Analysis"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult","os-statsany","os-dana","os-statsoft1","os-statsoft2","os-statsoft3","os-statsoft4",      
            "e-models","e-sources","e-report",
            "e-reflective","e-draft","e-report","saoptions");
        }

        if($orderType == "Laboratory Report"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult","os-lab","os-statsoft1","os-statsoft2","os-statsoft3","os-statsoft4",      
            "e-sources","e-plag",
            "e-reflective","e-draft","e-presentation","e-calculation");
        }

        if($orderType == "Literature Review" || $orderType == "Reflective Report"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-reflective","e-presentation","e-draft");

        }

        if($orderType == "Plan Model Answer" || $orderType == "Case Study Writing Service" || $orderType == "Research Proposal Writing Service" || $orderType == "Business Plan Writing Service" || $orderType == "Primary Research Service" || $orderType == "Secondary Research Collation Service"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-reflective","e-presentation","e-draft","e-calculation");

        }

        if($orderType == "Research Proposal Writing Service"){
            $allow = array("wordlength","os-prefarea","os-reqwriter","os-temp","os-appdiss","os-refstyle","phoneconsult",
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-reflective","e-presentation","e-draft","e-calculation");

        }

        if($orderType == "Poster"){
            $allow = array("poster_size","wordlengthposter","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",
            "e-models","e-sources","e-plag","e-graph",
            "e-reflective","e-report","e-draft","e-calculation","e-accompany");

        }

        if($orderType == "Presentation"){
            $allow = array("slide_length","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",
            "e-models","e-sources","e-plag","e-graph",
            "e-reflective","e-comment","e-draft","e-calculation","e-accompany");

        }

        if($orderType == "Personal Statement"){
            $allow = array("wordlengthps","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-reflective","e-presentation","e-draft","e-calculation");

        }
        
        if($orderType == "Annotated Bibliography Service"){
            $allow = array("wordlengthbib","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",
            "e-contentpage","e-models","e-sources","e-plag","e-summary",
            "e-reflective","e-presentation","e-draft","e-calculation");

        }

        if($orderType == "Professional Video Editing Service"){
            $allow = array("wordlengthbib","os-prefarea","os-reqwriter","os-refstyle","phoneconsult",            
            "e-reflective","e-presentation","e-draft");

        }
        
        $result = in_array($key, $allow);

        return $result;

        


    }


    public function saveSaleFileItems($saleResult,$newFileName,$key,$fileSize){

        
        $saleItemData = array(
            'sale_id'=> $saleResult->id,
            'order_number'=> $saleResult->order_number,
            'customer_id'=> $saleResult->customer_id,			
            'service_type'=> $saleResult->service_type,
            'order_type'=> $saleResult->order_type,
            'meta_key'=> $key,
            'meta_description'=> "Attachments",			
            'meta_value'=> $newFileName,	
            'cf1'=> $fileSize,
            'cf2'=> Null,			
            'cf3'=> Null,
            'cf4'=> Null,
            'cf5'=> Null   
        );
    
        $this->create($saleItemData);


    }
    

}
