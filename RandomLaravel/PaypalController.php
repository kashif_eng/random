<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OfCustomer;
use App\Models\OfSale;
use App\Models\OfSalesItem;
use App\Models\Payment;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Session;
use Exception;
use Config;
use App\Models\OfSaleStatus;


class PaypalController extends Controller
{
    
    private  $customer,$sale,$item,$payment;
    public function __construct(OfCustomer $customer,OfSale $sale,OfSalesItem $item,Payment $payment)
    {
        //$this->middleware('auth');
        $this->customer = $customer;
        $this->sale = $sale;
        $this->item = $item;
        $this->payment = $payment;

       
    }


    public function paypalpost(Request $request)
    {
        //dd($request->all());
        
        $ClientId = Config::get('app.ClientId');
        $ClientSecret = Config::get('app.ClientSecret');
        $BrandName = Config::get('app.BrandName');
        $DeveloperEmailAccount = Config::get('app.DeveloperEmailAccount');
        $ReturnUrl = Config::get('app.ReturnUrl');
        $CancelUrl = Config::get('app.CancelUrl');
        $Basic = Config::get('app.Basic');
       
        $orderDetail = $this->sale->where("order_number",$request->orderid)->first();

                       

        $thedata = array(
            "intent" => "CAPTURE",
            "purchase_units" => [array(              
                "reference_id" => "default",
                "description" => $orderDetail->service_type."-".$orderDetail->order_type,
                "invoice_id" => $request->orderid,
                "amount" => array(
                  "currency" => "GBP",
                  "details" => array(
                    "subtotal" => $request->paymentpaypal,
                    "shipping" => "0",
                    "tax" => "0"
                  ),
                  "total" => $request->paymentpaypal
                ),
                "payee" => array(
                  "email" => $DeveloperEmailAccount
                ),               
                "payment_linked_group" => 1,
                "custom" => $request->orderid,
                "invoice_number" => rand(),
                "payment_descriptor" => $BrandName             
            )],
            "application_context" => array(
                  "brand_name" => "Research Prospect",
                  "landing_page" => "LOGIN",
                  "shipping_preferences" => "NO_SHIPPING",
                  "user_action" => "CONTINUE",
              
                  "return_url" => $ReturnUrl,
                  "cancel_url" => $CancelUrl
            ),
            "redirect_urls" => array(
                "return_url" => $ReturnUrl,
                "cancel_url" => $CancelUrl
            )
        );

        $thedata = json_encode($thedata);
       
		
        $curl = curl_init();

        //$curl = curl_init();

        curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.sandbox.paypal.com/v1/oauth2/token",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "grant_type=client_credentials",
			CURLOPT_HTTPHEADER => array(
			  "Authorization: Basic ".$Basic,
			  "Content-Type: application/x-www-form-urlencoded"
			),
		  ));

			
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
            $getresponse = json_decode($response,TRUE);
            $thetoken = $getresponse["access_token"];
        }
        
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sandbox.paypal.com/v1/checkout/orders/",
        CURLOPT_SSL_VERIFYPEER =>false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $thedata,
        CURLOPT_HTTPHEADER => array(
			"Accept: */*",
            "Authorization: Bearer ".$thetoken."",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Content-Type: application/json",
            "Host: api.paypal.com",
            "Postman-Token: 7f389cf8-8027-4437-9e66-c4d25bf2fcba,f22800e0-b021-4934-abc1-340bfeb32d67",
            "User-Agent: PostmanRuntime/7.15.0",
            "accept-encoding: gzip, deflate",
            "cache-control: no-cache",
            "content-length: ".strlen($thedata)."",
            "cookie: x-pp-s=eyJ0IjoiMTU2MjU3NzcxNzQ2OCIsImwiOiIwIiwibSI6IjAifQ; ts=vr%3D814d7f4c16b0a590d69cf0baffffceca%26vreXpYrS%3D1657248494%26vteXpYrS%3D1562579517%26vt%3Dd0e3c8b216bac1200018c47dffff5028; cookie_check=yes; tsrce=unifiedloginnodeweb; X-PP-K=1562568469:5:NA; X-PP-SILOVER=name%3DSANDBOX3.API.1%26silo_version%3D1880%26app%3Dapiplatformproxyserv%26TIME%3D1836655709%26HTTP_X_PP_AZ_LOCATOR%3Dsandbox.slc"
        ),
        ));
		
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        
        if ($err) {
        echo "cURL Error #:" . $err;
        } else {  
            $getresponse = json_decode($response,TRUE);           
            $approvearray = $getresponse["links"][1];
            $approvelink = $approvearray["href"]; 
            
             			
            Session::put('responseid',$getresponse["id"]);
            Session::put('order_number',$request->orderid);
            Session::put('paymentpaypal',$request->paymentpaypal);

            
            
            return redirect()->to($approvelink);

            
        }     
        
        


    }



    public function paypalprocess(Request $request)
    {

            
            $responseid = $request->session()->get('responseid');
            $order_number = $request->session()->get('order_number');
            $paymentpaypal = $request->session()->get('paymentpaypal');
            $Basic = Config::get('app.Basic');
            //$orderDetail = $this->sale->where("order_number",$request->orderid)->first();
            

            $curl = curl_init();

            //$curl = curl_init();
    
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.sandbox.paypal.com/v1/oauth2/token",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "grant_type=client_credentials",
                CURLOPT_HTTPHEADER => array(
                  "Authorization: Basic ".$Basic,
                  "Content-Type: application/x-www-form-urlencoded"
                ),
              ));
    
                
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);

            if ($err) {
            echo "cURL Error #:" . $err;
            } else {
                $getresponse = json_decode($response,TRUE);
                $thetoken = $getresponse["access_token"];
            }


            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sandbox.paypal.com/v1/checkout/orders/".$responseid."/capture",
            CURLOPT_SSL_VERIFYPEER =>false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Authorization: Bearer ".$thetoken."",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: api.paypal.com",
                "Postman-Token: 298a851d-7a47-4f21-aac7-e0e3f55e554b,0404db16-ed25-45f8-a203-b01d5a792d54",
                "User-Agent: PostmanRuntime/7.15.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "content-length: ",
                "cookie: x-pp-s=eyJ0IjoiMTU2MjU3NzcxNzQ2OCIsImwiOiIwIiwibSI6IjAifQ; ts=vr%3D814d7f4c16b0a590d69cf0baffffceca%26vreXpYrS%3D1657248494%26vteXpYrS%3D1562579517%26vt%3Dd0e3c8b216bac1200018c47dffff5028; cookie_check=yes; tsrce=unifiedloginnodeweb; X-PP-K=1562568469:5:NA"
            ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            
           
        
            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
                $order_number_enc = Crypt::encryptString($order_number);
                return redirect()->route('pay',$order_number_enc)->with('failed','Something went wrong. Please try again.');
            } else {
                $response = json_decode($response,TRUE);        
                if(array_key_exists("order_id",$response)) {

                        //Payment Update Sale Table
                        date_default_timezone_set('Asia/Karachi');	
                        $date = date("Y-m-d");	       

                        $orderDetail = $this->sale->where("order_number",$order_number)->first();
                        $balance = $orderDetail->total - ($paymentpaypal + $orderDetail->paid);
                        if($balance > 0){
                            $payment_status = "partially_paid";
                        }
                        else{
                            $payment_status = "paid";
                        }        

                        if($paymentpaypal < $orderDetail->total){
                            $paymentDetail = $this->payment->where('order_number',$order_number)->orderBy('id', 'DESC')->get();
                            if(count($paymentDetail) > 0){
                                $newTotal = $orderDetail->total;
                            }
                            else{
                                $newTotal = $paymentpaypal*2;
                            }                            
                        }   
                        else{
                            $newTotal = $orderDetail->total;
                        }

                        $this->sale->where("order_number",$order_number)->update([
                            "total" => $newTotal,
                            "paid" => ($paymentpaypal + $orderDetail->paid),
                            "payment_status" => $payment_status,                            
                        ]);

                        

                        //Payment Create Payment Table
                        $paymentDetail = $this->payment->where('order_number',$order_number)->orderBy('id', 'DESC')->get();
                        
                        if(count($paymentDetail) > 0){
                            $transaction = "TRPAYPAL".$order_number.(count($paymentDetail)+1);
                        }
                        else{
                            $transaction = "TRPAYPAL".$order_number."1";
                        }

                        $paymentData = array(
                            'sale_id'=> $orderDetail->id,            
                            'order_number'=> $order_number,
                            'invoice_number'=> $order_number,
                            'payment_of'=> 'customer_payment',
                            'transaction_no'=> $transaction,    
                            'amount'=> $paymentpaypal,         
                            'payment_mathod'=> 'Paypal',            
                            
                        );

                        $paymentResult = $this->payment->create($paymentData);

                        $status = OfSaleStatus::where('order_number',$order_number)->update([
                            'order_number' => $order_number,
                            'current_status' => $payment_status,
                            'awaiting_for_payment' => $payment_status,
                            'academic_review' => 'yes',
                            // 'awaiting_more_info' => 'pending',            
                            // 'in_progress' => '1',
                            // 'in_review' => '1',
                            // 'complete' => '1',
                            
                        ]);


                        return redirect()->route('thanks',$order_number)->with('thanks','Your payment has been captured successfully.');


                }
                else{ 
                    $order_number_enc = Crypt::encryptString($order_number);
                    //return redirect()->route('pay',$order_number_enc)->with('failed','Something went wrong. Please try again.');
                    return redirect()->back()->with('failed','Something went wrong. Please try again.');
                    
                }

            }

    }


}