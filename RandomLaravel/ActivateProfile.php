<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Profile;
use App\Models\PasswordReset;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Config;

class ActivateProfile extends Mailable
{
    use Queueable, SerializesModels;

    
    public $theprofile;

    public function __construct(Profile $profile)
    {
        $this->theprofile = $profile;

    }

   
    public function build()
    {

        $crytp_email = Crypt::encryptString($this->theprofile->email);
        $web_url = Config::get('app.web_url');
        $reset_link = $web_url."activate-profile/".$crytp_email;
       

        return $this->subject('Welcome to wisedemics- Registered Successfully!')
                    ->view('emails.activate_profile',compact('reset_link'));
                    
    }
}
