<?php

namespace Cmcmtech\Mulphilog\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Cmcmtech\Mulphilog\Helper\Data as Helper;

class SaveLicense implements ObserverInterface
{
    protected $_messageManager;    
    private $_helper;


    public function __construct(        
        \Magento\Framework\Message\ManagerInterface $messageManager,
        Helper $helper       
    ) {
        $this->_messageManager = $messageManager;
        $this->_helper = $helper;
    }

  
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        
        $this->_helper->debug('execute observer save license');
        
        $username = $this->_helper->getAdminFieldSave('mulphilog_activate/mt_user');
        $productkey = $this->_helper->getAdminFieldSave('mulphilog_activate/mt_key');
        $productid = $this->_helper->getAdminFieldSave('mulphilog_activate/mt_productid');
        $api_key = $this->_helper->getAdminFieldSave('mulphilog_activate/mt_apikey');
        
        $check = $this->_helper->checkPurchaseCode(true,$username,$productid,$productkey,$api_key);
        $this->_helper->debug($check);
        
        $check = json_decode($check,true);
        
       
        $this->_helper->debug($check['status']);
        
        if($check['status'] == "1") {
            $this->_messageManager->getMessages(true);
            $this->_messageManager->addSuccess( $check['message'] );
        }
        else{
            $this->_messageManager->addError( $check['message'] );
        }
        
    }
}